/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : calibration.h
	Author       : Faustas Bagdonas
	Contributors : Dovydas Girdvainis
	Date Created : 2016-03
	Purpose      : Measurement calibration data functions
	Comments     :

	POSSIBLE BUGS:
	   1. I am not sure if EEPROM data CRC calculation algorithm matches with
	      Raimondas' one. Kazkada prisimenu sutapo, bet kazkas kazka pakeite ir po
	      to lyg nebesutapo. O gal dabar sutampa. Vienu metu buvo uzkomentuotas
	      CRC skaiciavimas, bet lygtais dabar jis atkomentuotas. Polinomas sutampa.

   ================================================================================= */

#ifndef _CALIBRATION_H_
#define _CALIBRATION_H_

#include <stm32f303xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "hal_flash.h"
#include "i2c_eeprom.h"
#include "crc.h"
#include "delay.h"

#define CALIBRATION_DATA_START_ADDRESS	(uint32_t)0x08037000UL
#define CALIBRATION_DATA_END_ADDRESS	(uint32_t)0x0803EFFFUL
#define PAGE_SIZE	2048
#define I2C_INTERFACE_ADDRESS	I2C1
#define EEPROM_ADDRESS	0x50
#define MIN_MAXVALUE	10

enum cal_status{SUCCESSFUL, INVALID_DATA, COPY_ERROR, SAME_SENSOR, ERASE_ERROR};

enum cal_status calibration_retrieveSensorCalibrationData();
bool calibration_sensorExists();
bool calibration_sensorValid();
bool calibration_dataInFlashIsValid();
void calibration_getSerialNumber(uint8_t *serialNumber);
int16_t calibration_getOffset();
bool calibration_setOffset(int16_t offset);
void calibration_changeEndianEEPROM();
bool calibration_isSensorConnected();

#endif
