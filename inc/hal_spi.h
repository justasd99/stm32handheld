/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : hal_rtc.h
	Author       : Faustas Bagdonas
	Date Created : 2016-03
	Purpose      : For internal SPI
	Comments     : Very very UGLY, quick & dirty implementation

   ================================================================================= */

#ifndef _HAL_SPI_
#define _HAL_SPI_

#include <stm32f3xx.h>
#include "delay.h"

#define SPI_PRESCALER_DIV4		0x00000008UL
#define SPI_PRESCALER_DIV8		0x00000010UL
#define SPI_PRESCALER_DIV16		0x00000018UL
#define SPI_PRESCALER_DIV32		0x00000020UL
#define SPI_PRESCALER_DIV64		0x00000028UL
#define SPI_PRESCALER_DIV128	0x00000030UL

void SPI_init(uint32_t prescaler);
//uint16_t SPI_SendData(SPI_TypeDef *spi, uint8_t Data);
bool SPI_SendReceiveByte(/*SPI_TypeDef *spi,*/ uint8_t txByte, uint8_t *rxByte, uint8_t Timeout);

#endif
