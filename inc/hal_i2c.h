/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : hal_i2c.h
	Author       : Faustas Bagdonas
	Date Created : 2016-03
	Purpose      : For internal MCU's I2C controller use
	Comments     :

   ================================================================================= */

#ifndef _HAL_I2C_
#define _HAL_I2C_
#include <stm32f303xc.h>
#include <stdbool.h>
#include "delay.h"

#define MAX_TRY	300

enum Mode{STANDARD_10, STANDARD_100, FAST, FAST_PLUS};

void I2C_setMode(I2C_TypeDef *i2c, enum Mode mode);
bool I2C_sendByte(I2C_TypeDef *i2c, uint8_t address, uint8_t data, uint16_t timeout);
bool I2C_sendBytes(I2C_TypeDef *i2c, uint8_t address, const uint8_t *data, uint8_t length, uint16_t timeout);
bool I2C_receiveBytes(I2C_TypeDef *i2c, uint8_t address, uint8_t *buffer, uint16_t count, uint16_t timeout);
bool I2C_isBusy(I2C_TypeDef *i2c);

#endif
