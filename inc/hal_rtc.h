/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : hal_rtc.h
	Author       : Faustas Bagdonas
	Date Created : 2016-03
	Purpose      : For internal RTC
	Comments     :

   ================================================================================= */

#ifndef _HAL_RTC_
#define _HAL_RTC_

#include <stm32f303xc.h>
#include "delay.h"

typedef struct{uint8_t year, month, day;} DATE;
typedef struct{uint8_t hours, minutes, seconds;} TIME;

void RTC_init();
DATE RTC_getDate();
TIME RTC_getTime();
void RTC_setDate(const DATE *date);
void RTC_setTime(const TIME *time);

#endif
