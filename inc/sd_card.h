//Remiantis Justo projektu
//Si koda Justas paeme is knygos
//JAN AXELSON "USB MASS STORAGE. Designing and programming devices and embedded hosts"
//ir pritaike savo projektui. Veliau as si koda nusikopijavau is Justo projekto ir
//pritaikiau savajam. Reikejo modifikaciju, iskaitant nauju microSD korteliu palaikyma.

//TODO: SI KODA AS PRITAIKIAU LABAI NETVARKINGAI IR PAKANKAMAI ATMESTINAI, TAD
//KODAS PO PAKEITIMU DABAR YRA SUPER NETVARKINGAS. REIKETU SUTVARKYTI!

#ifndef _SD_CARD_
#define _SD_CARD_

//#include <stm32f303xc.h>
#include <stm32f3xx.h>
#include "delay.h"
#include "hal_spi.h"

 #define byte  uint8_t
 #define word  uint16_t
 #define dword uint32_t

 #define ERROR_SDC		uint32_t
 #define FATAL_ERROR	0xFFFFFFFF
 #define NO_EXIST		0xFFFFFFFE


 #define FALSE				0
 #define TRUE				1

 #define MOREDATA			1
 #define NODATA				0

 #define CSD_SIZE			16
 #define DATA_START_TOKEN	0xFE				// The Start Block token

 #define mReadCRC()			WriteSPI(0xFF);		WriteSPI(0xFF);
 #define mSend8ClkCycles()	WriteSPI(0xFF);
 #define mSendCRC()			WriteSPI(0xFF);		WriteSPI(0xFF);

 #define SDC_FLOATING_BUS	0xFF
 #define SDC_BAD_RESPONSE	SDC_FLOATING_BUS
 #define SDC_SECTOR_SIZE	512

 #define DATA_ACCEPTED		0x05//0b00000101

 //--- The SD Card Commands (SPI Bus Mode)
 //--- Bit 7 = 0, Bit 6 = 1, Bits 5-0=Command Code
 #define     cmdGO_IDLE_STATE        0x40		//0 is the 6-bit command
 #define     cmdSEND_OP_COND         0x41		//1
 #define     cmdSEND_CSD             0x49		//9
 #define     cmdSEND_CID             0x4a		//10
 #define     cmdSTOP_TRANSMISSION    0x4c		//12
 #define     cmdSEND_STATUS          0x4d		//13
 #define     cmdSET_BLOCKLEN         0x50		//16
 #define     cmdREAD_SINGLE_BLOCK    0x51		//17
 #define     cmdREAD_MULTI_BLOCK     0x52		//18
 #define     cmdWRITE_SINGLE_BLOCK   0x58		//24
 #define     cmdWRITE_MULTI_BLOCK    0x59		//25
 #define     cmdTAG_SECTOR_START     0x60		//32
 #define     cmdTAG_SECTOR_END       0x61		//33
 #define     cmdUNTAG_SECTOR         0x62		//34
 #define     cmdTAG_ERASE_GRP_START  0x63		//35
 #define     cmdTAG_ERASE_GRP_END    0x64		//36
 #define     cmdUNTAG_ERASE_GRP      0x65		//37
 #define     cmdERASE                0x66		//38
 #define     cmdSD_APP_OP_COND       0x69		//41
 #define     cmdLOCK_UNLOCK          0x71		//49
 #define     cmdAPP_CMD              0x77		//55
 #define     cmdREAD_OCR             0x7a		//58
 #define     cmdCRC_ON_OFF           0x7b		//59
 #define     cmdCMD8           0x48		//

//--- sdmmc_cmd
  typedef enum
  {
	  GO_IDLE_STATE,		//==0
	  SEND_OP_COND,
	  SEND_CSD,
	  SEND_CID,
	  STOP_TRANSMISSION,
	  SEND_STATUS,
	  SET_BLOCKLEN,
	  READ_SINGLE_BLOCK,
	  READ_MULTI_BLOCK,
	  WRITE_SINGLE_BLOCK,
	  WRITE_MULTI_BLOCK,
	  TAG_SECTOR_START,
	  TAG_SECTOR_END,
	  UNTAG_SECTOR,
	  TAG_ERASE_GRP_START,
	  TAG_ERASE_GRP_END,
	  UNTAG_ERASE_GRP,
	  ERASE,
	  LOCK_UNLOCK,
	  SD_APP_OP_COND,
	  APP_CMD,
	  READ_OCR,
	  CRC_ON_OFF,
	  CMD8
  }sdmmc_cmd;

 //--- RESP
 typedef enum
 {
	 R1,	//==0
	 R1b,	//==1
	 R2,
	 R3
 }RESP;

//Kazkodel sita uniona kompiliuoja netaip. Gal keist reiketu optimizacijas? Butu gerai ateityje issiaiskint, o kolkas supaprastinu
//--- CMD_PACKET
// typedef union
// {
////	 struct
////	 {
////		byte field[5];
////	 };
//	 struct
//	 {
//		 uint8_t crc;
//		 uint8_t addr0;
//		 uint8_t addr1;
//		 uint8_t addr2;
//		 uint8_t addr3;
//		 uint8_t cmd;
//	 };
//	 struct
//	 {
//		 uint8_t END_BIT:1;
//		 uint8_t CRC7:7;
//		 //uint8_t crc7_end;
//		 uint32_t address;
//		 uint8_t  command;
//	 };
// } CMD_PACKET;
typedef struct {
	byte crc;
	byte addr0;
	byte addr1;
	byte addr2;
	byte addr3;
	byte cmd;
} CMD_PACKET;

//--- RESPONSE_1
 typedef union
 {
	byte _byte;
	 struct
	 {
		 unsigned IN_IDLE_STATE:1;
		 unsigned ERASE_RESET:1;
		 unsigned ILLEGAL_CMD:1;
		 unsigned CRC_ERR:1;
		 unsigned ERASE_SEQ_ERR:1;
		 unsigned ADDRESS_ERR:1;
		 unsigned PARAM_ERR:1;
		 unsigned B7:1;
	 };
 } RESPONSE_1;

//--- RESPONSE_2
 typedef union
 {
	uint16_t _word;
	 struct
	 {
		byte      _byte0;
		byte      _byte1;
	 };
	 struct
	 {
		 unsigned IN_IDLE_STATE:1;
		 unsigned ERASE_RESET:1;
		 unsigned ILLEGAL_CMD:1;
		 unsigned CRC_ERR:1;
		 unsigned ERASE_SEQ_ERR:1;
		 unsigned ADDRESS_ERR:1;
		 unsigned PARAM_ERR:1;
		 unsigned B7:1;
		 unsigned CARD_IS_LOCKED:1;
		 unsigned WP_ERASE_SKIP_LK_FAIL:1;
		 unsigned ERROR:1;
		 unsigned CC_ERROR:1;
		 unsigned CARD_ECC_FAIL:1;
		 unsigned WP_VIOLATION:1;
		 unsigned ERASE_PARAM:1;
		 unsigned OUTRANGE_CSD_OVERWRITE:1;
	 };
 } RESPONSE_2;

//--- SDC_RESPONSE
 typedef union
 {
	 RESPONSE_1  r1;
	 RESPONSE_2  r2;
 }SDC_RESPONSE;

//--- typSDC_CMD
 typedef struct
 {
	byte    CmdCode;            // the command number
	byte    crc;            	// the CRC value (CRC's are not required once you turn the option off!)
	RESP	responsetype;   	// the Response Type
	byte    moredataexpected;   // True if more data is expected
 } typSDC_CMD;

//--- CID
 typedef union
 {
	 struct
	 {
		dword _u320;
		dword _u321;
		dword _u322;
		dword _u323;
	 };
	 struct
	 {
		byte _byte[16];
	 };
	 struct
	 {
		 unsigned 	NOT_USED :1;
		 unsigned 	CRC_      :7;
		 unsigned 	MDT      :8; // Manufacturing Date Code (BCD)
		 dword 		PSN;    	 // Serial Number (PSN)
		 unsigned 	PRV      :8; // Product Revision
		 char		PNM[6];    	 // Product Name
		 word 		OID;    	 // OEM/Application ID
		 unsigned 	MID      :8; // Manufacture ID
	 };
 } CID;

//--- SDC_Error
 typedef enum
 {
	 sdcValid = 0,				// No error
	 sdcCardInitCommFailure,	// Communication hasnÂ’t been established with the card.
	 sdcCardNotInitFailure,		// Card did not initialize.
	 sdcCardInitTimeout,		// Card initialization timed out.
	 sdcCardTypeInvalid,		// Card type was not able to be defined.
	 sdcCardBadCmd,				// Card did not recognize the command.
	 sdcCardTimeout,			// Card timed out during a read, write or erase sequence.
	 sdcCardCRCError,			// A CRC error occurred during a read.
	 sdcCardDataRejected,		// CRC did not match.
	 sdcEraseTimedOut			// Erase timed out.
 }SDC_Error;

//--- SDCSTATE
 typedef union _SDCstate
 {
	 struct
	 {
		byte isSDMMC : 1;	// set for an SD Card or MultiMediaCard
		byte isWP : 1;		// set if write protected
	 };
	 uint8_t _byte;
 } SDCSTATE;


//--- CSD
typedef union
{
	struct
	{
		dword _u320;
		dword _u321;
		dword _u322;
		dword _u323;
	};
	struct
	{
		uint8_t _byte[16];
	};
} CSD;

//--- FAT16
#define FAT12 1
#define FAT16 2
#define FAT32 3
typedef struct
{
	byte* buffer;	// pointer to a buffer equal to one sector
	dword firsts;	// LBA of the volumeÂ’s first sector
	dword fat;		// LBA of the volumeÂ’s FAT
	dword root;		// LBA of the volumeÂ’s root directory
	dword data;		// LBA of the volumeÂ’s data area
	word maxroot;	// maximum number of entries in the root directory
	dword maxcls;	// maximum number of data clusters in the volume
	word fatsize;	// number of sectors in the FAT
	byte fatcopy;	// number of copies of the FAT
	byte SecPerClus;// number of sectors per cluster
	byte type;		// type of FAT (FAT16, FAT32)
	byte mount;		// TRUE if the media is mounted, FALSE if not mounted)
} DISK;

#define FILE_NAME_SIZE 11

typedef struct
{
	unsigned write :1;			// Set if the file was opened for writing.
	unsigned FileWriteEOF :1;	// Set if writing and have reached the end of the file.
}FileFlags;

#define CLUSTER_FAIL		0xffff
#define LAST_CLUSTER		0xfff8
#define LAST_CLUSTER_FAT16	0xfff8


#define CE_GOOD				0	// No error.
#define CE_BAD_SECTOR_READ	7	// Error in reading a sector.
#define CE_FAT_EOF			60	// Attempt to read beyond the FATÂ’s EOF.
#define CE_INVALID_CLUSTER	9	// The cluster number > maxcls.

#define CLUSTER_EMPTY 0x0000
#define END_CLUSTER 0xFFFE

//---  directory
#define DIR_NAMESIZE 8
#define DIR_EXTENSION 3
//#define NULL 0

#define DIRENTRIES_PER_SECTOR 0x10

#define ATTR_LONG_NAME	0x0f
#define DIR_DEL			0xE5	// deleted entry
#define DIR_EMPTY		0		// all entries that follow are empty
#define NULL			0

//--- file operation
typedef byte CETYPE;
#define CE_GOOD				0	// No error
#define CE_ERASE_FAIL		1	//???  // Internal Card erase failed
#define CE_NOT_INIT			6	// Card isnÂ’t initialized due to an error
#define CE_BAD_SECTOR_READ	7	// Error in reading a sector
#define CE_WRITE_ERROR		8	// CouldnÂ’t write to the sector
#define CE_FILE_NOT_FOUND	10	// CouldnÂ’t find the file
#define CE_DIR_FULL			17	// All of the entries are in use
#define CE_DISK_FULL		18	// All of the clusters are full
#define CE_WRITE_PROTECTED	22	// The card is write protected
#define CE_BADCACHEREAD		28	// Sector read failed
#define CE_EOF				61	// End of file reached

///#define DIR_DEL		0xe5	// deleted entry
#define DIR_EMPTY	0		// last entry in a directory
#define FOUND		0		// directory entry match
#define NOT_FOUND	1		// directory entry not found
#define NO_MORE		2		// no more files found

#define ATTR_MASK	0x3f
#define ATTR_HIDDEN 0x02
#define ATTR_VOLUME 0x08
#define FOUND		0	// directory entry match

#define ATTR_ARCHIVE 0x20
#define DIR_NAMECOMP (DIR_NAMESIZE + DIR_EXTENSION) // 11

//#define RAMread(a, f) *(a + f)

#define  FAIL 0




 SDC_RESPONSE	SendSDCCmd(uint8_t cmd, dword address);
 SDC_Error		CSDRead(void);
 SDC_Error		SectorRead(dword sector_addr,byte* buffer);
 SDC_Error		SectorWrite(dword sector_addr,const byte* buffer);
byte		IsWriteProtected(void);
 void			Delayms(uint8_t milliseconds) ;
 SDC_Error		MediaInitialize(SDCSTATE*);
uint32_t		SDCardSize();
//byte CMD[];//={0xF1,0x67,0xC0,0x40,0x50,0x2B,0xEB,0x81,0x68,0x89,0xAF};

 //------------------------------------------------------------------------------
 //void SocketInitialize(void);
 void OpenSPI(uint8_t FOSC_PRESCALER, uint8_t MODE );
 uint8_t DetectSDCard (void);
 uint8_t WriteSPI(uint8_t data_out);
 uint8_t ReadMedia(void);
 char SpiWriteRead(char data_out);
 void SDC_CS(uint8_t cs);

#endif /* SD_MMC_H_ */
