/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : flash_eeprom.h
	Author       : STMicroelectronics
	               Modified by Faustas Bagdonas
	Date Created : 2016-03
	Purpose      : For EEPROM emulation using Flash memory
	Comments     : This library had important bug(s) which I had to fix (see code)

    POSSIBLE BUGS:
	   1. Implementation could be more flash-friendly but it is good enough.

   ================================================================================= */


/******************** (C) COPYRIGHT 2007 STMicroelectronics, modifikuota ********************
* File Name          : eeprom.h
* Author             : MCD Application Team, minor modification by M. Thomas
* Version            : V1.0.1
* Date               : 10/08/2007 (modified 27. April 2009)
* Description        : This file contains all the functions prototypes for the
*                      EEPROM emulation firmware library.
********************************************************************************
* THE PRESENT SOFTWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH SOFTWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

//For more information see:
// http://www.st.com/content/ccc/resource/technical/document/application_note/ee/ef/d7/87/cb/b7/48/52/CD00165693.pdf/files/CD00165693.pdf/jcr:content/translations/en.CD00165693.pdf

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FLASH_EEPROM_H
#define __FLASH_EEPROM_H

/* Includes ------------------------------------------------------------------*/
#include "hal_flash.h"

/* Exported constants --------------------------------------------------------*/
/* Page size */
#define PAGE_SIZE  (uint16_t)2048  /* 2KByte */

//* EEPROM start address in Flash */
#ifdef MOD_MTHOMAS_STMLIB
extern uint32_t _seemul;
#define EEPROM_START_ADDRESS  ((uint32_t)&_seemul) /* start of emulated EEPROM def'd in linker-script */
/// #define EEPROM_START_ADDRESS  (uint32_t)(0x08000000+((128-2)*1024))
#else
#define EEPROM_START_ADDRESS  (uint32_t)0x0803F000 /* EEPROM emulation start address */
#endif /* MOD_MTHOMAS_STMLIB */

/* Pages 0 and 1 base and end addresses */
#define PAGE0_BASE_ADDRESS  (uint32_t)(EEPROM_START_ADDRESS + (uint16_t)0x0000)
#define PAGE0_END_ADDRESS   (uint32_t)(EEPROM_START_ADDRESS + (PAGE_SIZE - 1))

#define PAGE1_BASE_ADDRESS  (uint32_t)(EEPROM_START_ADDRESS + PAGE_SIZE)
#define PAGE1_END_ADDRESS   (uint32_t)(EEPROM_START_ADDRESS + (2 * PAGE_SIZE - 1))

/* Used Flash pages for EEPROM emulation */
#define PAGE0    (uint16_t)0x0000
#define PAGE1    (uint16_t)0x0001

/* No valid page define */
#define NO_VALID_PAGE    (uint16_t)0x00AB

/* Page status definitions */
#define ERASED             (uint16_t)0xFFFF      /* PAGE is empty */
#define RECEIVE_DATA       (uint16_t)0xEEEE      /* PAGE is marked to receive data */
#define VALID_PAGE         (uint16_t)0x0000      /* PAGE containing valid data */

/* Valid pages in read and write defines */
#define READ_FROM_VALID_PAGE    (uint8_t)0x00
#define WRITE_IN_VALID_PAGE     (uint8_t)0x01

/* Page full define */
#define PAGE_FULL    (uint8_t)0x80

/* Variables' number */
//#define NumbOfVar  (uint8_t)0x03

/* Exported types ------------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void EE_SetVirtualSize(uint16_t VirtSize);
FLASH_Status EE_Format(void);
uint16_t EE_ReadVariable(uint16_t VirtAddress, uint16_t* Read_data);
uint16_t EE_WriteVariable(uint16_t VirtAddress, uint16_t Data);

#endif /* __EEPROM_H */
