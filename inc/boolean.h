//TODO: Sita faila salinti is projekto. Anksciau maniau, kad cia nera <stdbool.h>, taciau yra, tad sito nereik failo
#ifndef __BOOLEAN_H
#define __BOOLEAN_H

#include <stdint.h>

typedef uint8_t BOOL;
#define TRUE	1
#define FALSE	0

#endif
