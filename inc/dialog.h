/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : dialog.h
	Author       : Faustas Bagdonas
	Date Created : 2016-03
	Purpose      : For representation of dialogs
	Comments     : I wrote it according uGUI library. uGUI library has dialog functions
	               as well but its dialogs have very obsolete design (not nice) and
	               lack of controls. As this library is quite similar to uGUI window
	               functions you can look uGUI docs to better understand how does it
	               work but keep in mind that this one has been written from scratch so
	               there are many differences as well. I also added spinbox support

    POSSIBLE BUGS:
	   1. According the function prototypes it looks like I implemented Radio Buttons
	      and CheckBoxes but I don't remember. I THINK THEY WILL NOT WORK!!! Perhaps
	      not full implementation?
   ================================================================================= */

#ifndef __DIALOG_H
#define __DIALOG_H

#include <stdlib.h>
#include <string.h>

#include "ugui.h"
#include "boolean.h"
#include "stmpe811_touch.h"

#define OBJ_TYPE_SPINBOX	2
#define OBJ_TYPE_LABEL		3
#define OBJ_TYPE_RADIO_BUTTON_LIST	4
#define OBJ_TYPE_STANDARD_ICON	5

#define SPBOX_EVENT_VALUE_CHANGED	2
#define CHECKBOX_EVENT_STATUS_CHANGED	3
#define RADIOBUTTONLIST_EVENT_STATUS_CHANGED	4

#define TITLE_HEIGHT	26
#define BUTTON_AREA_HEIGHT	40

//#define MSG_TYPE_DIALOG	100
//#define DLG_EVENT_ACCEPT	101
//#define DLG_EVENT_REJECT	102

//#define DLG_STATE_UPDATE	(1 << 5)

#define DLG_OBJ_STATE_FREE	(1 << 0)
//#define DLG_OBJ_STATE_FREE	(1 << 0)

#define DEFAULT_FONT FONT_8X14

typedef struct{
	uint16_t x, y;
	BOOL isPressed;
} DLG_TS_STATUS;

typedef struct S_DLG_OBJECT	DLG_OBJECT;
typedef struct S_DIALOG DIALOG;

typedef enum
{
	DLG_STATE_SHOWN,
	DLG_STATE_ACCEPT,
	DLG_STATE_REJECT,
	DLG_STATE_CLOSE
} DLG_STATE;

//typedef enum
//{
//	DLG_OBJ_STATE_INIT,
////	OBJ_STATE_FREE,
////	OBJ_STATE_VALID,
////	OBJ_STATE_VISIBLE
//} DLG_OBJ_STATE;

//typedef enum
//{
//	DLG_STATE_VALID,
//	DLG_STATE_UPDATE
//} DLG_STATE;

typedef enum{
	CRITICAL = 1,
	WARNING
} DLG_STANDARD_ICON;

/* Area structure */
typedef struct
{
	int16_t xs;
	int16_t ys;
	int16_t xe;
	int16_t ye;
} DLG_AREA;

typedef struct
{
	uint8_t type;
	uint8_t id;
	uint8_t sub_id;
	uint8_t event;
	void* src;
} DLG_MESSAGE;

struct  S_DIALOG{
	BOOL needUpdate;
	uint8_t objCount;
	DLG_OBJECT *objList;
	uint16_t width;
	uint16_t height;
	char *title;
	void (*callback)(DIALOG *, DLG_MESSAGE *);
	//DLG_TS_STATUS (*getTouchScreenStatus)();
	DLG_STATE dialogState;
};

struct S_DLG_OBJECT{
	BOOL isTouchChanged;
	BOOL isPressed;
	BOOL isFree;
	BOOL isDrawn;
	BOOL needUpdating;
	void (*update)(DIALOG *, DLG_OBJECT *);
	DLG_AREA a_absolute;
	DLG_AREA a_relative;
	uint8_t type;
	uint8_t id;
	uint8_t event;
	void *data;
	uint16_t touch_x, touch_y;
};

typedef struct{
	BOOL isPushed;
	BOOL isActive;
	char *caption;
} DLG_BUTTON;

typedef struct{
	BOOL isActive;
	uint16_t valueCurrent;
	uint16_t valueMin;
	uint16_t valueMax;
	char **textValues;
	BOOL isPushedUp;
	BOOL isPushedDown;

} DLG_SPINBOX;

typedef struct{
	char *text;
	uint32_t textColor;
	const UG_FONT *font;
} DLG_LABEL;

//typedef struct{
//	DLG_STANDARD_ICON_TYPE type;
//} DLG_STANDARD_ICON;

typedef struct{
	char **items;
	uint8_t itemsCount;
	uint8_t currentItem;
} DLG_RADIO_BUTTON_LIST;

typedef struct{
	char *text;
	BOOL isChecked;
	BOOL isActive;
} DLG_CHECKBOX;

BOOL createDialog(DIALOG *dlg, DLG_OBJECT *objList, uint8_t objCount, void (*callback)(DIALOG *, DLG_MESSAGE*) /*, DLG_TS_STATUS (*touchScreenStatusF)()*/ );
void updateDialog(DIALOG *dlg);
void redrawDialog(DIALOG *dlg);
//void deleteDialog(DIALOG *dlg);
//void showDialog(DIALOG *dlg);
//void hideDialog(DIALOG *dlg);

//void setDialogForeColor(DIALOG *dlg, uint32_t foreColor);
//void setDialogBgColor(DIALOG *dlg, uint32_t bgColor);
void setDialogTitleText(DIALOG *dlg, char *title);
void setDialogSize(DIALOG *dlg, uint16_t width, uint16_t height);
//void dialogGetArea(DIALOG *dlg, AREA *area);

//BOOL createButtonInButtonBox(DIALOG *dlg, DLG_BUTTON *button, uint8_t id);
BOOL createButtonsInButtonBox(DIALOG *dlg, uint8_t buttonCount, DLG_BUTTON *buttons, uint8_t *ids, char **captions);
BOOL createButton(DIALOG *dlg, DLG_BUTTON *button, uint8_t id, uint16_t xs, uint16_t ys, uint16_t xe, uint16_t ye);
//void setButtonState(DIALOG *dlg, DLG_BUTTON *button, uint8_t id, BOOL isActive);
//void setButtonForeColor(DIALOG *dlg, DLG_BUTTON *button, uint8_t id, uint8_t foreColor);
//void setButtonBgColor(DIALOG *dlg, DLG_BUTTON *button, uint8_t id, uint8_t bgColor);
BOOL setButtonCaption(DIALOG *dlg, uint8_t id, /*const*/ char *caption); //todo: surasyt "const"

BOOL createStandardIcon(DIALOG *dlg, DLG_STANDARD_ICON *standardIcon, uint8_t id, uint16_t x, uint16_t y);

BOOL createLabel(DIALOG *dlg, DLG_LABEL *label, uint8_t id, uint16_t x, uint16_t y, uint16_t width, uint16_t height);
void setLabelText(DLG_LABEL *label, char *text, uint32_t textColor, const UG_FONT *font);
//BOOL createLabel(DIALOG *dlg, DLG_LABEL *label, uint8_t id, uint16_t x, uint16_t y);
//void setLabelText(DLG_LABEL *label, char *text, uint16_t textColor, const UG_FONT *font);

BOOL createSpinBox(DIALOG *dlg, DLG_SPINBOX *spinBox, uint8_t id, uint16_t x, uint16_t y);
void setSpinBoxInterval(DLG_SPINBOX *spinBox, uint16_t min, uint16_t max);
void setSpinBoxValue(DLG_SPINBOX *spinBox, uint16_t value);
void setSpinBoxTextItems(DLG_SPINBOX *spinBox, /*const*/ char **items, uint8_t itemCount);

BOOL createRadioButtonList(DIALOG *dlg, DLG_RADIO_BUTTON_LIST *radioButtonList, uint8_t id, uint16_t x, uint16_t y, char **items, uint8_t itemsCount);
int8_t RadioButtonList_getCurrentIndex(DLG_RADIO_BUTTON_LIST *radioButtonList);
void RadioButtonList_getCurrentText(DLG_RADIO_BUTTON_LIST *radioButtonList, char *currentText);
BOOL RadioButtonList_setIndex(DLG_RADIO_BUTTON_LIST *radioButtonList, uint8_t index);

BOOL createCheckBox(DIALOG *dlg, DLG_CHECKBOX *checkBox, uint8_t id, uint16_t x, uint16_t y, char *text);
void CheckBox_setCheckState(DLG_CHECKBOX *checkBox, BOOL isChecked);
BOOL CheckBox_isChecked(DLG_CHECKBOX *checkBox);

void getDialogArea(DIALOG *dlg, DLG_AREA *area);

void setDialogState(DIALOG *dlg, DLG_STATE state);
DLG_STATE getDialogState(DIALOG *dlg);
DLG_OBJECT *getDialogObjectById(DIALOG *dlg, uint8_t id);

void msgBox(char *msg, char *title, DLG_STANDARD_ICON standardIcon, bool (*userFunction)());

#endif
