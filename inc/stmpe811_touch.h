/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : stmpe811_touch.h
	Author       : Faustas Bagdonas
	Date Created : 2016
	Purpose      : STMPE811 touchscreen controller driver library.
	Comments     :

   ================================================================================= */

#ifndef _STMPE811_TOUCH_H_
#define _STMPE811_TOUCH_H_

#include <stdint.h>
#include <stdbool.h>
#include <stm32f303xc.h>
#include "hal_i2c.h"
#include "delay.h"

#define I2C	I2C2
#define STMPE_DEFAULT_X_DIVIDER	(3900 << 4) / 480
#define STMPE_DEFAULT_Y_DIVIDER	(3800 << 4) / 272
#define STMPE_DEFAULT_X_OFFSET	0
#define STMPE_DEFAULT_Y_OFFSET	0

typedef struct STMPE_STATUS{
	uint16_t x0, y0, x1, y1;
	bool isPressed;
	//uint16_t xTouchController, yTouchController;
	uint16_t xDivider, yDivider;
	int16_t xOffset, yOffset;
} STMPE_STATUS;

void STMPE_Init();
void STMPE_SetDefaultCalibrationData();
void STMPE_PressCheck();
void STMPE_ReleaseCheck();
uint16_t STMPE_getX0();
uint16_t STMPE_getY0();
uint16_t STMPE_getX();
uint16_t STMPE_getY();
void STMPE_resetX0();
void STMPE_resetY0();
bool STMPE_isPressed();
void STMPE_forceUnpress();
void STMPE_forceSetX0(uint16_t x);
void STMPE_forceSetY0(uint16_t y);

void STMPE_setOffsetX(int16_t x);
void STMPE_setOffsetY(int16_t y);
void STMPE_setDividerX(uint16_t x);
void STMPE_setDividerY(uint16_t y);

#endif
