/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : ext_adc.h
	Author       : Faustas Bagdonas
	Date Created : 2016-03
	Purpose      : External 16 bit ADC library
	Comments     :

    KNOWN BUGS:
	   1. Functions ExtADC_startSingleBlocking() and ExtADC_startSingle() are buggy
	      as well as they need more testing!

   ================================================================================= */

#ifndef _EXT_ADC_H_
#define _EXT_ADC_H_
     
#include <stdbool.h>
#include <stdint.h>
#include "stm32f303xc.h"
#include "boolean.h"

//ADC connection: PA5 is CLK, GPIOE - 16 bit data

#define ADC_MAX_SAMPLES	15000
#define	CONTROL_PORT	  GPIOB
#define POWER_DOWN_PIN	11
#define NORMAL_PIN		  2
#define WARP_PIN		    5

//#define EXTADC_BUFFER_POSITION() 	(ADC_MAX_SAMPLES - DMA1_Channel1->CNDTR)
//#define EXTADC_BUFFER_POSITION() 	((DMA1_Channel1->CNDTR == ADC_MAX_SAMPLES) ? 0 : (ADC_MAX_SAMPLES - DMA1_Channel1->CNDTR))
//#define EXTADC_BUFFER_POSITION() 	((DMA1_Channel1->CNDTR == 0) ? (ADC_MAX_SAMPLES - 1) : (ADC_MAX_SAMPLES - DMA1_Channel1->CNDTR - 1))
#define EXTADC_GET_PRESCALER	(TIM3->PSC)
#define EXTADC_POWERDOWN()		(CONTROL_PORT->ODR |= (1 << POWER_DOWN_PIN))
#define EXTADC_POWERUP()		  (CONTROL_PORT->ODR &= ~(1 << POWER_DOWN_PIN))
#define EXTADC_NORMAL()			  (CONTROL_PORT->ODR &= ~((1 << NORMAL_PIN) | (1 << WARP_PIN)))
#define EXTADC_WARP()			    (CONTROL_PORT->ODR |= ((1 << NORMAL_PIN) | (1 << WARP_PIN)))
#define EXTADC_IN_PROGRESS()	(DMA1_Channel1->CNDTR > 0)

void initExtADC();
//BOOL ExtADC_setActiveBufferSize(uint16_t samples);
uint16_t ExtADC_getActiveBufferSize();
void ExtADC_changeClkPrescaler(uint32_t prescaler);
void ExtADC_stop();
void ExtADC_clearBuffer();

void ExtADC_startSingleBlocking(uint16_t samplesCount);
void ExtADC_startSingle(uint16_t samplesCount);
void ExtADC_startCircular();
BOOL ExtADC_transfered();

uint16_t EXTADC_BUFFER_POSITION();
uint16_t EXTADC_BUFFER_POSITION2();

extern volatile bool g_isExtAdcInited;
#endif
