/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : i2c_eeprom.h
	Author       : Faustas Bagdonas
	Date Created : 2016-03
	Purpose      : Standard I2C EEPROM driver library.
	Comments     : It is used to obtain a calibration data from the sensor's EEPROM
	               and to write custom offset on it.

   ================================================================================= */

#ifndef _I2C_EEPROM_H_
#define _I2C_EEPROM_H_

#include <stdbool.h>
#include "hal_i2c.h"

#define TIMEOUT	5

uint8_t I2C_EEPROM_readByte(I2C_TypeDef *i2c, uint8_t i2c_address, uint16_t address);
bool I2C_EEPROM_read(I2C_TypeDef *i2c, uint8_t i2c_address, uint16_t firstAddress, uint8_t *buffer, uint8_t count);
bool I2C_EEPROM_writeByte(I2C_TypeDef *i2c, uint8_t i2c_address, uint16_t address, uint8_t byte);
bool I2C_EEPROM_readSerial(I2C_TypeDef *i2c, uint8_t i2c_address, uint8_t *serial); //Serial length is 16B
//TODO: perhaps in the future this meter will have to write more data into EEPROM. Then realize EEPROM page write function!!!

#endif
