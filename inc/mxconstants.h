/* Includes ------------------------------------------------------------------*/

#define EXTADC_GPIO_Port GPIOE

#define TFT_DATA_GPIO_Port GPIOD
#define TFT_CONTROL_GPIO_Port GPIOC
#define TFT_RST__Pin 0
#define TFT_CS__Pin 1
#define TFT_D_C__Pin 2
#define TFT_RD__Pin 3
#define TFT_WR__Pin 4
#define TFT_TE_Pin 5

#define VBAT_Pin 0
#define VBAT_GPIO_Port GPIOA
#define ADC_MAGNET_ALT_Pin 4
#define ADC_MAGNET_ALT_GPIO_Port GPIOA

//#define LED0_Pin 6
//#define LED0_GPIO_Port GPIOC
//#define LED1_Pin 7
//#define LED1_GPIO_Port GPIOC
//#define BUZZER_Pin 8
//#define BUZZER_GPIO_Port GPIOC
//#define POWER_ON_OFF_Pin 13
//#define POWER_ON_OFF_GPIO_Port GPIOC

#define TP_INT_Pin 10
#define TP_INT_GPIO_Port GPIOB
#define TP_SCL_Pin 9
#define TP_SCL_GPIO_Port GPIOA
#define TP_SDA_Pin 10
#define TP_SDA_GPIO_Port GPIOA

#define DS_NSS_Pin 15
#define DS_NSS_GPIO_Port GPIOA

#define SD_SCK_Pin 10
#define SD_SCK_GPIO_Port GPIOC
#define SD_MISO_Pin 11
#define SD_MISO_GPIO_Port GPIOC
#define SD_MOSI_Pin 12
#define SD_MOSI_GPIO_Port GPIOC
#define SD_EXIST_Pin 15
#define SD_EXIST_GPIO_Port GPIOB

#define TEMP_SCL_Pin 6
#define TEMP_SCL_GPIO_Port GPIOB
#define TEMP_SDA_Pin 7
#define TEMP_SDA_GPIO_Port GPIOB

#define BUTTON_REC_Pin 8
#define BUTTON_REC_GPIO_Port GPIOB
#define BUTTON_RESERVED_Pin 9
#define BUTTON_RESERVED_GPIO_Port GPIOB

#define EXT_TRIGGER_Pin 11
#define EXT_TRIGGER_GPIO_Port GPIOB
