/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : hal_flash.h
	Author       : Faustas Bagdonas, according STMicroelectronics HAL libraries
	Date Created : 2016-03
	Purpose      : For MCU internal flash manipulation
	Comments     :

   ================================================================================= */

//Parengta remiantis HAL flash funkcijomis
#ifndef _FLASH_H_
#define _FLASH_H_

#define PAGE_SIZE	2048
#define ATTEMPTS	4000000

#include "stm32f303xc.h"
#include "delay.h"

typedef enum{FLASH_ERROR, FLASH_COMPLETE} FLASH_Status;

void FLASH_unlock();
void FLASH_lock();
FLASH_Status FLASH_erasePage(uint32_t pageAddress);
FLASH_Status FLASH_programHalfWord(uint32_t address, uint16_t data);

#endif
