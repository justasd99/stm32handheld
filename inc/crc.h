/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : crc.h
	Author       : Faustas Bagdonas
	Date Created : 2016-03
	Purpose      : For CRC calculation hardware
	Comments     :

   ================================================================================= */

#ifndef _CRC_H_
#define _CRC_H_

#include <stm32f303xc.h>

void CRC32_init();
uint32_t CRC32_calculate(uint8_t *string, uint16_t length);

void CRC32_reset();
void CRC32_add(uint32_t value);
uint32_t CRC32_result();

#endif
