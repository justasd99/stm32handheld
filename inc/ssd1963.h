/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : ssd1963.h
	Author       : Faustas Bagdonas
	Date Created : 2016
	Purpose      : SSD1963 controller driver library.
	Comments     : SSD1963 is designed for parallel color LCD

   ================================================================================= */

#ifndef _SSD1963_H_
#define _SSD1963_H_

#include <stdint.h>
#include "stm32f303xc.h"
#include "delay.h"

/* Config */
#define SSD1963_DATA_BUS	GPIOD
#define SSD1963_CONTROL_BUS	GPIOC
#define SSD1963_SIGNAL_RESET	0x02 /*0x01*/
#define SSD1963_SIGNAL_CS	0x01 /* 0x02 */
#define SSD1963_SIGNAL_DC	0x04
#define SSD1963_SIGNAL_RD	0x08
#define SSD1963_SIGNAL_WR	0x10
#define SSD1963_SIGNAL_TE	0x20

#define TFT_HORIZONTAL_RESOLUTION	480
#define TFT_VERTICAL_RESOLUTION		272

/* Komandos */
#define SSD1963_NOP					0x00
#define SSD1963_SOFT_RESET			0x01
#define SSD1963_GET_POWER_MODE		0x0A
#define SSD1963_GET_ADDRESS_MODE	0x0B
#define SSD1963_GET_PIXEL_FORMAT	0x0C
#define SSD1963_GET_DISPLAY_MODE	0x0D
#define SSD1963_GET_SIGNAL_MODE		0x0E
#define SSD1963_ENTER_SLEEP_MODE	0x10
#define SSD1963_EXIT_SLEEP_MODE		0x11
#define SSD1963_ENTER_PARTIAL_MODE	0x12
#define SSD1963_ENTER_NORMAL_MODE	0x13
#define SSD1963_EXIT_INVERT_MODE	0x20
#define SSD1963_ENTER_INVERT_MODE	0x21
#define SSD1963_SET_GAMMA_CURVE		0x26
#define SSD1963_SET_DISPLAY_OFF		0x28
#define SSD1963_SET_DISPLAY_ON		0x29
#define SSD1963_SET_COLUMN_ADDRESS	0x2A
#define SSD1963_SET_PAGE_ADDRESS	0x2B
#define SSD1963_WRITE_MEMORY_START	0x2C
#define SSD1963_READ_MEMORY_START	0x2E
#define SSD1963_SET_PARTIAL_AREA	0x30
#define SSD1963_SET_SCROLL_AREA		0x33
#define SSD1963_SET_TEAR_OFF		0x34
#define SSD1963_SET_TEAR_ON			0x35
#define SSD1963_SET_ADDRESS_MODE	0x36
#define SSD1963_SCROLL_START		0x37
#define SSD1963_EXIT_IDLE_MODE		0x38
#define SSD1963_ENTER_IDLE_MODE		0x39
#define SSD1963_SET_PIXEL_FORMAT	0x3A
#define SSD1963_WRITE_MEMORY_CONTINUE	0x3C
#define SSD1963_READ_MEMORY_CONTINUE	0x3E
#define SSD1963_SET_TEAR_SCANLINE		0x44
#define SSD1963_GET_SCANLINE	0x45
#define SSD1963_READ_DDB		0xA1
#define SSD1963_SET_LCD_MODE	0xB0
#define SSD1963_GET_LCD_MODE	0xB1
#define SSD1963_SET_HORI_PERIOD	0xB4
#define SSD1963_GET_HORI_PERIOD	0xB5
#define SSD1963_SET_VERT_PERIOD	0xB6
#define SSD1963_GET_VERT_PERIOD	0xB7
#define SSD1963_SET_GPIO_CONF	0xB8
#define SSD1963_GET_GPIO_CONF	0xB9
#define SSD1963_SET_GPIO_VALUE	0xBA
#define SSD1963_SET_GPIO_STATUS	0xBB
#define SSD1963_SET_POST_PROC	0xBC
#define SSD1963_GET_POST_PROC	0xBD
#define SSD1963_SET_PWM_CONF	0xBE
#define SSD1963_GET_PWM_CONF	0xBF
#define SSD1963_SET_LCD_GEN0	0xC0
#define SSD1963_GET_LCD_GEN0	0xC1
#define SSD1963_SET_LCD_GEN1	0xC2
#define SSD1963_GET_LCD_GEN1	0xC3
#define SSD1963_SET_LCD_GEN2	0xC4
#define SSD1963_GET_LCD_GEN2	0xC5
#define SSD1963_SET_LCD_GEN3	0xC6
#define SSD1963_GET_LCD_GEN3	0xC7
#define SSD1963_SET_GPIO0_ROP	0xC8
#define SSD1963_GET_GPIO0_ROP	0xC9
#define SSD1963_SET_GPIO1_ROP	0xCA
#define SSD1963_GET_GPIO1_ROP	0xCB
#define SSD1963_SET_GPIO2_ROP	0xCC
#define SSD1963_GET_GPIO2_ROP	0xCD
#define SSD1963_SET_GPIO3_ROP	0xCE
#define SSD1963_GET_GPIO3_ROP	0xCF
#define SSD1963_SET_DBC_CONF	0xD0
#define SSD1963_GET_DBC_CONF	0xD1
#define SSD1963_SET_DBC_TH		0xD4
#define SSD1963_GET_DBC_TH		0xD5
#define SSD1963_SET_PLL			0xE0
#define SSD1963_SET_PLL_MN		0xE2
#define SSD1963_GET_PLL_MN		0xE3
#define SSD1963_GET_PLL_STATUS	0xE4
#define SSD1963_SET_DEEP_SLEEP	0xE5
#define SSD1963_SET_LSHIFT_FREQ	0xE6
#define SSD1963_GET_LSHIFT_FREQ	0xE7
#define SSD1963_SET_PIXEL_DATA_INTERFACE	0xF0
#define SSD1963_GET_PIXEL_DATA_INTERFACE	0xF1

void SSD_Init();
void SSD_AdjustBrightness(uint8_t brightness);
void SSD_SetPixel(uint16_t x, uint16_t y, uint32_t color);
void SSD_SetArea(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);

#define GPIO_Write(GPIOx, data) (GPIOx->ODR = data)
#define GPIO_ResetBits(GPIOx, GPIO_Pin) (GPIOx->BSRRH = GPIO_Pin)
#define GPIO_SetBits(GPIOx, GPIO_Pin) (GPIOx->BSRRL = GPIO_Pin)

void SSD_WriteCommand(uint8_t command);
void SSD_WriteData(unsigned int data);

void SSD_fillScreen(uint32_t color);
void SSD_drawSquare(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint32_t color);

#endif
