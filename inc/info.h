/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : info.h
	Author       : Faustas Bagdonas
	Date Created : 2016-03
	Purpose      : Information menu item.
	Comments     : Not completed!!! Much more info should be added.

   ================================================================================= */

#ifndef _INFO_H_
#define _INFO_H_

#include <stdbool.h>
#include <stdint.h>
//#include <stdlib.h>

#include "ugui.h"
#include "delay.h"
#include "calibration.h"
#include "stmpe811_touch.h"

void information(const int16_t *temperature, int adc_offset);

#endif
