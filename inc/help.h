/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : help.h
	Author       : Faustas Bagdonas
	Date Created : 2016-03
	Purpose      : Help menu item. For help.
	Comments     : Currently removed (from 2017 November)

   ================================================================================= */

#ifndef HELP_H_
#define HELP_H_

#include "ugui.h"
#include "stmpe811_touch.h"

void help();

#endif /* HELP_H_ */
