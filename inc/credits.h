/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : credits.h
	Author       : Faustas Bagdonas
	Date Created : 2017-04-28
	Purpose      : To inform user who developed this device.

	WARNING: THIS FILE IS THE MOST IMPORTANT IN THIS WHOLE FIRMWARE! YOU CAN ADD
	         MORE PEOPLE WHO REALLY WORKED ON THIS PROJECT THERE BUT YOU MUST NOT
	         DELETE ANYONE FROM THE LIST!

	DEMESIO: TAI YRA PATS SVARBIAUSIAS FAILAS VISAME PROJEKTE! GALIMA CIA PRIRASYTI
	         DAUGIAU ZMONIU, KURIE IS TIESU (!) DIRBO TIES SIUO PROJEKTU, BET NEGALIMA
	         NIEKO PASALINTI!

   ================================================================================= */

#include <stdbool.h>
#include "ugui.h"
#include "stmpe811_touch.h"

#ifndef CREDITS_H_
#define CREDITS_H_

void showCredits();

#endif /* CREDITS_H_ */
