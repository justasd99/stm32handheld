/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : delay.h
	Author       : Faustas Bagdonas
	Date Created : 2016-03
	Purpose      : For creating delays
	Comments     : delay_ms() needs systick handler which is called each ms.

	POSSIBLE BUGS:
	   1. delay_us() has very big error (perhaps >20%) so would be good idea to overlook
	      timer settings.

   ================================================================================= */

#ifndef _DELAY_H_
#define _DELAY_H_

#include <stdint.h>
#include <stdbool.h>
#include "stm32f303xc.h"

#define TIMER		TIM2 /* if you are going to change this then you must also modify RCC->APB1ENR value (next line)! */
#define RCC_SETUP	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN
#define PRESCALER	47 /* Change to value which is necessary for counter increment exactly by one per 1 us  */
#define COUNTER_REGISTER_SIZE	32

#if COUNTER_REGISTER_SIZE != 16 && COUNTER_REGISTER_SIZE != 32
#error "Counter reg. size must be 16 or 32 bits"
#endif

void delayInit();
void delayDeInit();

#if COUNTER_REGISTER_SIZE == 32
	void delay_us(uint32_t delay);
#elif COUNTER_REGISTER_SIZE == 16
	void delay_us(uint16_t delay);
#endif

void increaseMilisecondTick();
uint32_t milisecondTick();
void delay_ms(uint32_t delay);

#endif
