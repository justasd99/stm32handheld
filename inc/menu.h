/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : menu.h
	Author       : Faustas Bagdonas
	Date Created : 2016-03
	Purpose      : Menu implementation
	Comments     :

   ================================================================================= */

#ifndef _MENU_H_
#define _MENU_H_

#include <stdint.h>
#include <stm32f303xc.h>

#include "ugui.h"
#include "boolean.h" //TODO: <stdbool.h> naudot
#include "stmpe811_touch.h"
//For credits:
#include "delay.h"
#include "credits.h"
     



typedef struct{
				char *name;
				//uint8_t *icon;
				void (*exec)(void);
			  } MENU_ITEM;

typedef struct{
				char *title;
				MENU_ITEM items[8];
			  } MENU;

enum button{BUTTON_LEFT, BUTTON_RIGHT, BUTTON_UP, BUTTON_DOWN, BUTTON_OK, BUTTON_REC, BUTTON_NONE};
enum selection{MENU_ITEM1, MENU_ITEM2, MENU_ITEM3, MENU_ITEM4, MENU_ITEM5, MENU_ITEM6, MENU_ITEM7, MENU_ITEM8, MENU_QUIT};

typedef struct{
				enum button button;
				BOOL isPressed;
			  } BUTTON_STATUS;
typedef struct{
				uint16_t x, y;
				uint16_t xTouchController, yTouchController;
				BOOL isPressed;
			  } TS_STATUS;

typedef struct{
				uint16_t xs, ys, xe;
			  } ITEM_COORDS;

void menu(MENU *pMenu, volatile BUTTON_STATUS *pButtonStatus /*, TS_STATUS *p_tsStatus*/);
int8_t drawPopupMenu(uint16_t x, uint16_t y, const char **itemList, uint8_t itemCount /*, volatile TS_STATUS *p_tsStatus*/);

#endif
