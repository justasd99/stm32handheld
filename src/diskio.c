/*-----------------------------------------------------------------------*/
/* Low level disk I/O module for FatFs (glue functions)                  */
/*-----------------------------------------------------------------------*/

#include "diskio.h"		/* FatFs lower layer API */
#include "sd_card.h"
#include "hal_rtc.h"

#define SUPPRESS_UNUSED_WARNING(x) (void)(x)

static SDCSTATE Flag;
//static
//volatile uint32_t g_offset;
//static
volatile uint32_t g_totalSectors;

//static bool getFirstPartitionInfo(){
//	char sector[512];
//	volatile uint8_t id;
//	if(SectorRead(0, sector) == sdcValid){
//		id = sector[0x1BE + 4];
//		if(id == 6 || id == 0x0C){ //Ar FAT16 arba FAT32 LBA?
//			g_offset = sector[0x1BE + 8] | sector[0x1BE + 9] << 8 | sector[0x1BE + 0x0A] << 16 | sector[0x1BE + 0x0B] <<  24;
//			g_totalSectors = sector[0x1BE + 0x0C] | sector[0x1BE + 0x0D] << 8 | sector[0x1BE + 0x0E] << 16 | sector[0x1BE + 0x0F] <<  24;
//			return true;
//		}
//	}
//	return false;
//}

/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/
DSTATUS disk_status (BYTE vol){
	SUPPRESS_UNUSED_WARNING(vol);
	return 0; //TODO: jei neinicializuotas, turetu grazint NOINIT
}

/* =================================================================================

	Project      : Magnetic Field Meter
	File Name    : diskio.c
	Author       : Adopted template by Faustas Bagdonas
	Date Created : 2016
	Purpose      : Low level SD card functions wrapper for FatFS library
	Comments     :

   ================================================================================= */


/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/
DSTATUS disk_initialize(BYTE vol){
	//volatile SDC_Error result;
	SUPPRESS_UNUSED_WARNING(vol);
	if(MediaInitialize(&Flag) == sdcValid){
//		if(getFirstPartitionInfo()){
			return 0;
//		}
	}
	return STA_NOINIT;
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/
/* buff - Data buffer to store read data */
/* sector - Start sector in LBA */
/* count - Number of sectors to read */
DRESULT disk_read (BYTE vol, BYTE *buff, DWORD sector, UINT count){
	int bufPos = 0;
	int bufSize = 512 * count;

	SUPPRESS_UNUSED_WARNING(vol);

	for(bufPos = 0; bufPos < bufSize; bufPos+=512){
		if(SectorRead(sector, &buff[bufPos]) != sdcValid){
			return RES_ERROR;
		}
	}
	return RES_OK;
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/
/* buff - Data to be written */
/* sector - Start sector in LBA */
/* Number of sectors to write */
DRESULT disk_write (BYTE vol, const BYTE *buff,	DWORD sector, UINT count){
	int bufPos = 0;
	int bufSize = 512 * count;

	SUPPRESS_UNUSED_WARNING(vol);

	for(bufPos = 0; bufPos < bufSize; bufPos+=512){
		if(SectorWrite(sector, &buff[bufPos]) != sdcValid){
			return RES_ERROR;
		}
	}
	return RES_OK;
}



/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/
/* cmd - Control code */
/* buff - Buffer to send/receive control data */
DRESULT disk_ioctl (BYTE vol, BYTE cmd, void *buff){
	SUPPRESS_UNUSED_WARNING(vol);

	switch(cmd){
		case CTRL_SYNC: return RES_OK;
		case GET_SECTOR_COUNT: {
				*((uint32_t *)buff) = SDCardSize();
				return RES_OK;
			}; break;
		case GET_SECTOR_SIZE: {
				*((uint32_t *)buff) = 512;
				return RES_OK;
			}; break;
		case GET_BLOCK_SIZE: return RES_OK;
	}

	return RES_PARERR;
}

DWORD get_fattime (void){
	DATE date = RTC_getDate();
	TIME time = RTC_getTime();
	return ((date.year + 20) << 25) | (date.month << 21) | (date.day << 16) | (time.hours << 11) | (time.minutes << 5) | (time.seconds / 2);
}
