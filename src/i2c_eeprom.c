#include "i2c_eeprom.h"

uint8_t I2C_EEPROM_readByte(I2C_TypeDef *i2c, uint8_t i2c_address, uint16_t address){
	uint8_t received = 0;
	if(I2C_sendBytes(i2c, i2c_address, (uint8_t*)&address, 2, TIMEOUT)){
		I2C_receiveBytes(i2c, i2c_address, &received, 1, TIMEOUT);
	}
	return received;
}

bool I2C_EEPROM_read(I2C_TypeDef *i2c, uint8_t i2c_address, uint16_t firstAddress, uint8_t *buffer, uint8_t count){
	bool status = false;
	firstAddress = (firstAddress>>8) | (firstAddress<<8);
	if(I2C_sendBytes(i2c, i2c_address, (uint8_t*)&firstAddress, 2, TIMEOUT)){
		status = I2C_receiveBytes(i2c, i2c_address, buffer, count, TIMEOUT);
	}
	return status;
}

bool I2C_EEPROM_writeByte(I2C_TypeDef *i2c, uint8_t i2c_address, uint16_t address, uint8_t byte){
	uint8_t buffer[3];
	bool status = false;
	buffer[0] = (uint8_t)(address >> 8);
	buffer[1] = (uint8_t)address;
	buffer[2] = byte;
	if(I2C_sendBytes(i2c, i2c_address, buffer, 3, TIMEOUT)){
		delay_ms(5);
		status = true;
	}
	return status;
}

//Serial number length is 16B. So buffer must be at lease 16B length
bool I2C_EEPROM_readSerial(I2C_TypeDef *i2c, uint8_t i2c_address, uint8_t *serial){
	bool status = false;
	uint8_t queryBuffer[2];
	queryBuffer[0] = 0x80;
	queryBuffer[1] = 0x00;
	if(I2C_sendBytes(i2c, i2c_address | 0x08, queryBuffer, 2, TIMEOUT)){
		status = I2C_receiveBytes(i2c, i2c_address, serial, 16, TIMEOUT);
	}
	return status;
}
