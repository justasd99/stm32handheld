#include "hal_i2c.h"

static bool locked;

    /* GPIO has to be initialized in GPIO initialization funtion */
	/* Code should be something like that (if we use STM32F303's first I2C module):
    RCC -> AHBENR |= RCC_AHBENR_GPIOBEN;
    GPIOB -> OSPEEDR &= ~GPIO_OSPEEDER_OSPEEDR6 | ~GPIO_OSPEEDER_OSPEEDR7;
    GPIOB -> MODER |= GPIO_MODER_MODER6_1 | GPIO_MODER_MODER7_1;
    GPIOB -> OTYPER |= GPIO_OTYPER_OT_6 | GPIO_OTYPER_OT_7;
    GPIOB -> AFR[0] |= (4 << 24) | (4 << 28);
    */

	/* Clock for I2C peripheral should be enabled in CLOCK initialization function */
	/* Code should look like that (if we use STM32F303's first I2C module):
    RCC->APB1ENR |= RCC_APB1ENR_i2cEN ;
    */


void I2C_setMode(I2C_TypeDef *i2c, enum Mode mode){
	//i2c->CR1 |= I2C_CR1_ANFOFF; //Analog noise filter DISABLED
	if(mode == STANDARD_10){
		//Selected STANDARD MODE (10 kHz):
		i2c->TIMINGR |= (0x01 << 28); // (PRESC = 0)
		i2c->TIMINGR |= (0x04 << 20); //SCL delay
		i2c->TIMINGR |= (0x02 << 16); //SDA delay
		i2c->TIMINGR |= (0x0F << 8); //SCL high
		i2c->TIMINGR |= 0x13; //SCL low
	}
	else if(mode == STANDARD_100){
		//Selected STANDARD MODE (100 kHz):
		i2c->TIMINGR |= (0x01 << 28); // (PRESC = 0)
		i2c->TIMINGR |= (0x04 << 20); //SCL delay
		i2c->TIMINGR |= (0x02 << 16); //SDA delay
		i2c->TIMINGR |= (0x0F << 8); //SCL high
		i2c->TIMINGR |= 0x13; //SCL low
	}
	else if(mode == FAST){
		//Selected FAST MODE (400 kHz):
		//No prescaler (PRESC = 0)
		i2c->TIMINGR |= (0x03 << 20); //SCL delay
		i2c->TIMINGR |= (0x01 << 16); //SDA delay
		i2c->TIMINGR |= (0x03 << 8); //SCL high
		i2c->TIMINGR |= 0x09; //SCL low
	}
	else if(mode == FAST_PLUS){
		//Selected FAST MODE (500 kHz):
		//No prescaler (PRESC = 0)
		i2c->TIMINGR |= (0x01 << 20); //SCL delay
		i2c->TIMINGR |= (0x01 << 16); //SDA delay
		i2c->TIMINGR |= (0x03 << 8); //SCL high
		i2c->TIMINGR |= 0x06; //SCL low
	}
	//i2c->ISR |= I2C_ISR_TXE;
	locked = false;
}

bool I2C_sendByte(I2C_TypeDef *i2c, uint8_t address, uint8_t data, uint16_t timeout) {
	uint16_t try;
	const uint16_t maxTry = MAX_TRY;

	try = 0;
	while((i2c->ISR & I2C_ISR_BUSY) || locked){
		try++;
		if(try == maxTry){
			if(timeout-- == 0){
				return false;
			}
			try = 0;
			delay_us(1000);
		}
	}
	locked = true;

	i2c->CR2 &= ~0x3FF;
    i2c->CR2 |= (address << 1);
    i2c->CR1 |= I2C_CR1_PE; //Peripheral enable
    i2c->CR2 &= ~(I2C_CR2_RD_WRN); //Request write transfer
    i2c->CR2 &= ~(0xff << 16);
    i2c->CR2 |= I2C_CR2_START |  (1 << 16);  //Start transmission, we have to transfer only one byte
    try = 0;
    while(i2c->CR2 & I2C_CR2_START){ //Wait until start is generated
    	try++;
    	if(try == maxTry){
    		if(timeout-- == 0){
    			i2c->CR1 &= ~I2C_CR1_PE;
    			locked = false;
    			return false;
    		}
    		try = 0;
    		delay_us(1000);
    	}
    }
	i2c->TXDR = data;
	try = 0;
	while (!(i2c->ISR & I2C_ISR_TXE)){
		try++;
		if(try == maxTry){
			if(timeout-- == 0){
				i2c->CR1 &= ~I2C_CR1_PE;
				locked = false;
				return false;
			}
			try = 0;
			delay_us(1000);
		}
	}
    i2c->CR2 |= I2C_CR2_STOP;
    try = 0;
    while(i2c->CR2 & I2C_CR2_STOP){
    	try++;
		if(try == maxTry){
			if(timeout-- == 0){
				i2c->CR1 &= ~I2C_CR1_PE;
				locked = false;
				return false;
			}
			try = 0;
			delay_us(1000);
		}
    }
    i2c->CR1 &= ~I2C_CR1_PE;
    locked = false;
    return true;
}

bool I2C_sendBytes(I2C_TypeDef *i2c, uint8_t address, const uint8_t *data, uint8_t length, uint16_t timeout) {
	uint16_t try;
	const uint16_t maxTry = MAX_TRY;
	uint8_t count;

	try = 0;
	while((i2c->ISR & I2C_ISR_BUSY) || locked){
		try++;
		if(try == maxTry){
			if(timeout-- == 0){
				return false;
			}
			try = 0;
			delay_us(1000);
		}
	}
	locked = true;

	i2c->CR2 &= ~0x3FF;
    i2c->CR2 |= (address << 1);
    i2c->CR1 |= I2C_CR1_PE; //Peripheral enable
    i2c->CR2 &= ~(I2C_CR2_RD_WRN); //Request write transfer
    i2c->CR2 &= ~(0xff << 16);
    i2c->CR2 |= I2C_CR2_START |  (length << 16);  //Start transmission
    try = 0;
    while(i2c->CR2 & I2C_CR2_START){ //Wait until start is generated
    	try++;
		if(try == maxTry){
			if(timeout-- == 0){
				i2c->CR1 &= ~I2C_CR1_PE;
				locked = false;
				return false;
			}
			try = 0;
			delay_us(1000);
		}
    }
    for(count = 0; count < length; count++){
		i2c->TXDR = data[count]; //
		try = 0;
		while (!(i2c->ISR & I2C_ISR_TXE)){
			try++;
			if(try == maxTry){
				if(timeout-- == 0){
					i2c->CR1 &= ~I2C_CR1_PE;
					locked = false;
					return false;
				}
				try = 0;
				delay_us(1000);
			}
		}
    }
    i2c->CR2 |= I2C_CR2_STOP;
    try = 0;
    while(i2c->CR2 & I2C_CR2_STOP){
    	try++;
		if(try == maxTry){
			if(timeout-- == 0){
				i2c->CR1 &= ~I2C_CR1_PE;
				locked = false;
				return false;
			}
			try = 0;
			delay_us(1000);
		}
    }
    i2c->CR1 &= ~I2C_CR1_PE;
    locked = false;
    return true;
}

bool I2C_receiveBytes(I2C_TypeDef *i2c, uint8_t address, uint8_t *buffer, uint16_t count, uint16_t timeout) {
	uint16_t try;
	const uint16_t maxTry = MAX_TRY;
	uint8_t nbyteCurrent;
    //uint8_t data = 0;
    uint8_t current = 0;

    try = 0;
    while((i2c->ISR & I2C_ISR_BUSY) || locked){
    	try++;
		if(try == maxTry){
			if(timeout-- == 0){
				return false;
			}
			try = 0;
			delay_us(1000);
		}
	}
	locked = true;

    i2c->CR2 &= ~0x3FF;
    i2c->CR2 |= (address << 1)	;
    i2c->CR1 |= I2C_CR1_PE; //Peripheral enable
    i2c->CR2 &= ~(I2C_CR2_RD_WRN);
    i2c->CR2 |= I2C_CR2_RD_WRN;
    i2c->CR2 &= ~(0xff << 16);
    if(count < 256){
    	i2c->CR2 |= (count << 16);
    	//nbytesCount = count;
    }
    else{
    	i2c->CR2 |= (0xFF << 16);
    	//nbytesCount = 255;
    }
    i2c->CR2 |= I2C_CR2_START;
    try = 0;
    while(i2c->CR2 & I2C_CR2_START){
    	try++;
		if(try == maxTry){
			if(timeout-- == 0){
				i2c->CR1 &= ~I2C_CR1_PE;
				locked = false;
				return false;
			}
			try = 0;
			delay_us(1000);
		}
    }
    for(current = 0, nbyteCurrent = 0; current < count; current++, nbyteCurrent++){
    	try = 0;
		while (!(i2c->ISR & I2C_ISR_RXNE)){
			try++;
			if(try == maxTry){
				if(timeout-- == 0){
					i2c->CR1 &= ~I2C_CR1_PE;
					locked = false;
					return false;
				}
				try = 0;
				delay_us(1000);
			}
		}
		buffer[current] = i2c->RXDR;
		if(nbyteCurrent == 255 && current < count){
			if(count - current < 256){
			    i2c->CR2 &= ~(0xFF << 16);
				i2c->CR2 |= ((count - current) << 16);
			}
			else{
				i2c->CR2 |= (0xFF << 16);
			}
			i2c->CR2 |= I2C_CR2_START;
			nbyteCurrent = 0;
		}
    }
    i2c->CR2 |= I2C_CR2_STOP;
    try = 0;
    while(i2c->CR2 & I2C_CR2_STOP){
    	try++;
		if(try == maxTry){
			if(timeout-- == 0){
				i2c->CR1 &= ~I2C_CR1_PE;
				locked = false;
				return false;
			}
			try = 0;
			delay_us(1000);
		}
    }
    i2c->CR1 &= ~I2C_CR1_PE;
    locked = false;
    return true;
}

bool I2C_isBusy(I2C_TypeDef *i2c){
	return (i2c->ISR & I2C_ISR_BUSY) || locked;
}
