#ifndef __MAIN_H
#define __MAIN_H

#include "boolean.h"

/* Type Definitions -----------------------------------------------------------------*/
typedef struct calibrationTable{
    uint16_t *tempMin;
    uint16_t *tempStep;
    uint16_t *tempMax;
    uint16_t *magnMin;
    uint16_t *magnStep;
    uint16_t *magnMax;
    uint16_t rows;
    uint16_t columns;
    uint16_t *table;
} CALIBRATION_TABLE;

typedef struct{uint16_t x1, y1, x2, y2;} TOUCH_AREA;
struct TouchScreenCalibration{uint16_t xDivider, yDivider, xOffset, yOffset;};
struct SystemCalibration{
							struct TouchScreenCalibration touchScreenCalibrationData;
							int16_t offsetADC;
                        };
typedef enum{EXTERNAL, FORCE, CONTINUOUS, SINGLE_SEQUENCE} MODE;
enum zoomButtonPressed{VZoomIn, VZoomOut, HZoomIn, HZoomOut, NONE};
enum saveFormat{UNUSED, TSV, CSV};
typedef struct{
	BOOL isMeasurementStarted; //TODO: Visur BOOL tipa reik keisti bool. Kazkaip maniau anksciau, kad cia nera "stdbool.h"
	BOOL isTouchLevelButtonPressed;
	bool isTouchScreenInitialized;
	bool isSensorConnected;
	bool isSensorConnectionStatusChanged;
	MODE mode;
	uint32_t forceMeasurementTimeLeft;
	//uint32_t hDiv;
	BOOL isMainWindow;
	//position
	int16_t temperature;
	int16_t offsetMiliTeslaTmp;
	int16_t graphOffsetX;
	bool isZoomButtonsEnabled;
	bool isBackToZeroButtonEnabled;
	uint16_t triggeredPosition;
	int32_t samplesToTakeForDrawing;
	bool startStopMeasurementButtonPressed;
	int16_t verticalTriggerLinePositionPx;
	bool isDataReceived;  
} STATUS;

typedef struct{
	BOOL isSingleSeqTouchModeSelected;
	uint32_t triggerLevelMiliTesla;
	uint32_t pulseLength_us, vDiv, hDiv;
	int16_t offsetMiliTesla; // user setpoint via menu
	enum saveFormat saveFormat;
} SETTINGS;

typedef struct AdcCAlibration_t{
    //--- adc calibration status
  bool           enable;
  int            sum;
  unsigned char  rej_cnt;
  unsigned short cnt;
  unsigned char  est_cnt;
  unsigned char  try_cnt;
  unsigned int   timer_ms;
  int            B_avg_mT;         
}AdcCAlibration_t;

const char *SubMenuItemNames[] = {
  "Set Date",
  "Calibration",
  "Reset Settings",
  "Format SD",
  "Information",
  "Save format"
};

//--- eeprom table definitions ---
#define EE_ID             0x99  // eeprom id number 

#define EE_ID_ADR         0     // id number location
#define EE_PULSE_LEN_us   2
#define EE_TRIG_LEV_mT    4
#define EE_VERTICAL_DIV   6
#define EE_HORIZONTAL_DIV 8
#define EE_SAVE_FORMAT    10

#define EE_ADC_ID_ADR     12
#define EE_ADC_OFFSET     14
  
//Goes from up to down, 40x40, monochrome
const uint8_t g_backToZeroIcon[40][5] = {
									{0x00, 0x00, 0x00, 0x00, 0x00},
									{0x00, 0x00, 0x00, 0x00, 0x00},
									{0x00, 0x00, 0x00, 0x00, 0x00},
									{0x00, 0x00, 0x00, 0x01, 0xF0},
									{0x04, 0x00, 0x00, 0x02, 0x08},
									{0x0C, 0x00, 0x00, 0x02, 0x08},
									{0x18, 0x00, 0x00, 0x01, 0xF0},
									{0x3F, 0xFF, 0xFF, 0xF8, 0x00},
									{0x7F, 0xFF, 0xFF, 0xF8, 0x00},
									{0x3F, 0xFF, 0xFF, 0xF8, 0x00},
									{0x18, 0x00, 0x00, 0x38, 0x00},
									{0x0C, 0x00, 0x00, 0x38, 0x00},
									{0x04, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x01, 0xBB, 0x00},
									{0x00, 0x00, 0x00, 0xFE, 0x00},
									{0x00, 0x00, 0x00, 0x7C, 0x00},
									{0x00, 0x00, 0x00, 0x38, 0x00},
									{0x00, 0x00, 0x00, 0x10, 0x00},
									{0x00, 0x00, 0x00, 0x00, 0x00},
									{0x00, 0x00, 0x00, 0x00, 0x00},
									{0x00, 0x00, 0x00, 0x00, 0x00}
								};

/* Macros ---------------------------------------------------------------------------*/

#ifdef USE_FULL_ASSERT
	#define assert(expr) ((expr) ? (void)0 : assert_failed((uint8_t *)__FILE__, __LINE__));
#else
	#define assert(expr) do{} while(0);
#endif

#define MAX(a,b) (((a)>(b))?(a):(b))
//#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

#define ABS(val) (((val) < 0) ? -(val) : (val))

// getValueFromCalibrationTable() is used to get specific value from cached calibration table which is in MCU flash memory
#define getValueFromCalibrationTable(row, column) (*(g_calibrationTable.table + ((row) * (g_calibrationTable.columns - 1) + column)))


#define RGB565(rgb) (((rgb&0xF8)>>3) | ((rgb&0xFC00)>>5)|((rgb&0xF80000)>>8)) // rgb888 to rgb565 conversion
#define RGB888(rgb) (((rgb&0x1F)<<3) | ((rgb&0x7E0)<<5) |  ((rgb&0xF800)<<8)) // rgb565 to rgb888

/* Private Function Prototypes---------------------------------------------------------------*/
/* Initialization */
void SystemClock_Config(void);
static void GPIO_Init(void);
static void InternalADC1_Init(void);
static void I2C_Init();

/* For Processing Data */
static uint16_t adc2mag(uint16_t adcValue, int16_t temperature);
static uint16_t mag2adc(uint32_t magneticField, int32_t temperature);
static FRESULT generatePreview(uint32_t srcSamplesToTake, uint16_t samplesToTakeTemperature, int32_t samplesOffset, enum saveFormat saveFormat, FIL *dstFile, bool m); //m - usually false. See drawGraph() detailed explanation

static void startMeasurement();
static void stopMeasurement();

static void updateSamplingFrequency();
static bool isTemperatureInvalid();

bool save(enum saveFormat format); //Save to SD card

/* For Presentation of Data */
static FRESULT drawGraph(enum saveFormat saveFormat, FIL *dstFile);
static void recalculateHorizontalDivAccordingPulseLengthAndMode();
static void changeStdVerticalDiv(bool toIncrease);
static void changeStdHorizontalDiv(BOOL toIncrease);
//static void adjustStdHorizontalDiv(uint32_t toThisValue);
//static void horizontalDivAppriximateToStandard(); //Not used. Highly possible that this function will be used in the future
static enum zoomButtonPressed zoomButtons();
void clearBuffers();

void refreshPositionArrows();

/* For Settings Saving & Restoring */
static void saveAdcOffset();
static void saveSettings();
static void restoreSettings();

/* Full-Screen Windows */
static void mainWindow();
static void mainMenu();
static void serviceMenu();

/* Dialogs */
void cbResetSettings(DIALOG *dlg, DLG_MESSAGE *msg); //Dialog callback function
void rmenu_setTriggerLevel();
void cbSetTriggerLevel(DIALOG *dlg, DLG_MESSAGE *msg);

void cbSetLength(DIALOG *dlg, DLG_MESSAGE *msg);
void cbSetLengthError(DIALOG *dlg, DLG_MESSAGE *msg); //Callback function which is called from other callback function cbSetLength()

void menu_setDate();
void cbSetDate(DIALOG *dlg, DLG_MESSAGE *msg);

void menu_setOffset();
void cbSetOffset(DIALOG *dlg, DLG_MESSAGE *msg);

void menu_resetSettings();
void cbResetSettings(DIALOG *dlg, DLG_MESSAGE *msg);

void menu_formatCard();
void cbFormatCard(DIALOG *dlg, DLG_MESSAGE *msg);

void menu_saveFormat();
void cbSaveFormat(DIALOG *dlg, DLG_MESSAGE *msg);

void menu_calibrateAdc();
void adcCal_init();

void cbSetLengthError(DIALOG *dlg, DLG_MESSAGE *msg);
void cbSetLength(DIALOG *dlg, DLG_MESSAGE *msg);
void rmenu_setLength();

/* Menu Items */
int8_t getRightMenuSelectedItemId();
void menu_information();

/* Service Menu Items */
static void smenu_setAdcOffset();
//static void smenu_touchScreenCalibration();
static void smenu_status();

/* For Status Information */
void refreshMainWindowStatus();
static void temperatureAsString(char *temperatureStr); //For refreshMainWindowStatus()
static uint8_t getBatteryStatus();
static void updateBatteryStatus();
void infoSensorConnected(enum cal_status calStatus);

/* Message Windows */
static void msg_sensorConnected(char *msg);
static void msg_sensorDisconnected(char *msg);
static void msg_collectingData();
static void msg_waiting();
static void msg_custom(char *message);

/* Interrupt Handlers */
void EXTI1_IRQHandler(void);
void EXTI9_5_IRQHandler(void);
void SysTick_Handler();

/* Power Management */
static void enterStandbyMode();

/* Other */
static char* uint64ToDecimal(uint64_t v); //sprintf() does not support uint64 but I need to represent it. This function converts uint64_t to string
void f(); //Dummy function used for testing
#ifdef USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line);
#endif

#endif