#include "ssd1963.h"

void SSD_Init(){
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_RESET);
	delay_ms(110);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_RD);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_WR);

	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_RESET);
	delay_ms(15);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_RESET);
	delay_ms(110);

	SSD_WriteCommand(SSD1963_SOFT_RESET);
	delay_ms(20);
	///SSD_WriteCommand(SSD1963_SOFT_RESET);
	///delay_ms(20);

	SSD_WriteCommand(SSD1963_SET_PLL); //Start PLL
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x01); //Enable PLL, but still use reference clock as system clock
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	delay_us(400); //Wait at least 100us to let the PLL stable delay_ms(200);
	SSD_WriteCommand(SSD1963_SET_PLL); //Lock PLL
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x03); //Now use PLL output as system clock
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	delay_us(400);

	SSD_WriteCommand(SSD1963_SET_LCD_MODE);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x20); //LCD 24 bit
	SSD_WriteData(0x00); //HSync + VSync + DE MODE
	SSD_WriteData((TFT_HORIZONTAL_RESOLUTION - 1) >> 8);
	SSD_WriteData((TFT_HORIZONTAL_RESOLUTION - 1) & 0x00FF);
	SSD_WriteData((TFT_VERTICAL_RESOLUTION - 1) >> 8);
	SSD_WriteData((TFT_VERTICAL_RESOLUTION - 1) & 0x00FF);
	SSD_WriteData(0x00); //RGB TFT
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
//delay_ms(100);

	SSD_WriteCommand(SSD1963_SET_PIXEL_DATA_INTERFACE);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x00); //8 bit MCU interface
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
//delay_ms(100);

	SSD_WriteCommand(SSD1963_SET_PIXEL_FORMAT);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x70); //24 bit per pixel
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
//delay_ms(100);

	SSD_WriteCommand(SSD1963_SET_LSHIFT_FREQ);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x01); //SET PCLK freq=4.94MHz  ; pixel clock frequency (0x02ffff works as well)
	SSD_WriteData(0x45);
	SSD_WriteData(0x47);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
//delay_ms(200);

	SSD_WriteCommand(SSD1963_SET_HORI_PERIOD);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x02);			//SET HSYNC Total=600 (display + non-display)
	SSD_WriteData(0x0d);
	SSD_WriteData(0x00);			//SET HPS = 68 (non-display period before the first display data)
	SSD_WriteData(0x2b);
	SSD_WriteData(0x28);			//SET HPW (HSYNC pulse width (LLINE) in pixel clock
	SSD_WriteData(0x00);			//SET LPS (HSYNC pulse start location in pixel clock)
	SSD_WriteData(0x00);
	SSD_WriteData(0x00);			//SET LPSPP (HSYNC pulse subpixel start position)
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
//delay_ms(200);

	SSD_WriteCommand(SSD1963_SET_VERT_PERIOD);
//	GPIO_SetBits(GPIOC, RS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x01);			//SET VSYNC Total=360 (display + non-display)
	SSD_WriteData(0x1d);
	SSD_WriteData(0x00);			//SET VPS (non-display period before the first display data)
	SSD_WriteData(0x0c);
	SSD_WriteData(0x09);			//SET VPW (VSYNC pulse width (LFRAME) in lines)
	SSD_WriteData(0x00);			//SET FPS (VSYNC pulse start location in lines)
	SSD_WriteData(0x00);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	SSD_WriteCommand(SSD1963_ENTER_NORMAL_MODE); //In this case the whole display area will be used
	SSD_WriteCommand(SSD1963_EXIT_IDLE_MODE); //Because in the idle mode only 16 basic colors are used
	SSD_WriteCommand(SSD1963_SET_DISPLAY_ON);

	SSD_AdjustBrightness(0); //Disable backlight not to show random picture

//delay_ms(20);
	SSD_WriteCommand(SSD1963_SET_TEAR_ON); //TEAR signal is on...
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x00); //... and consist only of V-blanking information
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	//16 bit (565 format)
	SSD_WriteCommand(SSD1963_SET_PIXEL_DATA_INTERFACE);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x03); //16 bit LCD interface
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	// Use whole display area
	SSD_WriteCommand(SSD1963_SET_COLUMN_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0); //First Col
	SSD_WriteData(0);
	SSD_WriteData((TFT_HORIZONTAL_RESOLUTION - 1) >> 8); //Last Col
	SSD_WriteData(TFT_HORIZONTAL_RESOLUTION - 1);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteCommand(SSD1963_SET_PAGE_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0); //First Page
	SSD_WriteData(0);
	SSD_WriteData((TFT_VERTICAL_RESOLUTION - 1) >> 8); //Last Page
	SSD_WriteData(TFT_VERTICAL_RESOLUTION - 1);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	SSD_fillScreen(0x000000); //Fill with black

	SSD_AdjustBrightness(255); //Enable backlight
}

void SSD_AdjustBrightness(uint8_t brightness){
	SSD_WriteCommand(SSD1963_SET_PWM_CONF);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x0E);			// PWMF[7:0] = 2, PWM base freq = PLL/(256*(1+5))/256 =
								    // 300Hz for a PLL freq = 120MHz
	SSD_WriteData(brightness);		// Set duty cycle, from 0x00 (total pull-down) to 0xFF
	SSD_WriteData(0x01);			// PWM enabled and controlled by host (MCU)
	SSD_WriteData(0x00);
	SSD_WriteData(0x00);
	SSD_WriteData(0x00);

	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
}

void SSD_PowerOn(){
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteCommand(SSD1963_SET_DISPLAY_ON);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
}

void SSD_SetPixel(uint16_t x, uint16_t y, uint32_t color){
	SSD_WriteCommand(SSD1963_SET_COLUMN_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(x >> 8); //First Col
	SSD_WriteData(x);
	SSD_WriteData((TFT_HORIZONTAL_RESOLUTION - 1) >> 8); //Last Col
	SSD_WriteData(TFT_HORIZONTAL_RESOLUTION - 1);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteCommand(SSD1963_SET_PAGE_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(y >> 8); //First Page
	SSD_WriteData(y);
	SSD_WriteData((TFT_VERTICAL_RESOLUTION - 1) >> 8); //Last Page
	SSD_WriteData(TFT_VERTICAL_RESOLUTION - 1);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	//	SSD_SetArea(0, TFT_HORIZONTAL_RESOLUTION - 1, 0, TFT_VERTICAL_RESOLUTION - 1);
	SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
//	SSD_WriteData(0);
//	SSD_WriteData(0);
//	SSD_WriteData(255);

	//for(x = 0; x < 48; x++){
	uint16_t data;
	data = ((color & 0xF80000UL) >> 8) | ((color & 0x00FC00UL) >> 5) | ((color >> 3) & 0x00001FUL);
	SSD_WriteData(data);

//		SSD_WriteData(color >> 8);
//		SSD_WriteData(color);

//		SSD_WriteData(255);
//		SSD_WriteData(0);
//		SSD_WriteData(0);
	//}
//	SSD_WriteData(color >> 16);
//	SSD_WriteData(color >> 8);
//	SSD_WriteData(color);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
}

void SSD_SetArea(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2){
	SSD_WriteCommand(SSD1963_SET_COLUMN_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(x1 >> 8); //First Col
	SSD_WriteData(x1);
	SSD_WriteData(x2 >> 8); //Last Col
	SSD_WriteData(x2);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteCommand(SSD1963_SET_PAGE_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(y1 >> 8); //First Page
	SSD_WriteData(y1);
	SSD_WriteData(y2 >> 8); //Last Page
	SSD_WriteData(y2);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
}

__attribute__( ( always_inline ) ) void SSD_WriteCommand(uint8_t command){
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_DC);
	GPIO_Write(SSD1963_DATA_BUS, command);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_WR);
	//delay_ms(1);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_WR);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	//GPIO_SetBits(SSD1963_DATA_BUS, SSD1963_SIGNAL_DC);
}

__attribute__( ( always_inline ) ) void SSD_WriteData(unsigned int data){
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_DC);
	GPIO_Write(SSD1963_DATA_BUS, data);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_WR);
	//delay_ms(1);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_WR);
	//GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_DC);
	///GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
}

void SSD_fillScreen(uint32_t color){
	SSD_drawSquare(0, 0, TFT_HORIZONTAL_RESOLUTION - 1, TFT_VERTICAL_RESOLUTION - 1, color);
}

void SSD_drawSquare(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint32_t color){
	uint16_t x, y;

	uint32_t pri_mask = __get_PRIMASK(); //=0 if interrupts are enabled
	__disable_irq();

	SSD_WriteCommand(SSD1963_SET_COLUMN_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(x1 >> 8); //First Col
	SSD_WriteData(x1);
	SSD_WriteData(x2 >> 8); //Last Col
	SSD_WriteData(x2);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteCommand(SSD1963_SET_PAGE_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(y1 >> 8); //First Page
	SSD_WriteData(y1);
	SSD_WriteData(y2 >> 8); //Last Page
	SSD_WriteData(y2);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	for(y = y2 - y1 + 1; y; y--){
		for(x = x2 - x1 + 1; x; x--){
			SSD1963_CONTROL_BUS->BSRRL = SSD1963_SIGNAL_DC;
			//SSD1963_DATA_BUS->ODR = color;
			SSD1963_DATA_BUS->ODR = ((color & 0xF80000UL) >> 8) | ((color & 0x00FC00UL) >> 5) | ((color >> 3) & 0x00001FUL);
			SSD1963_CONTROL_BUS->BSRRH = SSD1963_SIGNAL_WR;
			SSD1963_CONTROL_BUS->BSRRL = SSD1963_SIGNAL_WR;
		}
	}

	if(!pri_mask){
		__enable_irq();
	}

}
