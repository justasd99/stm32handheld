#include "ext_adc.h"
#include <stdbool.h>

uint16_t g_adcBuffer[ADC_MAX_SAMPLES]; //Use as "extern"
static uint16_t g_activeBufferSize;
void initExtADC(){

	/* Configure ADC control pins */

	CONTROL_PORT->MODER |= (1 << (POWER_DOWN_PIN * 2));
	CONTROL_PORT->PUPDR |= (1 << (POWER_DOWN_PIN * 2));
	CONTROL_PORT->MODER |= (1 << (NORMAL_PIN * 2));
	CONTROL_PORT->PUPDR |= (1 << (NORMAL_PIN * 2));
	CONTROL_PORT->MODER |= (1 << (WARP_PIN * 2));
	CONTROL_PORT->PUPDR |= (1 << (WARP_PIN * 2));
	EXTADC_WARP();

	/* Configure clocks */

	RCC->AHBENR |= RCC_AHBENR_GPIOAEN; //GPIO clock enable

	GPIOA->MODER |= 2 << (6 * 2); //Alternate function mode
	GPIOA->OTYPER |= 0 << 6; //GPIO pin as output (default)
	GPIOA->OSPEEDR |= 1 << (6 * 2); //10 MHz medium speed (NETIKRINAU!!!)
	GPIOA->AFR[0] |= 2 << (6 * 4); //Alternate Function 1 on GPIO PA5

	//Configure TIMER 3
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN; //Enable TIMER3
	TIM3->PSC = 2; //Prescaler
	TIM3->CCR1 = 0; //Clear/Compare Register (CLK)
	//TIM2->CCR2 =1; //Clear/Compare Register (DMA request)
	TIM3->ARR = 3; //Auto Reload Register

	//Enable CLK signal for External ADC
	TIM3->CCMR1 |= TIM_CCMR1_OC1M_0;
	TIM3->CCMR1 |= TIM_CCMR1_OC1M_1;
	TIM3->CCMR1 |= TIM_CCMR1_OC1CE;

	//Enable TIMER3 1-st channel
	TIM3->CCER |= TIM_CCER_CC1E;

	//Configure TIMER 4---------------
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN; //Enable TIMER4
	TIM4->PSC = 5; //Prescaler
	TIM4->CCR1 =1; //Clear/Compare Register (DMA request)
	TIM4->ARR = 3; //Auto Reload Register

	//Enable TIMER4 1-st ch
	TIM4->CCER |= TIM_CCER_CC1E;

	//Configure DMA----------
  RCC->AHBENR |= RCC_AHBENR_DMA1EN; //Enable DMA
	DMA1_Channel1->CPAR = (uint32_t)(&(GPIOE->IDR)); //Peripheral Address (from)
	DMA1_Channel1->CMAR = (uint32_t)g_adcBuffer; //Memory address (to)
	DMA1_Channel1->CNDTR = ADC_MAX_SAMPLES; //Buffer size in samples (not bytes!)
	DMA1_Channel1->CCR |= (DMA_CCR_PL_1 | DMA_CCR_PL_0); //Very high priority
	//Transfer from peripheral, circular mode, memory increment, peripheral not increment
	DMA1_Channel1->CCR |= (DMA_CCR_CIRC | DMA_CCR_MINC /* | DMA_CCR_PINC */);
	//Data size: 16 bit (for both memory and peripheral)
	DMA1_Channel1->CCR |= (DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE_0);
  DMA1_Channel1->CCR |= (DMA_CCR_TCIE); //Transfer Complete Interrupt Enable
	DMA1_Channel1->CCR |= (DMA_CCR_EN); //Enable

	TIM4->DIER |= TIM_DIER_CC1DE; //Connect timer to DMA (for DMA request)
	TIM3->CR1 |= TIM_CR1_CEN;
	TIM4->CR1 |= TIM_CR1_CEN;

	g_activeBufferSize = ADC_MAX_SAMPLES;
}
//BOOL ExtADC_setActiveBufferSize(uint16_t samples){
//	if(samples > ADC_MAX_SAMPLES)
//		return FALSE;
//	DMA1_Channel1->CCR &= ~(DMA_CCR_EN); //Disable
//	g_activeBufferSize = samples;
//	DMA1_Channel1->CNDTR = samples; //Buffer size in samples (not bytes!)
//	DMA1_Channel1->CCR |= (DMA_CCR_EN); //Enable
//	return TRUE;
//}

uint16_t getActiveBufferSize(){
	return g_activeBufferSize;
}

void ExtADC_changeClkPrescaler(uint32_t prescaler){
	TIM3->PSC = prescaler;
	TIM4->PSC = prescaler * 2 + 1;
}

void ExtADC_stop(){
	TIM4->CR1 &= ~TIM_CR1_CEN;
	DMA1_Channel1->CCR &= ~DMA_CCR_EN;
}

void ExtADC_clearBuffer(){
	int x;
	for(x = 0; x < ADC_MAX_SAMPLES; x++)
		g_adcBuffer[x] = 0;
}

void ExtADC_startSingleBlocking(uint16_t samplesCount){
	ExtADC_stop();
	ExtADC_clearBuffer();
	g_activeBufferSize = samplesCount;
	DMA1->IFCR |= (DMA_IFCR_CTEIF1 | DMA_IFCR_CHTIF1 | DMA_IFCR_CTCIF1 | DMA_IFCR_CGIF1);
	DMA1_Channel1->CNDTR = samplesCount;
	//Transfer from peripheral, circular mode, memory increment, peripheral not increment
	DMA1_Channel1->CCR &= ~DMA_CCR_CIRC;
	DMA1_Channel1->CCR |= DMA_CCR_TCIE;
	DMA1_Channel1->CCR |= DMA_CCR_EN; //Enable
	TIM4->CR1 |= TIM_CR1_CEN;
	while(DMA1_Channel1->CNDTR); //!(DMA1->ISR & (DMA_ISR_TCIF1 | DMA_ISR_TEIF1)));
	DMA1->IFCR |= (DMA_IFCR_CTEIF1 | DMA_IFCR_CHTIF1 | DMA_IFCR_CTCIF1 | DMA_IFCR_CGIF1);
}

void ExtADC_startSingle(uint16_t samplesCount){
	ExtADC_stop();
	ExtADC_clearBuffer();
	g_activeBufferSize = samplesCount;
	DMA1->IFCR |= (DMA_IFCR_CTEIF1 | DMA_IFCR_CHTIF1 | DMA_IFCR_CTCIF1 | DMA_IFCR_CGIF1);
	DMA1_Channel1->CNDTR = samplesCount;
	//Transfer from peripheral, non-circular mode, memory increment, peripheral not increment
	DMA1_Channel1->CCR &= ~DMA_CCR_CIRC;
	DMA1_Channel1->CCR |= DMA_CCR_TCIE;
	DMA1_Channel1->CCR |= DMA_CCR_EN; //Enable
	TIM4->CR1 |= TIM_CR1_CEN;
}

void ExtADC_startCircular(){
	ExtADC_clearBuffer();
	g_activeBufferSize = ADC_MAX_SAMPLES;
	DMA1_Channel1->CNDTR = g_activeBufferSize; //Buffer size in samples (not bytes!)
	//Transfer from peripheral, circular mode, memory increment, peripheral not increment
	DMA1_Channel1->CCR |= DMA_CCR_CIRC;
//DMA1_Channel1->CCR &= ~DMA_CCR_TCIE;
	DMA1_Channel1->CCR |= (DMA_CCR_EN); //Enable
	TIM4->CR1 |= TIM_CR1_CEN;
}

BOOL ExtADC_transfered(){
	if(DMA1->ISR & DMA_ISR_TCIF1){
		DMA1->IFCR |= DMA_IFCR_CTCIF1;
		return TRUE;
	}
	return FALSE;
}

__attribute__( ( always_inline ) )  uint16_t EXTADC_BUFFER_POSITION(){
	return g_activeBufferSize - DMA1_Channel1->CNDTR;
}

__attribute__( ( always_inline ) ) uint16_t EXTADC_BUFFER_POSITION2(){ //TODO: Kuris geras???
	return ((DMA1_Channel1->CNDTR == g_activeBufferSize) ? 0 : (g_activeBufferSize - DMA1_Channel1->CNDTR));
}
