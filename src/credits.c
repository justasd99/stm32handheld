#include "credits.h"

static void drawWindow(bool isSoftDev);

/* =================================================================================
	Function   : drawWindow()
	Description: Draws window with credits information.
	Arguments  : isSoftDev - draw window which contains information about software
	                         developers (if true) or hardware developers (if false).
	Return     : -
   --------------------------------------------------------------------------------- */

static void drawWindow(bool isSoftDev){
	UG_DrawLine(0, 20, 479, 20, C_YELLOW);
	UG_FillFrame(0, 21, 479, 271, C_BLACK);

	//Name
	UG_SetBackcolor(0x000000);
	UG_SetForecolor(C_YELLOW);
	UG_FontSelect(&FONT_12X20);
	UG_PutString(480 / 2 - 12 * 7 / 2 - 12 / 2, 30, "CREDITS"); //
	UG_DrawLine(480 / 2 - 12 * 7 / 2 - 12 / 2, 22 + 30, 480 / 2 + 12 * 6 / 2 + 12 / 2, 22 + 30, C_YELLOW);

	UG_ConsoleSetForecolor(C_WHITE);
	UG_ConsoleSetBackcolor(C_BLACK);

	if(isSoftDev){
		UG_FontSelect(&FONT_10X16);
		UG_PutString(5, 60, "Software Development:");

		UG_ConsoleSetArea(5, 80, 474, 271);
		UG_ConsoleClear();
		UG_FontSelect(&FONT_10X16);

		UG_ConsolePutString(" - Azuolas Faustas Bagdonas\n");
		UG_ConsolePutString("     email: afaustas@gmail.com\n");
		UG_ConsolePutString("     phone: +37065396714\n");
		UG_ConsolePutString(" - Dovydas Girdvainis\n");
		UG_ConsolePutString("     email: dovydas.girdvainis@gmail.com\n");
		UG_ConsoleSetForecolor(C_YELLOW);
		UG_FontSelect(&FONT_8X14);
		UG_ConsolePutString("\n \n \n \n \n SD support done according J. AXELSON code\n FATFS library has been used for FS support\n uGUI library has been used for graphics primitives");
	}
	else{
		UG_FontSelect(&FONT_10X16);
		UG_PutString(5, 60, "Hardware Development:");

		UG_ConsoleSetArea(5, 80, 474, 271);
		UG_ConsoleClear();
		UG_FontSelect(&FONT_10X16);

		UG_ConsolePutString(" - Vytautas Bleizgys\n");
		UG_ConsolePutString("     Schematics & PCB design\n");
		UG_ConsolePutString(" - Vitoldas Gobis\n");
		UG_ConsolePutString("     Research\n");
		UG_ConsolePutString(" - Audrius\n");
		UG_ConsolePutString("     Schematics & Case design\n");
	}
}

/* =================================================================================
	Function   : showCredits()
	Description: Fill screen with information about the developers of device
	Arguments  : -
	Return     : -
   --------------------------------------------------------------------------------- */

void showCredits(){
	drawWindow(false);
	while(!STMPE_isPressed());
	drawWindow(true);
	while(!STMPE_isPressed());
	while(STMPE_isPressed());
}
