#include "crc.h"

void CRC32_init(){
	RCC->AHBENR |= RCC_AHBENR_CRCEN;
}

uint32_t CRC32_calculate(uint8_t *string, uint16_t length){
	//We use hardware CRC32, Ethernet
	uint16_t position;
	CRC->CR |= CRC_CR_RESET;
	for(position = 0; position < length; position++){
		CRC->DR = string[position];
	}
	return CRC->DR;
}

void CRC32_reset(){
	CRC->CR |= CRC_CR_RESET;
}

void CRC32_add(uint32_t value){
	CRC->DR = value;
}

uint32_t CRC32_result(){
	return CRC->DR;
}
