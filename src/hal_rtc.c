#include "hal_rtc.h"

static uint8_t bcd2bin(uint8_t bcd);
static uint8_t bin2bcd(uint8_t bin);

void RTC_init(){
	RCC->APB1ENR |= RCC_APB1ENR_PWREN; //PWR CLK enable
	PWR->CR |= PWR_CR_DBP; //Enable access to BDCR (Backup Domain Control Register)
	RCC->BDCR |= RCC_BDCR_RTCSEL_0; //LSE as RTC CLK
	RCC->BDCR |= RCC_BDCR_LSEON; //LSE ON
	RCC->BDCR |= RCC_BDCR_RTCEN; //RTC enable
}

DATE RTC_getDate(){
	DATE date;
	uint32_t regValue = RTC->DR;
	date.year = bcd2bin((uint8_t)(regValue >> 16));
	date.month = bcd2bin((uint8_t)((0x00001F00 & regValue) >> 8));
	date.day = bcd2bin((uint8_t)(0x0000003F & regValue));
	return date;
}

TIME RTC_getTime(){
	TIME time;
	uint32_t regValue = RTC->TR;
	time.hours = bcd2bin((uint8_t)((0x007F0000 & regValue) >> 16));
	time.minutes = bcd2bin((uint8_t)((0x00007F00 & regValue) >> 8));
	time.seconds = bcd2bin((uint8_t)(0x0000007F & regValue));
	return time;
}

void RTC_setDate(const DATE *date){
	uint32_t regValue;
	uint32_t year_bcd, month_bcd, day_bcd;

	//Find out register value
	year_bcd = bin2bcd(date->year);
	month_bcd = bin2bcd(date->month);
	day_bcd = bin2bcd(date->day);
	regValue = (year_bcd << 16) | (month_bcd << 8) | day_bcd;
	regValue |= 1 << 13; //I must set week day even though I will not use it

	//Unlock RTC registers
	RTC->WPR = 0xCA;
	RTC->WPR = 0x53;

	//Write new register value
	RTC->ISR |= RTC_ISR_INIT;
	while((RTC->ISR & RTC_ISR_INITF) == 0);
	RTC->DR = regValue;
	RTC->ISR &= ~RTC_ISR_INIT;

	//Lock RTC registers
	RTC->WPR = 0xAF;
	RTC->WPR = 0xAB;

	regValue = RTC->DR; //I must do one dummy read with incorrect result. Next time I will get correct value
}

void RTC_setTime(const TIME *time){
	uint32_t regValue;
	uint32_t h_bcd, min_bcd, sec_bcd;

	//Find out register value
	h_bcd = bin2bcd(time->hours);
	min_bcd = bin2bcd(time->minutes);
	sec_bcd = bin2bcd(time->seconds);
	regValue = (h_bcd << 16) | (min_bcd << 8) | sec_bcd;

	//Unlock RTC registers
	RTC->WPR = 0xCA;
	RTC->WPR = 0x53;

	//Write new register value
	RTC->ISR |= RTC_ISR_INIT;
	while((RTC->ISR & RTC_ISR_INITF) == 0);
	//delay_ms(1);
	RTC->TR = regValue;
	RTC->ISR &= ~RTC_ISR_INIT;

	//Lock RTC registers
	RTC->WPR = 0xAF;
	RTC->WPR = 0xAB;
}

static uint8_t bcd2bin(uint8_t bcd){
	uint8_t bin;
	uint8_t tens, units;
	tens = bcd >> 4;
	units = bcd & 0x0F;
	bin = tens * 10 + units;
	return bin;
}

static uint8_t bin2bcd(uint8_t bin){
	uint8_t bcd;
	uint8_t tens, units;
	tens = bin / 10;
	units = bin % 10;
	bcd = (tens << 4) | units;
	return bcd;
}
