#include "menu.h"

static void refresh(MENU *pMenu);
static enum selection getItemBlocking(volatile BUTTON_STATUS *pButtonStatus, MENU *pMenu);
static void paintSelection(MENU_ITEM *pMenuItem, enum selection selection, BOOL isSelected);
static BOOL touchCoordsToItemId(TS_STATUS *p_tsStatus, enum selection *selection);

extern const char *SubMenuItemNames[];
void menu(MENU *pMenu, volatile BUTTON_STATUS *pButtonStatus /*, TS_STATUS *p_tsStatus*/){
	enum selection itemId;

	refresh(pMenu);
	while((itemId = getItemBlocking(pButtonStatus, pMenu)) != MENU_QUIT){
    
		//if(pMenu->items[itemId].exec != NULL)
		//	pMenu->items[itemId].exec();
    //refresh(pMenu);
		
    //--- adc calibration function to call
    if(pMenu->items[itemId].name == (char *)SubMenuItemNames[1]){
      if(pMenu->items[itemId].exec != NULL)
        pMenu->items[itemId].exec();
      break;
    }
    else{
      if(pMenu->items[itemId].exec != NULL)
        pMenu->items[itemId].exec();
      refresh(pMenu);
    }
	}
}

void refresh(MENU *pMenu){
	UG_FillFrame(0, 20, 479, 60, 0x004667);
	UG_FillFrame(0, 61, 479, 271, C_BLACK);
	UG_DrawLine(0, 60, 479, 60, C_DARK_GRAY);
	UG_DrawLine(0, 60 + 1 * 53, 479, 60 + 1 * 53, C_DARK_GRAY);
	UG_DrawLine(0, 60 + 2 * 53, 479, 60 + 2 * 53, C_DARK_GRAY);
	UG_DrawLine(0, 60 + 3 * 53, 479, 60 + 3 * 53, C_DARK_GRAY);
	UG_DrawLine(239, 61, 239, 271, C_DARK_GRAY);
	UG_DrawLine(440, 20, 440, 60, C_DARK_GRAY);

	//Isejimo ikona "X"
	UG_DrawLine(445, 25, 475, 55, C_YELLOW);
	UG_DrawLine(446, 25, 476, 55, C_YELLOW);
	UG_DrawLine(444, 25, 474, 55, C_YELLOW);
	UG_DrawLine(445, 55, 475, 25, C_YELLOW);
	UG_DrawLine(446, 55, 476, 25, C_YELLOW);
	UG_DrawLine(444, 55, 474, 25, C_YELLOW);

	//Meniu pavadinimas
	UG_SetBackcolor(0x004667);
	UG_SetForecolor(C_YELLOW);
	UG_FontSelect(&FONT_12X20);
	UG_PutString(40, 35, pMenu->title);

	UG_SetForecolor(C_WHITE);
	UG_SetBackcolor(C_BLACK);
	UG_FontSelect(&FONT_12X16);

	uint8_t itemId;
	for(itemId = 0; itemId < 8; itemId++){
		if(pMenu->items[itemId].name != NULL){
			UG_PutString(55 + ((itemId < 4) ? 0 : 240), 80 + 53 * ((itemId < 4) ? itemId : (itemId - 4)), pMenu->items[itemId].name);
		}
	}
}

static enum selection getItemBlocking(volatile BUTTON_STATUS *pButtonStatus, MENU *pMenu){
	enum selection selection = MENU_QUIT;
	BOOL selectionMade = FALSE;

	do{
		if(STMPE_isPressed()){
			TS_STATUS tsStatus;
			//refresh(pMenu);
			paintSelection(&pMenu->items[selection], selection, FALSE);
			tsStatus.x = STMPE_getX0();
			tsStatus.y = STMPE_getY0();
			tsStatus.isPressed = STMPE_isPressed();
			selectionMade = touchCoordsToItemId(&tsStatus, &selection);
			paintSelection(&pMenu->items[selection], selection, TRUE);
			while(STMPE_isPressed());
		}
		if(pButtonStatus->isPressed){
			while(pButtonStatus->isPressed);
			if(pButtonStatus->button == BUTTON_OK){

				while(!pButtonStatus->isPressed && !STMPE_isPressed());
				while(pButtonStatus->isPressed);
				if(pButtonStatus->button == BUTTON_REC){

					while(!pButtonStatus->isPressed && !STMPE_isPressed());
					while(pButtonStatus->isPressed);
					if(pButtonStatus->button == BUTTON_OK){
            while(!pButtonStatus->isPressed && !STMPE_isPressed());
            while(pButtonStatus->isPressed);
            if(pButtonStatus->button == BUTTON_OK){

              while(!pButtonStatus->isPressed && !STMPE_isPressed());
              while(pButtonStatus->isPressed);
              if(pButtonStatus->button == BUTTON_REC){

                while(!pButtonStatus->isPressed && !STMPE_isPressed());
                while(pButtonStatus->isPressed);
                if(pButtonStatus->button == BUTTON_REC){

                  showCredits();
                  refresh(pMenu);
                }
              }
            }
					}
				}
			}
		}

	}
	while(!selectionMade); // && (selection != MENU_QUIT));
	return selection;
}

static void paintSelection(MENU_ITEM *pMenuItem, enum selection selection, BOOL isSelected){
	if(selection != MENU_QUIT){
		uint8_t itemId = selection;
		UG_SetForecolor(isSelected ? C_RED : C_WHITE);
		UG_SetBackcolor(isSelected ? C_GREEN : C_BLACK);
		UG_FontSelect(&FONT_12X16);
		UG_FillFrame((itemId < 4) ? 0 : 240,
					 61 + 53 * ((itemId < 4) ? itemId : (itemId - 4)),
					 (itemId < 4) ? 238 : 479,
					 61 + 53 * ((itemId < 4) ? itemId : (itemId - 4)) + 51,
					 isSelected ? C_GREEN : C_BLACK);
		UG_PutString((itemId < 4) ? 55 : 295, 80 + 53 * ((itemId < 4) ? itemId : (itemId - 4)), pMenuItem->name);
	}
	else{
		//TODO: "X" mygtukui aprasyt ateityje
	}
}

static BOOL touchCoordsToItemId(TS_STATUS *p_tsStatus, enum selection *selection){
	int8_t selId;
	int16_t x = p_tsStatus->x;
	int16_t y = p_tsStatus->y;
	selId = (y - 61) / 53;
	if(y > 20 && y < 61 && x > 440){
		*selection = MENU_QUIT;
		return TRUE;
	}
	else if(y > 20 && y < 61)
		return FALSE;
	if(x > 241)
		selId += 4;
	*selection = selId;
	return TRUE;
}

int8_t drawPopupMenu(uint16_t x, uint16_t y, const char **itemList, uint8_t itemCount){
	const uint8_t arrowOffset = 16 + 10; //16 - font height
	const uint8_t arrowHeight = 20;
	const uint8_t arrowLength = 40;
	const uint8_t fontWidth = 10;
	const uint8_t fontHeight = 16;
	const uint8_t itemHeight = 35;
	uint16_t top, bottom;
	uint16_t width, height;
	uint16_t xStart, xEnd;
	//uint16_t ySelStart, ySelEnd;
	uint16_t xi, yi;
	uint8_t currentItem;
	int8_t selectedItem;
	//ITEM_COORDS itemCoords[7]; //Max 7 items supported in this menu
	TS_STATUS tsStatus;

	{
		uint8_t maxStrLen = 0;
		uint8_t currentStrLen;
		for(currentItem = 0; currentItem < itemCount; currentItem++){
			for(currentStrLen = 0; itemList[currentItem][currentStrLen]; currentStrLen++);
			if(currentStrLen > maxStrLen)
				maxStrLen = currentStrLen;
		}
		width = maxStrLen * fontWidth + 15;
	}
	height = itemCount * itemHeight;
	top = y - arrowOffset;
	bottom = top + height;
	if(bottom > 271){ //If bottom is out of bounds of the screen
		bottom = y + arrowHeight;
		top = bottom - height;
	}
	xStart = x - arrowLength - width;
	xEnd = x - arrowLength;
	UG_FillRoundFrame(xStart, top, xEnd, bottom, 5, C_BLUE); //0x004667);
	UG_DrawRoundFrame(xStart, top, xEnd, bottom, 5, C_YELLOW);
	UG_DrawLine(x - arrowLength, y - arrowHeight / 2, x, y, C_YELLOW);
	UG_DrawLine(x - arrowLength, y + arrowHeight / 2, x, y, C_YELLOW);
	fillTriangle(x - arrowLength, y - arrowHeight / 2 + 1, x - 1, y, x - arrowLength, y + arrowHeight / 2 - 1, C_BLUE); //0x004667);

	UG_SetBackcolor(C_BLUE); //0x004667
	UG_SetForecolor(C_YELLOW);
	UG_FontSelect(&FONT_10X16);
	xi = xStart + 5;
	yi = top;
	for(currentItem = 0; currentItem < itemCount; currentItem++){
		UG_PutString(xi, yi + itemHeight / 2 - fontHeight / 2, (char*)itemList[currentItem]);
		if(currentItem < itemCount - 1)
			UG_DrawLine(xStart, yi + itemHeight, xStart + width, yi + itemHeight, C_YELLOW);
		yi += itemHeight;
	}

	while(STMPE_isPressed());

	while(!STMPE_isPressed()); //Waiting for choice
	tsStatus.x = STMPE_getX0();
	tsStatus.y = STMPE_getY0();
	//Choice made. Determine which item selected.
	if((tsStatus.x > xStart && tsStatus.x < xEnd) && (tsStatus.y > top && tsStatus.y < bottom)){
		//One item is selected. Determine witch:
		selectedItem = (tsStatus.y - top) / itemHeight;
		//Select
		if(selectedItem == 0)
			UG_FillFrame(xStart, top + selectedItem * itemHeight + 5, xStart + width, top + selectedItem * itemHeight + itemHeight, C_GREEN);
		else if(selectedItem == itemCount - 1)
			UG_FillFrame(xStart, top + selectedItem * itemHeight, xStart + width, top + selectedItem * itemHeight + itemHeight - 5, C_GREEN);
		else
			UG_FillFrame(xStart, top + selectedItem * itemHeight, xStart + width, top + selectedItem * itemHeight + itemHeight, C_GREEN);
		UG_SetBackcolor(C_GREEN);
		UG_SetForecolor(C_RED);
		UG_PutString(xi, top + selectedItem * itemHeight + itemHeight / 2 - fontHeight / 2, (char*)itemList[selectedItem]);
	}
	else{
		//Selected item is out of the bounds of menu
		selectedItem = -1;
	}

	return selectedItem;
}
