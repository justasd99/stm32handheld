#include "calibration.h"

static bool checkValidityEEPROM();
static bool checkValidityFlash();
static bool recalculateCRC();
//static uint32_t getMaxValueFrom_EEPROM_Table();

enum cal_status calibration_retrieveSensorCalibrationData(){
	uint8_t *lastUsedSensorSerialNumber = (uint8_t *)(CALIBRATION_DATA_START_ADDRESS + 43);
	uint8_t currentSensorSerialNumber[16];
	enum cal_status status;
	if(!I2C_EEPROM_readSerial(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, currentSensorSerialNumber)){
		//EEPROM does not have a serial number
		I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 0x002B, currentSensorSerialNumber, 16);
	}
	if(checkValidityFlash() && memcmp(lastUsedSensorSerialNumber, currentSensorSerialNumber, 16) == 0){
		//Sensor has NOT changed
		status = SAME_SENSOR;
	}
	else if(checkValidityEEPROM()){
		//There is a connection with EEPROM and it looks like that this EEPROM contains valid data.
		//Let's copy to the internal flash memory
		uint32_t flashAddress, eepromAddress, eepromLastAddress;
		uint32_t crcCalculated /*, crcRead*/;
		uint16_t data;
		uint8_t tryCopy = 0;
		//uint8_t tryErase;
		const uint8_t tryMax = 20;
		FLASH_unlock();
		do{
			CRC32_reset();
			//We must erase old information before writing new
			for(flashAddress = CALIBRATION_DATA_START_ADDRESS; flashAddress <= CALIBRATION_DATA_END_ADDRESS; flashAddress += PAGE_SIZE){
			//flashAddress = CALIBRATION_DATA_START_ADDRESS;
				if(FLASH_erasePage(flashAddress) == FLASH_ERROR)
					return ERASE_ERROR;
			}

			//Now we can copy
			for(eepromAddress = 0, flashAddress = CALIBRATION_DATA_START_ADDRESS; eepromAddress < 128; eepromAddress += 2, flashAddress += 2){
				//We will not copy CRC from EEPROM because it will not be the same because table will be recalculated
				if(6 <= eepromAddress && eepromAddress <= 9){
					continue;
				}
				//Copy Serial Number
				else if(eepromAddress == 42){
					data = 0;
					I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, eepromAddress, (uint8_t*)(&data), 1);
					data |= currentSensorSerialNumber[0] << 8;
				}
				else if(44 <= eepromAddress && eepromAddress <= 56){
					data = currentSensorSerialNumber[eepromAddress - 43]; //Remember that usually we use EEPROM ID instead our generated and written SN
					data |= currentSensorSerialNumber[eepromAddress - 43 + 1] << 8;
				}
				else if(eepromAddress == 58){
					data = 0;
					I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, eepromAddress + 1, (uint8_t*)(&data) + 1, 1);
					data |= currentSensorSerialNumber[15];
				}
				//Copy other header data
				else{
					I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, eepromAddress, (uint8_t*)&data, 2);
				}
				FLASH_programHalfWord(flashAddress, data); //Let's write just generated word to flash
				if(eepromAddress >= 10){
					CRC32_add((uint32_t)data);
				}
			}
			uint16_t adcValue;
			uint16_t maxValue;
			I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 128, (uint8_t*)&maxValue, 2);

			I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 2, (uint8_t*)&eepromLastAddress, 4);
			for(; eepromAddress < eepromLastAddress && flashAddress <= CALIBRATION_DATA_END_ADDRESS ; eepromAddress += 2, flashAddress += 2){
				I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, eepromAddress, (uint8_t*)&data, 2);

				//TODO: CIA DAR GAL BUTU VERTA IR GALIMA ITERPTI PAPILDOMA TIKRINIMA ATEITYJE, NORS VARGU AR JO REIK?!

				///adcValue = (uint16_t)((uint32_t)data * 0xFFFFUL / maxValue); //There we are already sure that maxValue > 0
				adcValue = (uint16_t)((uint32_t)data * 0x10000UL * 4 / 10240);
				FLASH_programHalfWord(flashAddress, adcValue);
				CRC32_add((uint32_t)(adcValue));
			}
			crcCalculated = CRC32_result();
			FLASH_programHalfWord(flashAddress, (uint16_t)crcCalculated);
			FLASH_programHalfWord(flashAddress + 2, (uint16_t)(crcCalculated >> 16));
			tryCopy++; //We are going to try copying operation for $tryMax times
		}
		while(checkValidityFlash() == false && tryCopy < tryMax);
		if(tryCopy == tryMax){
			status = COPY_ERROR;
			FLASH_erasePage(flashAddress);
		}
		else{
			status = SUCCESSFUL;
		}
		FLASH_lock();
	}
	else{
		status = INVALID_DATA;
	}
	return status;
}

static bool checkValidityEEPROM(){
	bool isValid = false;
	uint8_t magic[2] = {0x00, 0x00};
	I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 0, magic, 2);
	if(magic[0] == 0xAA && magic[1] == 0xBB){
		uint32_t currentAddress, lastAddress;
		I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 2, (uint8_t*)&lastAddress, 4);
		if(lastAddress > 128 && lastAddress <= CALIBRATION_DATA_END_ADDRESS - CALIBRATION_DATA_START_ADDRESS){
			uint8_t version;
			uint8_t data;
			I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 10, &version, 1);
			if(version == 1){ //Only 1-st version is supported
				uint16_t magMin, magStep, magMax;
				int16_t tempMin, tempStep, tempMax;
				I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 15, (uint8_t*)&magMin, 2);
				I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 17, (uint8_t*)&magMax, 2);
				I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 19, (uint8_t*)&magStep, 2);
				I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 21, (uint8_t*)&tempMin, 2);
				I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 23, (uint8_t*)&tempMax, 2);
				I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 25, (uint8_t*)&tempStep, 2);
				if((10 <= magStep && magStep <= 10000) &&
				   (0 <= magMin && magMin <= 65535) &&
				   (0 <= magMax && magMax <= 65535) &&
				   (10 <= tempStep && tempStep <= 100) &&
				   (-400 <= tempMin && tempMin <= 1000) &&
				   (-400 <= tempMax && tempMax <= 1000)){

					uint32_t crcCalculated, crcRead;
					I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 6, (uint8_t*)&crcRead, 4);
					CRC32_reset();
					CRC32_add(version);
					for(currentAddress = 11; currentAddress <= lastAddress; currentAddress++){
						I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, currentAddress, &data, 1);
						CRC32_add(data);
					}
					crcCalculated = CRC32_result();
					if(crcCalculated == crcRead){
						uint16_t maxValue = 0;
						I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 128, (uint8_t*)&maxValue, 2);
						if(maxValue > MIN_MAXVALUE){
							isValid = true;
						}
					}
				}
			}
		}
	}
	return isValid;
}

static bool checkValidityFlash(){
	bool isValid = false;
	if(*(__IO uint16_t*)CALIBRATION_DATA_START_ADDRESS == 0xBBAA){
		uint32_t currentRelativeAddress, lastRelativeAddress;
		lastRelativeAddress = *(__IO uint32_t*)(CALIBRATION_DATA_START_ADDRESS + 2);
		if(lastRelativeAddress <= CALIBRATION_DATA_END_ADDRESS - CALIBRATION_DATA_START_ADDRESS){
			uint8_t version;
			uint16_t data;
			version = *(__IO uint8_t*)(CALIBRATION_DATA_START_ADDRESS + 10);
			if(version == 1){ //Only 1-st version is supported
				uint16_t magMin, magStep, magMax;
				int16_t tempMin, tempStep, tempMax;
				magMin = *(__IO uint16_t*)(CALIBRATION_DATA_START_ADDRESS + 15);
				magMax = *(__IO uint16_t*)(CALIBRATION_DATA_START_ADDRESS + 17);
				magStep = *(__IO uint16_t*)(CALIBRATION_DATA_START_ADDRESS + 19);
				tempMin = *(__IO int16_t*)(CALIBRATION_DATA_START_ADDRESS + 21);
				tempMax = *(__IO int16_t*)(CALIBRATION_DATA_START_ADDRESS + 23);
				tempStep = *(__IO int16_t*)(CALIBRATION_DATA_START_ADDRESS + 25);
				if((10 <= magStep && magStep <= 10000) &&
				   (0 <= magMin && magMin <= 65535) &&
				   (0 <= magMax && magMax <= 65535) &&
				   (10 <= tempStep && tempStep <= 100) &&
				   (-400 <= tempMin && tempMin <= 1000) &&
				   (-400 <= tempMax && tempMax <= 1000)){

					uint32_t crcCalculated, crcRead;
					CRC32_reset();
					for(currentRelativeAddress = 10; currentRelativeAddress <= lastRelativeAddress; currentRelativeAddress+=2){
						data = *(__IO uint16_t*)(CALIBRATION_DATA_START_ADDRESS + currentRelativeAddress);
						CRC32_add((uint32_t)data);
					}
					crcRead = *(__IO uint32_t*)(CALIBRATION_DATA_START_ADDRESS + currentRelativeAddress);
					crcCalculated = CRC32_result();
					if(crcCalculated == crcRead){
						isValid = true;
					}
				}
			}
		}
	}
	return isValid;
}

static bool recalculateCRC(){
	uint32_t crc;
	uint16_t address, lastAddress;
	uint8_t byte;
	bool success = true;
	success &= I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 2, (uint8_t*)&lastAddress, 4);
	CRC32_reset();
	for(address = 10; address <= lastAddress; address ++){
		success &= I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, address, &byte, 1);
		CRC32_add(byte);
	}
	crc = CRC32_result();
	success &= I2C_EEPROM_writeByte(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 6, (uint8_t)crc);
	success &= I2C_EEPROM_writeByte(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 7, (uint8_t)(crc >> 8));
	success &= I2C_EEPROM_writeByte(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 8, (uint8_t)(crc >> 16));
	success &= I2C_EEPROM_writeByte(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 9, (uint8_t)(crc >> 24));
	return success;
}

bool calibration_sensorExists(){
	uint8_t magic[2] = {0x00, 0x00};
	I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 0, magic, 2);
	if(magic[0] == 0xAA && magic[1] == 0xBB)
		return true;
	return false;
}

bool calibration_sensorValid(){
	return checkValidityEEPROM();
}

bool calibration_dataInFlashIsValid(){
	return checkValidityFlash();
}

void calibration_getSerialNumber(uint8_t *serialNumber){
	I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 43, serialNumber, 16);
}

int16_t calibration_getOffset(){
	int16_t offset = 0;
	uint8_t testIfUsed;
	I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 125, &testIfUsed, 1);
	if(testIfUsed != 1)
		offset = 0;
	else
		I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 126, (uint8_t*)&offset, 2);
	return offset;
}
bool calibration_setOffset(int16_t offset){
	bool success = false;
	if(I2C_EEPROM_writeByte(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 125, 0x01)){
		if(I2C_EEPROM_writeByte(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 126, (uint8_t)offset)){
			if(I2C_EEPROM_writeByte(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 127, (uint8_t)(offset >> 8))){
				success = true;
			}
		}
		recalculateCRC();
	}
	return success;
}

bool calibration_isSensorConnected(){
	uint8_t magic[2] = {0x00, 0x00};
	I2C_EEPROM_read(I2C_INTERFACE_ADDRESS, EEPROM_ADDRESS, 0, magic, 2);
	return (magic[0] == 0xAA && magic[1] == 0xBB);
}
