//Remiantis Justo Dilio projektu (ankstesnis matuoklio variantas)
//Sis kodas buvo paimtas is knygos JAN AXELSON "USB MASS STORAGE. Designing and programming devices and embedded hosts" ir pritaikytas musu projektui.
//Modifikavau ir pridejau draft'ini SDHC palaikyma

//TODO: SIS KODAS PO PAKEITIMU DABAR YRA SUPER NETVARKINGAS. REIKETU SUTVARKYTI!

#include "sd_card.h"

static bool g_isSDHC = false;
/*static*/volatile uint64_t g_size;

//extern SPI_HandleTypeDef hspi2;

volatile byte msd_buffer[512];
CSD gblCSDReg;
CID gblCIDReg;

 word		gblAcessTime_ms;
 dword	gblSpeed_kHz;
 word		gblIndex;

 const			typSDC_CMD sdmmc_cmdtable[] =
 {
	 // command name CRC response type more data?
	 {cmdGO_IDLE_STATE, 0x95, R1, NODATA},
	 {cmdSEND_OP_COND, 0xF9, R1, NODATA},
	 {cmdSEND_CSD, 0xAF, R1, MOREDATA},
	 {cmdSEND_CID, 0x1B, R1, MOREDATA},
	 {cmdSTOP_TRANSMISSION, 0xC3, R1, NODATA},
	 {cmdSEND_STATUS, 0xAF, R2, NODATA},
	 {cmdSET_BLOCKLEN, 0xFF, R1, NODATA},
	 {cmdREAD_SINGLE_BLOCK, 0xFF, R1, MOREDATA},
	 {cmdREAD_MULTI_BLOCK, 0xFF, R1, MOREDATA},
	 {cmdWRITE_SINGLE_BLOCK, 0xFF, R1, MOREDATA},
	 {cmdWRITE_MULTI_BLOCK, 0xFF, R1, MOREDATA},
	 {cmdTAG_SECTOR_START, 0xFF, R1, NODATA},
	 {cmdTAG_SECTOR_END, 0xFF, R1, NODATA},
	 {cmdUNTAG_SECTOR, 0xFF, R1, NODATA},
	 {cmdTAG_ERASE_GRP_START, 0xFF, R1, NODATA},
	 {cmdTAG_ERASE_GRP_END, 0xFF, R1, NODATA},
	 {cmdUNTAG_ERASE_GRP, 0xFF, R1, NODATA},
	 {cmdERASE, 0xDF, R1b, NODATA},
	 {cmdLOCK_UNLOCK, 0x89, R1b, NODATA},
	 {cmdSD_APP_OP_COND, 0xE5, R1, NODATA},
	 {cmdAPP_CMD, 0x73, R1, NODATA},
	 {cmdREAD_OCR, 0x25, R3, NODATA},
	 {cmdCRC_ON_OFF, 0x25, R1, NODATA},
	 {cmdCMD8, 0x87, R1, NODATA}
 };

 SDC_RESPONSE SendSDCCmd(byte cmd, dword address){
	 CMD_PACKET CmdPacket;
	byte index;
	SDC_RESPONSE response;
	word timeout = 9;

	//--- Bring the cardÂ’s chip-select line low.
	SDC_CS(0);
	//--- Store a command byte, address, and CRC value in the CMD_PACKET structure.
	CmdPacket.cmd = sdmmc_cmdtable[cmd].CmdCode;
	//CmdPacket.address = address;
	CmdPacket.addr0 = address;
	CmdPacket.addr1 = (address >> 8);
	CmdPacket.addr2 = (address >> 16);
	CmdPacket.addr3 = (address >> 24);
	CmdPacket.crc = sdmmc_cmdtable[cmd].crc;

	//--- Send the command byte, address bytes, and CRC byte.
	//--- The WriteSPI library function writes a byte on the SPI bus.
	WriteSPI(CmdPacket.cmd);
	WriteSPI(CmdPacket.addr3);
	WriteSPI(CmdPacket.addr2);
	WriteSPI(CmdPacket.addr1);
	WriteSPI(CmdPacket.addr0);
//		WriteSPI(0x00);
//		WriteSPI(0x00);
//		WriteSPI(0x00);
//		WriteSPI(0x00);
	WriteSPI(CmdPacket.crc);


	//--- Is the command's response type R1 or R1b?
	if (sdmmc_cmdtable[cmd].responsetype == R1 || sdmmc_cmdtable[cmd].responsetype == R1b)
	{
		do
		{
			//--- Read a byte from the card until the byte doesn't equal FFh or a timeout occurs.
			response.r1._byte = ReadMedia();
			timeout--;
		} while ((response.r1._byte == 0xFF) && (timeout != 0));
	}
	//--- Is the commandÂ’s response type R2?
	else if (sdmmc_cmdtable[cmd].responsetype == R2)
	{
		do
		{
			//--- read the first bye of the response.
			//--- _byte0 transmits first.
			response.r2._byte0 = ReadMedia();
			timeout--;
		} while ((response.r2._byte0 == 0xFF) && (timeout != 0));
		//--- If the first byte was read, read the second byte.
		if (response.r2._byte0 != 0xFF)
		response.r2._byte1 = ReadMedia();
	}
	//--- Is the response type R1b?
	if (sdmmc_cmdtable[cmd].responsetype == R1b)
	{
		//--- The R1b response byte has been read.
		//--- Wait for not busy status by reading from the card until a byte doesn't equal 00h
		//--- or a timeout occurs..
		response.r1._byte = 0x00;
		for (index = 0; index < 0xFF && response.r1._byte == 0x00; index++)
		{
			timeout = 0xFFFF;
			do
			{
				response.r1._byte = ReadMedia();
				timeout--;
			} while ((response.r1._byte == 0x00) && (timeout != 0));
		}
	}
	//--- Generate 8 clock cycles.
	mSend8ClkCycles();
	//--- If no more data is expected for this command, deselect the card.
	if (!(sdmmc_cmdtable[cmd].moredataexpected))
	SDC_CS(1);
	return(response);

 }

SDC_Error CSDRead(){
	 dword address = 0x00;
	 byte cmd = SEND_CSD;
 CMD_PACKET CmdPacket;
	 byte data_token;
	 word index;
	 SDC_RESPONSE response;
	 SDC_Error status = sdcValid;
	 word timeout = 0x1fff;
	 // Select the card.
	 SDC_CS(0);
	 // Store a command byte, address, and CRC value in the CMD_PACKET structure.
	 CmdPacket.cmd = sdmmc_cmdtable[cmd].CmdCode;
	 //CmdPacket.address = address;
	CmdPacket.addr3 = address;
	CmdPacket.addr2 = (address >> 8);
	CmdPacket.addr1 = (address >> 16);
	CmdPacket.addr0 = (address >> 24);
	 CmdPacket.crc = sdmmc_cmdtable[cmd].crc;
	 // Send the command byte, address bytes, and CRC byte.
	 // The WriteSPI library function writes a byte on the SPI bus.
	 WriteSPI(CmdPacket.cmd);
	 WriteSPI(CmdPacket.addr3);
	 WriteSPI(CmdPacket.addr2);
	 WriteSPI(CmdPacket.addr1);
	 WriteSPI(CmdPacket.addr0);
	 WriteSPI(CmdPacket.crc);
	 // Read a byte from the card until the byte doesn't equal FFh or a timeout occurs.
	 do
	 {
		 response.r1._byte = ReadMedia();
		 timeout--;
	 } while ((response.r1._byte == 0xFF) && (timeout != 0));
	 // A response of 00h means the command was accepted.
	 if (response.r1._byte != 0x00)
	 {
		 status = sdcCardBadCmd;
	 }
	 else
	 {
		 index = 0x1FFF;	//2ff
		 //Wait for the data_start token or a timeout.
		 do
		 {
			 data_token = ReadMedia();
			 index--;
		 } while ((data_token == SDC_FLOATING_BUS) && (index != 0));
		 if ((index == 0) || (data_token != DATA_START_TOKEN))
		 status = sdcCardTimeout;
		 else
		 {
			 // A data start token was received.
			 // Read the CSD register's 16 bytes.
			 for (index = 0; index < CSD_SIZE; index++)
			 {
				 gblCSDReg._byte[index] = ReadMedia();
			 }
		 }
		 // Generate 8 clock cycles to complete the command.
		 mSend8ClkCycles();
	 }
	 // De select the card.
	 SDC_CS(1) ;
	 return(status);
}

SDC_Error CIDRead(){
	dword address = 0x00;
	byte cmd = SEND_CID;
	CMD_PACKET CmdPacket;
	byte data_token;
	word index;
	SDC_RESPONSE response;
	SDC_Error status = sdcValid;
	word timeout = 0x1fff;
	// Select the card.
	SDC_CS(0);
	// Store a command byte, address, and CRC value in the CMD_PACKET structure.
	CmdPacket.cmd = sdmmc_cmdtable[cmd].CmdCode;
	//CmdPacket.address = address;
	CmdPacket.addr3 = address;
	CmdPacket.addr2 = (address >> 8);
	CmdPacket.addr1 = (address >> 16);
	CmdPacket.addr0 = (address >> 24);
	CmdPacket.crc = sdmmc_cmdtable[cmd].crc;
	// Send the command byte, address bytes, and CRC byte.
	// The WriteSPI library function writes a byte on the SPI bus.
	WriteSPI(CmdPacket.cmd);
	WriteSPI(CmdPacket.addr3);
	WriteSPI(CmdPacket.addr2);
	WriteSPI(CmdPacket.addr1);
	WriteSPI(CmdPacket.addr0);
	WriteSPI(CmdPacket.crc);
	// Read a byte from the card until the byte doesn't equal FFh or a timeout occurs.
	do
	{
		response.r1._byte = ReadMedia();
		timeout--;
	} while ((response.r1._byte == 0xFF) && (timeout != 0));
	// A response of 00h means the command was accepted.
	if (response.r1._byte != 0x00)
	{
		status = sdcCardBadCmd;
	}
	else
	{
		index = 0x1FFF;	//2ff
		//Wait for the data_start token or a timeout.
		do
		{
			data_token = ReadMedia();
			index--;
		} while ((data_token == SDC_FLOATING_BUS) && (index != 0));
		if ((index == 0) || (data_token != DATA_START_TOKEN))
		status = sdcCardTimeout;
		else
		{
			// A data start token was received.
			// Read the CSD register's 16 bytes.
			for (index = 0; index < 16; index++)
			{
				gblCIDReg._byte[index] = ReadMedia();
			}
		}
		// Generate 8 clock cycles to complete the command.
		mSend8ClkCycles();
	}
	// De select the card.
	SDC_CS(1) ;
	return(status);
}

SDC_Error SectorRead(dword sector_addr, byte* buffer){
	byte data_token;
	word index;
	SDC_RESPONSE response;
	SDC_Error status = sdcValid;
	// Issue a READ_SINGLE_BLOCK command.
	if(g_isSDHC){
		response = SendSDCCmd(READ_SINGLE_BLOCK, sector_addr);
	}
	else{
		//Seniau korteles buvo adresuojamos baitais, tad *512 norint gaut sektorius, o SDHC jau ir taip sektoriais adresuojamos

		// Specify the address of the first byte to read in the media.
		// To obtain the address of a sectorÂ’s first byte,
		// shift the sector address left 9 times to multiply by 512 (sector size).
		response = SendSDCCmd(READ_SINGLE_BLOCK, (sector_addr << 9));
	}
	// A response of 00h indicates success.
	if (response.r1._byte != 0x00)
	{
		status = sdcCardBadCmd;
	}
	else
	{
		// The command was accepted.

		index = 0x1FFF;	//2ff
		do
		{
			// Read from the card until receiving a response or a timeout.
			data_token = ReadMedia();
			index--;
		} while ((data_token == SDC_FLOATING_BUS) && (index != 0));
		if ((index == 0) || (data_token != DATA_START_TOKEN))
		// The card didn't send a data start token.
		status = sdcCardTimeout;
		else
		{
			// The card sent a data start token.
			// Read a sectorÂ’s worth of data from the card.
			for (index = 0; index < SDC_SECTOR_SIZE; index++)
			{
				buffer[index] = ReadMedia();
			}
			// Read the CRC bytes.
			mReadCRC();
		}
		// Generate 8 clock cycles to complete the command.
		mSend8ClkCycles();
	}
	// Des elect the card.
	SDC_CS(1);
	return(status);
}
 SDC_Error SectorWrite(dword sector_addr,const byte* buffer){
	byte data_response;
	word index;
	SDC_RESPONSE response;
	SDC_Error status = sdcValid;

	if(g_isSDHC){
		response = SendSDCCmd(WRITE_SINGLE_BLOCK, sector_addr);
	}
	else{
		//Seniau korteles buvo adresuojamos baitais, tad *512 norint gaut sektorius, o SDHC jau ir taip sektoriais adresuojamos

		// Issue a WRITE_SINGLE_BLOCK command.
		// Pass the address of the first byte to write in the media.
		// To obtain the address of a sectorÂ’s first byte,
		// shift the sector address left 9 times to multiply by 512 (sector size).
		response = SendSDCCmd(WRITE_SINGLE_BLOCK, (sector_addr << 9));
	}

	// A response of 00h indicates success.
	if (response.r1._byte != 0x00)
	status = sdcCardBadCmd;
	else
	{
		// The command was accepted.
		// Send a data start token.
		WriteSPI(DATA_START_TOKEN);
		// Send a sectorÂ’s worth of data.
		for(index = 0; index < 512; index++)
		WriteSPI(buffer[index]);
		// Send the CRC bytes.
		mSendCRC();
		// Read the cardÂ’s response.
		data_response = ReadMedia();
		if ((data_response & 0x0F) != DATA_ACCEPTED)
		{
			status = sdcCardDataRejected;
		}
		else
		{
			// The card is writing the data into the storage media.
			// Wait for the card to return non-zero (not busy) or a timeout.
			index = 0;
			do
			{
				data_response = ReadMedia();
				index++;
			} while ((data_response == 0x00) && (index != 0));
			  if (index == 0)
			  // The write timed out.
			  status = sdcCardTimeout;
		  }
		  // The write was successful.
		  // Generate 8 clock cycles to complete the command.
		  mSend8ClkCycles();
	  }
	  // De select the card.
	  SDC_CS(1);
	  return(status);
  }
 byte IsWriteProtected(void){
	 // if (MEDIA_WD) return TRUE;
	 // else return FALSE;
	 //return TRUE;

	 return FALSE;
 }

 void Delayms(byte milliseconds){
	 delay_us(milliseconds * 1000);
}

SDC_Error MediaInitialize(SDCSTATE *Flag){
	SDC_Error CSDstatus = sdcValid;
	volatile SDC_RESPONSE response;
	SDC_Error status = sdcValid;
	word timeout;
	Flag -> _byte = 0x0;

	//--- De select the card.
	SDC_CS(1);
	Delayms(10);
	//--- Open the SPI port.
	//--- Clock speed must be <= 400 kHz until the card is initialized
	//--- and the CSD register has been read.
	//--- MultiMediaCards require CKE = 0, CKP = 1,
	//--- and sampling DataOut in the middle of a clock cycle.
	//OpenSPI(SPI_FOSC_64, MODE_11, SMPMID);

	//--- 24M/128=188kHz
//	OpenSPI(0,0); //SPI_PRESCALER_DIV128_gc,SPI_MODE_3_gc);
	SPI_init(SPI_PRESCALER_DIV128);

	//--- Allow the card time to initialize.
	Delayms(100);	 //100

	//--- Generate clock cycles for 1 millisecond as required by the MultiMediaCard spec.
	//--- 32*8=256 (1ms time delay with clocks)
	for (timeout = 0; timeout < 32; timeout++)	  //32
	mSend8ClkCycles();

	//--- Select the card.
	SDC_CS(0);
	Delayms(1);	 //1
	WriteSPI(0xFF);WriteSPI(0xFF);
	//--- Issue the GO_IDLE_STATE command to select SPI mode.
SDC_CS(1);
	response = SendSDCCmd(GO_IDLE_STATE, 0x0);
	if (response.r1._byte == SDC_BAD_RESPONSE)
	{
		status = sdcCardInitCommFailure;
		goto InitError;
	}

	//--- A response of 01h means the card is in the idle state and is initializing.
	if (response.r1._byte != 0x01)
	{
		status = sdcCardNotInitFailure;
		goto InitError;
	}
//IDLE

/*Sitas eiluted idet del HC korteliu. Jei grazina 0x1AA, tai HC kortele */
	g_isSDHC = false;
	volatile uint32_t ans = 0;
	WriteSPI(0xFF);
	response = SendSDCCmd(CMD8, 0x1AA);
	if (response.r1._byte == 5 /*SDC_BAD_RESPONSE*/ ){ //jei ILLEGAL_CMD
		//Jei ILLEGAL_CMD, tai SD version 1 or MMC version 3
	}
	else{
		//SD version 2
		SDC_CS(0);
		ans = ReadMedia() << 16;
		ans |= ReadMedia() << 8;
		ans |= ReadMedia();
		WriteSPI(0xFF);
		SDC_CS(1);
		//Kai SD version 2, tai atsakymas butinai turi buti 0x1AA. Jei ne - atmesti
		if(ans != 0x1AA){
			//g_isSDHC = true;
			status = sdcCardTypeInvalid;
			goto InitError;
		}
	}


	//--- Issue the SEND_OP_COND command until the card responds or a timeout.
	timeout = 0xFFF;	 //fff
	do{
//		if(g_isSDHC){
			SendSDCCmd(APP_CMD, 0);
			response = SendSDCCmd(SD_APP_OP_COND, 1 << 30); //ACMD41
//		}
//		else{
//			response = SendSDCCmd(SEND_OP_COND, 0x0); //Sitas paprastom kortelem
//		}
		timeout--;
		delay_us(1000);
	} while (response.r1._byte != 0x00 /*&& response.r1._byte != 0x01*/ && timeout != 0);

WriteSPI(0xFF);

	if (timeout == 0){
		timeout = 0xFFF;
		do{
			response = SendSDCCmd(SEND_OP_COND, 0); //CMD1
			timeout--;
			delay_us(1000);
		} while (response.r1._byte != 0x00 && timeout != 0);

		if(timeout == 0){
			status = sdcCardInitTimeout;
			goto InitError;
		}
	}
	else
	{
		g_isSDHC = false; //Make assumption that it is not high capacity card
		{
			//Super stupid way...
			uint8_t b;
			SDC_CS(0);
			WriteSPI(0x7A);
			WriteSPI(0x00);
			WriteSPI(0x00);
			WriteSPI(0x00);
			WriteSPI(0x00);
			WriteSPI(0x01);
			WriteSPI(0xFF);
			ReadMedia();
			b=ReadMedia();
			ReadMedia();
			ReadMedia();
			ReadMedia();
			SDC_CS(1);

			//Check HCS flag (bit 30 in the answer of READ_OCR).
			if(b & 0x40){
				//If 1, then my assumption was incorrect. The card is SDHC (high capacity)
				g_isSDHC = true;
			}
		}


		SDC_Error CIDstatus;
		CIDstatus=CIDRead();
		if (CIDstatus)
			goto InitError;

		//-- The command succeeded.
		//-- Read the CSD register.
		CSDstatus = CSDRead();
//TODO: SPI greitis cia turetu buti keiciamas:
//      SIUO METU GREITIS PO INIT NEPAKEICIAMAS!!!
		if (!CSDstatus)
		{
			//--- The response was zero. The CSD was read successfully.
			//--- OK to increase the clock speed.
			 word	a0, a1, a2;
			 a0=gblCSDReg._byte[3];
			 a0=a0 & 0x7;

			 if (a0==0)
			 {
				 a1=100;	//100khz
			 }
			 else if(a0==1)
			 {

				 a1=1000;	//1000khz
			 }
			 else
			 {
				 a1=10000;//10MHz
			 }

			 a0=gblCSDReg._byte[3];
			 a0=a0 & 0x78;
			 a0=a0>>3;

			 if (a0<=5)
			 {
				 a2=a1;
			 }
			 else	 if(a0>=5 && a0<9)
			 {
				 //a2=a1<<1;
				 a2=a1;
				 a2=a2<<1;
			 }
			 else
			 {
				// a2= a1<<2;
				 a2=a1;
				 a2=a2<<2;
			 }
			 gblSpeed_kHz=a2;

			if (gblSpeed_kHz>=8000)
			{
				//OpenSPI(0,0);//SPI_PRESCALER_DIV4_gc, SPI_MODE_3_gc); //TODO: NEREALIZUOTA!!!
				SPI_init(SPI_PRESCALER_DIV4); //24M/4=6M
			}
			else if(gblSpeed_kHz>=2000)
			{
				 //OpenSPI(0,0); //SPI_PRESCALER_DIV16_gc, SPI_MODE_3_gc); //TODO: NEREALIZUOTA!!!
				 SPI_init(SPI_PRESCALER_DIV16); //24M/16=1.5M
			}
			else if (gblSpeed_kHz>=500)
			{
				//OpenSPI(0,0); //SPI_PRESCALER_DIV64_gc, SPI_MODE_3_gc); //TODO: NEREALIZUOTA!!!
				SPI_init(SPI_PRESCALER_DIV64); //24M/64=375kHz
			}
			//---else don't change speed

			//Now I would like to find this SD card size in bytes.
			if((gblCSDReg._byte[0] >> 6) == 0){
				//CSD Version 1.0
				g_size = (( (gblCSDReg._byte[6] & 0x03) << 10) + (gblCSDReg._byte[7] << 2) + (gblCSDReg._byte[8] >> 6) + 1) * (1 << ( ((gblCSDReg._byte[9] & 0x1C) >> 2) + 2)) * (1 << (gblCSDReg._byte[5] & 0x0F));
			}
			else if ((gblCSDReg._byte[0] >> 6) == 1){
				//CSD Version 2.0
				g_size = (uint64_t)(((gblCSDReg._byte[7] & 0x3F) << 16) + (gblCSDReg._byte[8] << 8) + gblCSDReg._byte[9] + 1) * 512 * 1024;
			}
			else{
				status = sdcCardTypeInvalid;
				goto InitError;
			}
		}
		else
		// Unable to read the CSD.
		status = sdcCardTypeInvalid;
	}

	//--- Issue the SET_BLOCKLEN command to set the block length to 512.
	//--- (Optional, since this is the default.)

	SendSDCCmd(SET_BLOCKLEN, 512);

	//--- Set a bit in the SDCSTATE structure if the card is write-protected.
	if (IsWriteProtected())
	Flag -> isWP = TRUE;
	// Read sector zero from the card into msd_buffer until success or a timeout.
	// Some cards require multiple attempts.
	for (timeout = 0xFF;timeout > 0 && SectorRead(0x0, (byte*)msd_buffer) != sdcValid;timeout--)
	// The attempt to read timed out.
	if (timeout == 0)
	{
		status = sdcCardNotInitFailure;
		goto InitError;
	}

	return(status);


	InitError:
	// On error or success, deselect the device.
	SDC_CS (1) ;
	return(status);
}

//In sectors (512 bytes each)
uint32_t SDCardSize(){
	return (uint32_t)(g_size / 512);
}
//---------------------------------------------------------
uint8_t DetectSDCard (void){
	return(1);
}

//void SocketInitialize(void){
//	// --- configure MOSI, SS, CLK as outputs on PORT, and  MISO as input(pin6)
//	mySPI_PORT.DIRCLR = 0xF0;
//	mySPI_PORT.DIRSET = 1<<7|1<<5|1<<4;		// as output, 1<<6 as input(MISO)
//	mySPI_PORT.OUTSET = 1<<7|1<<5<<4;		// set high
//	SDC_CS(1);
//};

void OpenSPI(uint8_t FOSC_PRESCALER, uint8_t MODE ){
	//--- enable SPI master mode, CLK/128
	//SPID.CTRL = SPI_ENABLE_bm | SPI_MASTER_bm | SPI_MODE_3_gc | SPI_PRESCALER_DIV128_gc;
	///mySPI.CTRL =	SPI_ENABLE_bm |	  FOSC_PRESCALER | MODE |  SPI_MASTER_bm;
///	SPI_init(SPI2);
}

uint8_t WriteSPI(uint8_t data_out){
//  mySPI.DATA = data_out;      // initiate write
//  if (mySPI.STATUS & SPI_WRCOL_bm)
//  {
//	  return(-1) ;
//  }
//  else
//  {
//	while(!(mySPI.STATUS & SPI_IF_bm));
//	return (0);
//  }
///	SPI_SendData(SPI2, data_out);

	///HAL_SPI_Transmit(&hspi2, &data_out, 1, 1000);
	uint8_t r;
	SPI_SendReceiveByte(data_out, &r, 1000);

	return 0;
};

uint8_t ReadMedia(void){
//  mySPI.DATA =0xFF;
//  while(!(mySPI.STATUS & SPI_IF_bm));
//  return mySPI.DATA;
	uint8_t result = 0;
	uint8_t tx;
	tx=0xFF;
///result = SPI_SendData(SPI2, 0xFF);
	//HAL_SPI_Receive(&hspi2, &result, 1, 1000);
	///HAL_SPI_TransmitReceive(&hspi2, &tx, &result, 1, 1000);

	SPI_SendReceiveByte(tx, &result, 1000);

	return result;
};

char SpiWriteRead(char data_out){
	uint8_t result;
	///result = SPI_SendData(SPI2, data_out);
	///WriteSPI(data_out);
	///HAL_SPI_TransmitReceive(&hspi2, &data_out, &result, 1, 1000);

	SPI_SendReceiveByte(data_out, &result, 1000);

	return result;
};

void SDC_CS(uint8_t cs)
{
	if (cs==0)
	{
		//GPIOB -> BSRRH |= (1<<12); // reset B12
		//mySPI_PORT.OUTCLR = 1<<4;  // lower ss line indicating start of transfer
		GPIOB->ODR &= ~(1<<12);
	}
	else if (cs==1)
	{
		//GPIOB -> BSRRL |= (1<<12); // set B12
		 //mySPI_PORT.OUTSET = 1<<4;   // raise ss line indicating end of transfer
		GPIOB->ODR |= (1<<12);
	}
};


