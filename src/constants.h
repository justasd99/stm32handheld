#include "ugui.h"

#define RIGHT_MENU_ITEM_HEIGHT	50
#define RIGHT_MENU_WIDTH	      106
#define STATUS_BAR_HEIGHT	      20
#define DISPLAY_WIDTH	          480
#define DISPLAY_HEIGHT	        272
  
#define GRAPH_LINE_WIDTH  2
#define GRAPH_START_X	    12
#define GRAPH_START_Y	    20
#define GRAPH_WIDTH		    360
#define GRAPH_HEIGHT	    240
#define GRAPH_ZERO_POS    (GRAPH_HEIGHT/8)
#define GRID_DENSITY	    30

//--- adc calibration
#define ADC_CAL_SUM_CNT       200      
#define ADC_CAL_ERROR_MT      10
#define ADC_CAL_SCALE(x)      ((x*231)/100)
#define ADC_CAL_ADC_MAX       (1<<14)
#define ADC_CAL_ADC_STEP_MAX  500
#define ADC_CAL_EST_CNT       5
#define ADC_CAL_TRY_CNT       30
#define ADC_CAL_REJ_CNT       1
#define ADC_CAL_TMR_P         500 // aditional timer to calculate B avg

#define MAX_MKS_DIV		    5000000UL
#define TESLA_MAX		      10
#define MILITESLA_MIN 	  50
#define MAX_MT_DIV		    (TESLA_MAX * 1000)
#define DEFAULT_V_DIV     100                   // mT 
#define DEFAULT_H_DIV     100000                // us
#define SETTINGS_UPD_TIME 10000                 // ms 

#define TEMPERATURE_BUFFER_LENGTH	600

//--- redifined colors 
#define GRAPH_ZERO_POS_C  C_YELLOW    // zero point trigger arrow
#define GRAPH_BACK_C      C_GRAY      // all background
#define GRAPH_TRIGGER_C   C_GREEN      // trigger arrow
#define GRAPH_TEXT_LN_C   C_WHITE    // the upper line(below text)
#define C_GRAPGH_CNTR     C_WHITE  // contour color

//--- 16 bit user color coded
#define  C_ZOOM_U_16b                 0xA514              // zoom
#define  C_ZOOM_S_16b                 0x4208              // zoom
#define  C_ZOOM_B_16b                 0x630C              // zoom
#define  C_TRIGGER_16b                RGB565(GRAPH_TRIGGER_C)//0x07FF              // trigger:  horiz & vert line colors
#define  C_SCREEN_16b                 RGB565(C_BLACK)     // graph black-screen
#define  C_TICKETS_16b                RGB565(C_WHITE)     // graph tickets, white
#define  C_GRID_16b                   RGB565(C_WHITE)     // graph grid, white
#define  C_USR_LINE_16b               RGB565(C_YELLOW)    // graph line, yellow   
#define  C_USR_LINE_ADC_CAL_16b       RGB565(C_SILVER)      // graph line, when auto adc calibration runs 
#define  C_ZERO_POS_16b               RGB565(C_WHITE)     // zero position line


