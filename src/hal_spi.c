#include "hal_spi.h"

static bool SPI_EndRxTxTransaction(uint32_t Timeout);
static bool SPI_WaitFlagStateUntilTimeout(uint32_t Flag, uint32_t State, uint32_t Timeout);
static bool SPI_WaitFifoStateUntilTimeout(uint32_t Flag, uint32_t State, uint32_t Timeout);

void SPI_init(uint32_t prescaler){
	RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;
	SPI2->I2SCFGR &= ~SPI_I2SCFGR_I2SMOD; //reset (default)
	SPI2->CR1 &=  ~SPI_CR1_SPE; //Disable SPI2
	SPI2->CR1 = 0x304;
	SPI2->CR1 |= prescaler;
	SPI2->CR2 = 0x1700;

//	  spi->CR2 = SPI_CR2_SSOE;
//	  spi->CR1 = (SPI_CR1_SPE
//		       | (SPI_CR1_BR_2)	| (SPI_CR1_BR_1) /* fPCLK / 32 */
//		       | SPI_CR1_MSTR
//		       | SPI_CR1_SSM | SPI_CR1_SSI);

	RCC -> AHBENR |= RCC_AHBENR_GPIOBEN;
	GPIOB->MODER |= 0xA9000000UL;
	GPIOB->PUPDR |= 0x54000000UL;
	GPIOB->OSPEEDR |= 0xFC000000UL;
	GPIOB->AFR[1] |= 0x55500000UL;
}

bool SPI_SendReceiveByte(/*SPI_TypeDef *spi,*/ uint8_t txByte, uint8_t *rxByte, uint8_t Timeout){

	//rxtx:

SET_BIT(SPI2->CR2, SPI_CR2_FRXTH);
//

	/* Check if the SPI is already enabled */
	if((SPI2->CR1 & SPI_CR1_SPE) != SPI_CR1_SPE){
		/* Enable SPI peripheral */
		SPI2->CR1 |= SPI_CR1_SPE;
	}

    /* Wait until TXE flag is set to send data */
    if(!SPI_WaitFlagStateUntilTimeout(SPI_SR_TXE,SPI_SR_TXE,Timeout)){
    	return false;
    }

    *((__IO uint8_t*)&SPI2->DR) = txByte;

//----------------------------

    if(!SPI_WaitFlagStateUntilTimeout(SPI_SR_RXNE, SPI_SR_RXNE, Timeout)){
    	return false;
    }

    *rxByte =  *(__IO uint8_t *)&SPI2->DR;

//----------------------------

    /* Check the end of the transaction */
	if(!SPI_EndRxTxTransaction(Timeout)){
		return false;
	}

	/* Clear OVERUN flag in 2 Lines communication mode because received is not read */
//		__HAL_SPI_CLEAR_OVRFLAG(hspi);

	return true;
}

/* Procedure to check the transaction complete */
static bool SPI_EndRxTxTransaction(uint32_t Timeout){
	if(!SPI_WaitFifoStateUntilTimeout(SPI_SR_FTLVL, 0, Timeout)){
		return false;
	}
	if(!SPI_WaitFlagStateUntilTimeout(SPI_SR_BSY, 0, Timeout)){
		return false;
	}
	if(!SPI_WaitFifoStateUntilTimeout(SPI_SR_FRLVL, 0, Timeout)){
		return false;
	}
	return true;
}

static bool SPI_WaitFlagStateUntilTimeout(uint32_t Flag, uint32_t State, uint32_t Timeout){

	while((SPI2->SR & Flag) != State){
		if(Timeout-- == 0){
			SPI2->CR2 &= (~(SPI_CR2_TXEIE | SPI_CR2_RXNEIE | SPI_CR2_ERRIE));
			return false;
		}
		delay_us(1);
	}

	return true;
}

static bool SPI_WaitFifoStateUntilTimeout(uint32_t Flag, uint32_t State, uint32_t Timeout){
	__IO uint8_t tmpreg;

	while((SPI2->SR & Flag) != State){
		if((Flag == SPI_SR_FRLVL) && (State == 0)){
			tmpreg = *((__IO uint8_t*)&SPI2->DR);
			///UNUSED(tmpreg); /* To avoid GCC warning */
		}
		if(Timeout-- == 0){
			SPI2->CR2 &= (~(SPI_CR2_TXEIE | SPI_CR2_RXNEIE | SPI_CR2_ERRIE));
			return false;
		}
		delay_us(1);
	}
	return true;
}
