/* Includes -------------------------------------------------------------------------*/

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "constants.h"
#include "mxconstants.h"

/* For Graphics: */
#include "ssd1963.h"
#include "ugui.h"
#include "dialog.h"

/* For Menu Items */
#include "menu.h"
#include "info.h"
#include "credits.h"
#include "help.h"

/* For Periphery */
#include "ext_adc.h"
#include "flash_eeprom.h"
#include "hal_i2c.h"
#include "hal_rtc.h"
#include "crc.h"
#include "i2c_eeprom.h"
#include "sd_card.h"

/* Other */
#include "calibration.h" //Functions for processing calibration data
#include "delay.h" //Micro and Mili Seconds delay functions
#include "ff.h" //FatFS library

#include "main.h"


STATUS g_status; //volatile pasalinau
SETTINGS g_settings;
AdcCAlibration_t adcCal;

/* Global Variables -----------------------------------------------------------------*/
int g_Settings_timer_ms = SETTINGS_UPD_TIME;

/* For Processing of Data */
extern uint16_t g_adcBuffer[ADC_MAX_SAMPLES]; //With ext_adc.h
const uint16_t g_currentADC_BufferLength = ADC_MAX_SAMPLES; //
int16_t g_temperatureBuffer[TEMPERATURE_BUFFER_LENGTH];
uint16_t g_temperatureBufferPosition;
CALIBRATION_TABLE g_calibrationTable;
uint16_t g_previewBuffer[GRAPH_WIDTH + 1];
uint16_t g_previewBufferPosition;

/* For Graphics */
volatile BUTTON_STATUS g_buttonStatus;
volatile BOOL g_drawInProgress = FALSE;
UG_GUI gui; //This is required by uGUI library (for more information read uGUI documentation)


/* Other */
struct SystemCalibration g_systemCalibrationData;
uint32_t g_sysTick = 0;

//--- 2018.07.19 added for fixing bug of "force" trigger
volatile bool g_isInited = true;
void ForceReInitExtAdc()
{
  initExtADC();
	EXTADC_POWERUP();
	stopMeasurement();
	clearBuffers();
	g_status.isDataReceived = false;
}


/* ========================================================================================== */
/* Declarations ----------------------------------------------------------------------------- */
/* ========================================================================================== */
int main(void){

	gui.pset = NULL;
	g_status.isTouchScreenInitialized = false;
	g_buttonStatus.button = BUTTON_NONE; //No button pressed (dar nebuvo nuspaustas joks mygtukas)

    /* MCU Configuration ----------------------------------------------------------*/

	SystemClock_Config();
	InternalADC1_Init();
	{
		/* If battery is discharged too much do not let device to start. Go to sleep
		   instead and wait until charger is connected. */
		RCC->APB1ENR |= RCC_APB1ENR_PWREN; //For wkup
		PWR->CSR |= PWR_CSR_EWUP2; //For Wake Up pin config
		if(getBatteryStatus() <= 3){ //Do not let it to turn on if battery charge is lower than 3%
			enterStandbyMode(); //PC13 to Wake Up
		}
	}
	/* Battery is not discharged or charger is connected so resume */

	GPIO_Init();
	delayInit();
	I2C_Init();
	RTC_init();
	CRC32_init();
	SSD_Init();

	UG_Init(&gui,(void(*)(UG_S16,UG_S16,UG_COLOR))SSD_SetPixel,480,272);
	UG_DriverRegister(DRIVER_FILL_FRAME, (void*)SSD_drawSquare);
	UG_DriverEnable(DRIVER_FILL_FRAME);

	initExtADC();
	EXTADC_POWERUP();

  STMPE_Init();
	//Kolkas spejimo budu kalibravimas LCD. TODO: Daryti, kad veiktu tai per service menu.
	STMPE_setOffsetX(-10);
	STMPE_setOffsetY(-10);

	/* Restore settings -----------------------------------------------------------*/

	/* Get sensor calibration data from MCU internal flash memory */
	g_calibrationTable.magnMin = (uint16_t *)(CALIBRATION_DATA_START_ADDRESS + 15);
	g_calibrationTable.magnStep = (uint16_t *)(CALIBRATION_DATA_START_ADDRESS + 19);
	g_calibrationTable.magnMax = (uint16_t *)(CALIBRATION_DATA_START_ADDRESS + 17);
	g_calibrationTable.tempMin = (uint16_t *)(CALIBRATION_DATA_START_ADDRESS + 21);
	g_calibrationTable.tempStep = (uint16_t *)(CALIBRATION_DATA_START_ADDRESS + 25);
	g_calibrationTable.tempMax = (uint16_t *)(CALIBRATION_DATA_START_ADDRESS + 23);
	g_calibrationTable.table = (uint16_t *)(CALIBRATION_DATA_START_ADDRESS + 128);
	g_calibrationTable.columns = (uint16_t)((*g_calibrationTable.magnMax - *g_calibrationTable.magnMin + *g_calibrationTable.magnStep) / *g_calibrationTable.magnStep);
	g_calibrationTable.rows = (uint16_t)((*g_calibrationTable.tempMax - *g_calibrationTable.tempMin + *g_calibrationTable.tempStep) / *g_calibrationTable.tempStep);

	/* Default status */
	//memset(g_previewBuffer, 0, GRAPH_WIDTH * sizeof(uint16_t));
  for(int i = 0; i < GRAPH_WIDTH; i++)
    g_previewBuffer[i] = 0;
  
	g_temperatureBufferPosition = 0;
	g_status.isZoomButtonsEnabled = true;
	g_status.isBackToZeroButtonEnabled = true;
	g_status.mode = CONTINUOUS; //init
	g_status.isMeasurementStarted = TRUE;
	g_status.graphOffsetX = 0;
	//g_settings.hDiv = 1;
	//g_settings.pulseLength_us = 12;
	//g_currentADC_BufferLength = ADC_MAX_SAMPLES; //Pastebejau, kad nekeiciu jau buferio dydzio visai. TODO: padaryti kaip const
	g_status.offsetMiliTeslaTmp = 0;

	/* Restore device settings from virtual EEPROM (use MCU internal flash because this MCU does not have EEPROM) */
	restoreSettings();

	/* ---------------------------------------------------------------------------------------------------------- */

	enum cal_status calStatus;
	bool importedCorrectly; //, isSensorConnected;
	importedCorrectly = false;

	/* Check if user wants to enter the service menu */
	if(((GPIOA->IDR & 0x0100) == 0) && ((GPIOB->IDR & 0x0002) == 0)){
		serviceMenu();
	}

	/* Import connected sensor calibration data if required:  */
	do{
		UG_FillScreen(C_BLACK);
		updateBatteryStatus();
		g_status.isSensorConnected = calibration_isSensorConnected();
		if(g_status.isSensorConnected){
			calStatus = calibration_retrieveSensorCalibrationData();
			if(calStatus != SAME_SENSOR)
				infoSensorConnected(calStatus);
			if(calStatus == SAME_SENSOR || calStatus == SUCCESSFUL){
				importedCorrectly = true;
			}
		}
		else{
		  msg_sensorDisconnected("No sensor. Connect.");
		}
	}
	while(!importedCorrectly || !g_status.isSensorConnected);

	g_settings.offsetMiliTesla = calibration_getOffset();
	g_status.isSensorConnectionStatusChanged = false;

	stopMeasurement();
	clearBuffers();
	g_status.isDataReceived = false;

   while(1)
   {
    int8_t rightMenuSelectedItemId;
    g_status.isMainWindow = TRUE;
    
    UG_DrawLine(11, 19, 480, 19, GRAPH_TEXT_LN_C); //Linija po busenos juosta (TODO: perkelti kur nors, kad butu aiskiau)
    mainWindow();
    while(STMPE_isPressed() == FALSE && !g_status.startStopMeasurementButtonPressed && g_buttonStatus.button != BUTTON_REC)
    {
      refreshMainWindowStatus();
      drawGraph(UNUSED, NULL);
      refreshPositionArrows();

      if(g_status.isSensorConnectionStatusChanged)
      {
       if(!g_status.isSensorConnected)
         msg_sensorDisconnected("Sensor disconnected.");
       if(g_status.isSensorConnected)
       {
         enum cal_status calStatus;
         UG_PutString(50, 50, "Importing sensor data...");
         calStatus = calibration_retrieveSensorCalibrationData();
         g_settings.offsetMiliTesla = calibration_getOffset();
         if(calStatus != SAME_SENSOR){
           infoSensorConnected(calStatus);
           //TODO: Paziuret, lygtai niekur kitur po jutiklio prijungimo as to nedarau???
           g_calibrationTable.magnMin = (uint16_t *)(CALIBRATION_DATA_START_ADDRESS + 15);
           g_calibrationTable.magnStep = (uint16_t *)(CALIBRATION_DATA_START_ADDRESS + 19);
           g_calibrationTable.magnMax = (uint16_t *)(CALIBRATION_DATA_START_ADDRESS + 17);
           g_calibrationTable.tempMin = (uint16_t *)(CALIBRATION_DATA_START_ADDRESS + 21);
           g_calibrationTable.tempStep = (uint16_t *)(CALIBRATION_DATA_START_ADDRESS + 25);
           g_calibrationTable.tempMax = (uint16_t *)(CALIBRATION_DATA_START_ADDRESS + 23);
           g_calibrationTable.table = (uint16_t *)(CALIBRATION_DATA_START_ADDRESS + 128);
           g_calibrationTable.columns = (uint16_t)((*g_calibrationTable.magnMax - *g_calibrationTable.magnMin + *g_calibrationTable.magnStep) / *g_calibrationTable.magnStep);
           g_calibrationTable.rows = (uint16_t)((*g_calibrationTable.tempMax - *g_calibrationTable.tempMin + *g_calibrationTable.tempStep) / *g_calibrationTable.tempStep);
         }
       }
       g_status.isSensorConnectionStatusChanged = false;
      }
      
      if((g_status.temperature < *g_calibrationTable.tempMin * 10 || g_status.temperature > *g_calibrationTable.tempMax * 10) && g_status.isMeasurementStarted)
      {
       stopMeasurement();
       startMeasurement(); //Just to show a message that temperature is out of bounds. It will not start.
       mainWindow(); //To refresh right menu
      }
      
      //--- update battery and save settings to eeprom memory
      if(g_Settings_timer_ms <= 0){
        saveSettings();
        updateBatteryStatus();
        g_Settings_timer_ms = SETTINGS_UPD_TIME;
      }
    }

    //---
    if(STMPE_isPressed())
    {
     rightMenuSelectedItemId = getRightMenuSelectedItemId();
     switch(rightMenuSelectedItemId)
     {
       case 0:
         if(g_status.isMeasurementStarted && g_status.mode != SINGLE_SEQUENCE && g_status.mode != EXTERNAL)
           stopMeasurement();
         else
           //startMeasurement();
           g_status.startStopMeasurementButtonPressed = true;
       break;/**/
       
       case 1:
       {
         if(!g_status.isMeasurementStarted)
           save(g_settings.saveFormat);
       }
       break;
       
       case 2:
         char *itemList[4];
         int8_t choice;
         itemList[0] = "External";
         itemList[1] = "Force"; //Only run for specified time
         itemList[2] = "Continuous";
         itemList[3] = "Internal"; //A.k.a Single Sequence
         choice = drawPopupMenu(374, 20 + RIGHT_MENU_ITEM_HEIGHT * 2 + RIGHT_MENU_ITEM_HEIGHT / 2, itemList, 4); //Draw in the middle of middle menu item
         if(choice != -1){
           stopMeasurement();
           g_status.mode = choice;
           if(g_status.mode == SINGLE_SEQUENCE) 
             rmenu_setTriggerLevel();
           if(g_status.mode == FORCE) 
           {
             g_status.isMeasurementStarted = true; // added 2018.07.19
             ExtADC_clearBuffer();
           }
           else 
             clearBuffers();

           //TODO: Nesamoningas uzrasymas sitas if'as - rasti normalu grazu sprendima:
           if(g_status.mode == SINGLE_SEQUENCE || g_status.mode == EXTERNAL){ //Because these cases are blocking
             g_status.startStopMeasurementButtonPressed = false;
           }
           if(g_status.mode == CONTINUOUS){
             //To approx to std div if it is not standard.
             changeStdHorizontalDiv(true);
             changeStdHorizontalDiv(false);
           }
         }
       break;
       case 3:
         {
           if(g_status.mode == SINGLE_SEQUENCE || g_status.mode == EXTERNAL || (g_status.mode == FORCE && !g_status.isMeasurementStarted)){
             rmenu_setLength();
           }
         }
         break;
       case 4:
         {
           char *itemList[3];
           int8_t choice;
           itemList[0] = g_status.isZoomButtonsEnabled ? "[\x9E] Zoom" : "[ ] Zoom Buttons ";
           itemList[1] = g_status.isBackToZeroButtonEnabled ? "[\x9E] Back Button " : "[ ] Back Button ";
           itemList[2] = "    Sub Menu   "; //"    Main Menu   "; //Only run for specified time
           choice = drawPopupMenu(374, 20 + RIGHT_MENU_ITEM_HEIGHT * 4 + RIGHT_MENU_ITEM_HEIGHT / 2, itemList, 3); //Draw in the middle of middle menu item
           switch(choice){
             case 0: g_status.isZoomButtonsEnabled = !g_status.isZoomButtonsEnabled; break;
             case 1: g_status.isBackToZeroButtonEnabled = !g_status.isBackToZeroButtonEnabled; break;
             case 2: EXTADC_POWERDOWN(); mainMenu(); EXTADC_POWERUP(); break;
           }
         }
         break;
       default:
       {
         //Paspausta buvo ne ant meniu
         enum zoomButtonPressed zoomButtonPressed = NONE;
         bool backToZeroButtonPressed = false;
         if(g_status.isZoomButtonsEnabled){
           memset(g_previewBuffer, '\0', sizeof(g_previewBuffer));
           zoomButtonPressed = zoomButtons();
           switch(zoomButtonPressed){
             case VZoomIn: 
                  changeStdVerticalDiv(true); 
                  g_Settings_timer_ms = 0; 
             break;
             case VZoomOut: 
                  changeStdVerticalDiv(false); 
                  g_Settings_timer_ms = 0; 
             break;
             case HZoomIn: 
                  changeStdHorizontalDiv(FALSE);
                  g_Settings_timer_ms = 0; 
             break;
             case HZoomOut: 
                  changeStdHorizontalDiv(TRUE);
                  g_Settings_timer_ms = 0; 
             break;
             default:;
           }
           if((zoomButtonPressed == HZoomIn || zoomButtonPressed == HZoomOut) && g_status.isMeasurementStarted)
             ExtADC_clearBuffer();
         }
         if(g_status.isBackToZeroButtonEnabled){
           const uint16_t xStart = GRAPH_START_X;
           const uint16_t yStart = GRAPH_START_Y;
           const uint16_t xEnd = xStart + GRAPH_WIDTH;
           //const uint16_t yEnd = yStart + GRAPH_HEIGHT;
           //enum zoomButtonPressed pressed = NONE;
           uint16_t ts_x, ts_y;
           uint8_t xIcon, yIcon;
           ts_x = STMPE_getX0();
           ts_y = STMPE_getY0();

           if((ts_x < xEnd - 10 && ts_x > xEnd - 60) && (ts_y > yStart + 10 && ts_y < yStart + 60)){
             //Back to zero
             g_status.graphOffsetX = 0;
             g_status.offsetMiliTeslaTmp = 0;

             UG_FillFrame(xEnd - 50, yStart + 10, xEnd - 10, yStart + 50, 0x505050);
             UG_DrawLine(xEnd - 51, yStart + 9, xEnd - 9, yStart + 9, 0x404040);
             UG_DrawLine(xEnd - 9, yStart + 9, xEnd - 9, yStart + 51, 0xA0A0A0);
             UG_DrawLine(xEnd - 51, yStart + 9, xEnd - 51, yStart + 51, 0x404040);
             UG_DrawLine(xEnd - 51, yStart + 51, xEnd - 10, yStart + 51, 0xA0A0A0);

             for(xIcon = 0; xIcon < 40; xIcon++){
               for(yIcon = 0; yIcon < 40; yIcon++){
                 if(g_backToZeroIcon[xIcon][yIcon / 8] & (0x80 >> yIcon % 8)){
                   UG_DrawPixel(xEnd - 50 + xIcon, yStart + 10 + yIcon, C_BLACK);
                 }
               }
             }
             backToZeroButtonPressed = true;
           }
         }
         if((!g_status.isBackToZeroButtonEnabled || backToZeroButtonPressed == false) && (!g_status.isZoomButtonsEnabled || zoomButtonPressed == NONE)){
           while(STMPE_isPressed() && !g_status.isMeasurementStarted){
             delay_ms(10); //To be sure that STMPE_ReleaseCheck() was executed before continuing
             int16_t deltaX_touch = STMPE_getX() - STMPE_getX0();
             int16_t deltaY_touch = STMPE_getY() - STMPE_getY0();
             STMPE_resetX0();
             STMPE_resetY0();
             g_status.offsetMiliTeslaTmp -= (int16_t)((8 * (int32_t)g_settings.vDiv * (int32_t)deltaY_touch) / GRAPH_HEIGHT);
             if(g_status.offsetMiliTeslaTmp > TESLA_MAX * 1000)
               g_status.offsetMiliTeslaTmp = TESLA_MAX * 1000;
             else if(g_status.offsetMiliTeslaTmp > 0)
               g_status.offsetMiliTeslaTmp = 0; //TODO: rodyt truputi apacios ateityje!
             g_status.graphOffsetX += deltaX_touch;
             drawGraph(UNUSED, NULL);
             refreshPositionArrows();
           }
         }
       }
     }
     while(STMPE_isPressed()); //Wait for release
    }
  
    
    //---- putted it in System tick timer
    if(g_status.startStopMeasurementButtonPressed){
      //g_status.isMeasurementStarted ? stopMeasurement() : startMeasurement();
      if(g_status.isMeasurementStarted )
        stopMeasurement();
      else{
        if(g_isInited == false){ // have problems after force measure. sow I am trying to reinit all!!!
          ForceReInitExtAdc();   // start external timer just after force mode
          g_isInited = true;
        }
        startMeasurement();
      }
      g_status.startStopMeasurementButtonPressed = false;
    }/**/
    
    //---
    if(g_buttonStatus.button == BUTTON_REC){
     bool wasMeasurementStarted = g_status.isMeasurementStarted;
     if(g_status.isMeasurementStarted)
       stopMeasurement();
     save(g_settings.saveFormat);
     if(wasMeasurementStarted)
       startMeasurement();
     g_buttonStatus.button = BUTTON_NONE;
    }
    /**/
  }
}

void SysTick_Handler(){
	UG_GUI bkp_gui = gui;
	//g_sysTick++;
	increaseMilisecondTick();
	if(milisecondTick() % 1000 == 0 && gui.pset && !g_drawInProgress){
		uint8_t n;
		char dateTimeStr[35]         = "20YY-MM-DD HH:MM:SS        "; //%s\xF8\x43";
		//char mainWindowStatusStr[sizeof(dateTimeStr)];
		//char asciiInt[7]; //7 tikrai?
		if(g_status.isMainWindow){
			refreshMainWindowStatus();
		}
		else{
			n = (uint8_t)(((RTC->DR) >> 20) & 0x0000000F);
			dateTimeStr[2] = n + '0';
			n = (uint8_t)(((RTC->DR) >> 16) & 0x0000000F);
			dateTimeStr[3] = n + '0';
			n = (uint8_t)(((RTC->DR) >> 12) & 0x00000001);
			dateTimeStr[5] = n + '0';
			n = (uint8_t)(((RTC->DR) >> 8) & 0x0000000F);
			dateTimeStr[6] = n + '0';
			n = (uint8_t)(((RTC->DR) >> 4) & 0x00000003);
			dateTimeStr[8] = n + '0';
			n = (uint8_t)((RTC->DR) & 0x0000000F);
			dateTimeStr[9] = n + '0';

			n = (uint8_t)(((RTC->TR) >> 20) & 0x00000003);
			dateTimeStr[11] = n + '0';
			n = (uint8_t)(((RTC->TR) >> 16) & 0x0000000F);
			dateTimeStr[12] = n + '0';
			n = (uint8_t)(((RTC->TR) >> 12) & 0x00000007);
			dateTimeStr[14] = n + '0';
			n = (uint8_t)(((RTC->TR) >> 8) & 0x0000000F);
			dateTimeStr[15] = n + '0';
			n = (uint8_t)(((RTC->TR) >> 4) & 0x00000007);
			dateTimeStr[17] = n + '0';
			n = (uint8_t)((RTC->TR) & 0x0000000F);
			dateTimeStr[18] = n + '0';/**/

			/*if(g_status.isSensorConnected){
				char temperatureStr[6];
				temperatureAsString(temperatureStr);
				strcpy(&dateTimeStr[27], temperatureStr);
				dateTimeStr[32] = '\xF8';
				dateTimeStr[33] = 'C';
			}
			else{
				for(n = 27; n < 34; n++)
					dateTimeStr[n] = ' ';
			}*/
      for(n = 19; n < 34; n++)
					dateTimeStr[n] = ' ';
			dateTimeStr[34] = '\0';

			UG_SetForecolor(C_YELLOW);
			UG_SetBackcolor(C_BLACK);
			UG_FontSelect(&FONT_10X16);
			UG_PutString(1, 3, dateTimeStr);
		}
	}

  //--- Update temperature
	if(((milisecondTick() % ((g_settings.hDiv * 12 > 6000000UL) ? 100 : 10) == 0)) &&
     g_status.isSensorConnected)
  {
		
		uint8_t ans[6], tryCnt = 10;
		uint16_t tempADC;
		if(!I2C_isBusy(I2C2)){
			do{
				if(I2C_receiveBytes(I2C2, 0x78, ans, 6, 0)){
					break;
				}
			}
			while(--tryCnt);
			if(tryCnt){
				tempADC = (ans[0] << 8) | ans[1];
				g_status.temperature = tempADC * 100 / 64 - 3200; //Temp in Celsius multiplied by 100
			}
			if(g_status.isMeasurementStarted){
				if(tryCnt)
					g_temperatureBuffer[g_temperatureBufferPosition] = g_status.temperature;
				else{
					//Jei nepavyko ismatuoti
					if(g_temperatureBufferPosition >= 0)
						g_temperatureBuffer[g_temperatureBufferPosition] = g_temperatureBuffer[g_temperatureBufferPosition - 1];
					else
						g_temperatureBuffer[g_temperatureBufferPosition] = 2400; //24 laipsniai C - iprasta kambario temp
				}
				if(++g_temperatureBufferPosition == TEMPERATURE_BUFFER_LENGTH)
					g_temperatureBufferPosition = 0;
			}
		}
	}
  
  //--- update settings (write to eeprom)
  if(g_Settings_timer_ms > 0)
    g_Settings_timer_ms--;
  
  if(adcCal.timer_ms > 0)
    adcCal.timer_ms--;

	if(milisecondTick() % 500 == 0){
		bool isSensorExistsNow = calibration_sensorExists();
		if(g_status.isSensorConnected != isSensorExistsNow){
			g_status.isSensorConnectionStatusChanged = true;
			g_status.isSensorConnected = isSensorExistsNow;
		}
	}
  
  //--- read buttons ---
  static int st_butt_state_last = 0, sv_butt_state_last = 0;
  static unsigned short by_pass_cnt = 50;
  int st_butt_state = (GPIOA->IDR & 0x0100), sv_butt_state = ((GPIOB->IDR & 0x0002) == 0);
  
  if(by_pass_cnt)
    by_pass_cnt--;
  else{
    if(st_butt_state == 0 && st_butt_state_last){                               // high-to-low
      g_buttonStatus.button = BUTTON_OK;
      g_buttonStatus.isPressed = TRUE;
      g_status.startStopMeasurementButtonPressed = true;
    }
    else if(st_butt_state && st_butt_state_last == 0){                          // low-to-high
      g_buttonStatus.isPressed = FALSE;
    }
    if(sv_butt_state == 0 && sv_butt_state_last){
      g_buttonStatus.button = BUTTON_REC;
      g_buttonStatus.isPressed = ((GPIOB->IDR & 0x0002) == 0);
    }
    st_butt_state_last = st_butt_state;
    sv_butt_state_last = sv_butt_state;
    by_pass_cnt = 50;
  }
  //------------------------------------------------
  STMPE_ReleaseCheck();
  /*if(STMPE_isPressed())
  {
    if(getRightMenuSelectedItemId() == 0){
      if(g_status.isMeasurementStarted && g_status.mode != SINGLE_SEQUENCE && g_status.mode != EXTERNAL)
       stopMeasurement();
      else
       startMeasurement();
    }
    STMPE_forceUnpress();
  }
  else if(g_status.startStopMeasurementButtonPressed)
  {
    if(g_status.isMeasurementStarted )
      stopMeasurement();
    else
      startMeasurement();
    g_status.startStopMeasurementButtonPressed = false;
  }*/
  //************************************************************************

	if(g_status.mode == FORCE && g_status.isMeasurementStarted){
		if(ExtADC_transfered()){
			g_status.isMeasurementStarted = FALSE;
			mainWindow(); //Refresh
		}
	}
	gui = bkp_gui;
}


int8_t getRightMenuSelectedItemId(){
	int8_t selected = -1;
	if(STMPE_getX0() > DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 && STMPE_getY0() > STATUS_BAR_HEIGHT){
		selected = (STMPE_getY0() - STATUS_BAR_HEIGHT) / RIGHT_MENU_ITEM_HEIGHT;
	}
	return selected;
}

static void recalculateHorizontalDivAccordingPulseLengthAndMode(){
	uint32_t convertedLength_us;

//	if(g_status.mode == SINGLE_SEQUENCE || g_status.mode == EXTERNAL){
//		convertedLength_us = g_settings.pulseLength_us + g_settings.pulseLength_us / 10;
//	}
//	else{ //Force trigger
//		convertedLength_us = g_settings.pulseLength_us;
//	}
///	hDiv = convertedLength_us / 12 + (convertedLength_us % 12 > 0);
///	hiDigit = hDiv;
///	if(hiDigit > 10){
///		while(hiDigit > 100){
///			hiDigit /= 10;
///			multiplier *= 10;
///		}
///		multiplier *= 10;
///		hiDigit = hiDigit / 10 + (hiDigit % 10 > 0);
///	}
///	if(hDiv > hiDigit * multiplier)
///		hiDigit++;
///	if(hiDigit > 2 && hiDigit < 5)
///		hiDigit = 5;
///	else if(hiDigit > 5)
///		hiDigit = 10;
///	hDiv = hiDigit * multiplier;
///	assert(hDiv * 12 >= convertedLength_us);
	g_settings.hDiv = g_settings.pulseLength_us / 12; //convertedLength_us / 12 + (convertedLength_us % 12 > 0);
	if(g_status.isMeasurementStarted)
		updateSamplingFrequency();
}

//static void horizontalDivAppriximateToStandard(){
//	uint32_t hiDigit, hDiv, multiplier = 1;
//	hDiv = g_settings.hDiv;
//	hiDigit = hDiv;
//	if(hiDigit > 10){
//		while(hiDigit > 100){
//			hiDigit /= 10;
//			multiplier *= 10;
//		}
//		multiplier *= 10;
//		hiDigit = hiDigit / 10 + (hiDigit % 10 > 0);
//	}
//	if(hiDigit > 2 && hiDigit < 5)
//		hiDigit = 5;
//	else if(hiDigit > 5)
//		hiDigit = 10;
//	hDiv = hiDigit * multiplier;
//}

//------------

void cbSetTriggerLevel(DIALOG *dlg, DLG_MESSAGE *msg){
	uint16_t levelMiliTesla;
	DLG_SPINBOX *sbTesla, *sbMiliTeslaHundreds, *sbMiliTeslaTens, *sbMiliTeslaUnits;
	if((msg->id == OBJ_TYPE_BUTTON) && (msg->sub_id == 0)){
		sbTesla = (DLG_SPINBOX*)(getDialogObjectById(dlg, 3)->data);
		sbMiliTeslaHundreds = (DLG_SPINBOX*)(getDialogObjectById(dlg, 4)->data);
		sbMiliTeslaTens = (DLG_SPINBOX*)(getDialogObjectById(dlg, 5)->data);
		sbMiliTeslaUnits = (DLG_SPINBOX*)(getDialogObjectById(dlg, 6)->data);
		levelMiliTesla = sbTesla->valueCurrent * 1000 + sbMiliTeslaHundreds->valueCurrent * 100 + sbMiliTeslaTens->valueCurrent * 10 + sbMiliTeslaUnits->valueCurrent;
		if (levelMiliTesla >= MILITESLA_MIN){
			g_settings.triggerLevelMiliTesla = levelMiliTesla;
		}
		else{
			msgBox("Trigger level must be at least 50 mT.", "Error", CRITICAL, NULL);
			redrawDialog(dlg);
			return;
		}
	}
	if(msg->id == OBJ_TYPE_BUTTON)
		setDialogState(dlg, DLG_STATE_CLOSE);
}

void rmenu_setTriggerLevel(){
	DIALOG dlg;
	DLG_OBJECT objects[8];
	DLG_BUTTON butt[2];
	DLG_LABEL labelDot, labelTesla;
	DLG_SPINBOX sbTesla, sbMiliTeslaHundreds, sbMiliTeslaTens, sbMiliTeslaUnits;
	uint8_t ids[2] = {0, 1};
	uint16_t levelMiliTesla;
	char *captions[2];
	captions[0] = "OK";
	captions[1] = "Cancel";
	createDialog(&dlg, objects, 8, cbSetTriggerLevel);
	setDialogSize(&dlg, 350, 100);
	setDialogTitleText(&dlg, "Set Trigger Level");
	if(!createButtonsInButtonBox(&dlg, 2, butt, ids, captions))
	  return;
	createSpinBox(&dlg, &sbTesla, 3, 15, TITLE_HEIGHT + 5);
	createLabel(&dlg, &labelDot, 7, 15 + 70 - 7, TITLE_HEIGHT + 40, 0, 0);
	setLabelText(&labelDot, ".", C_WHITE, &FONT_16X26);
	createSpinBox(&dlg, &sbMiliTeslaHundreds, 4, 15 + 10 + 70, TITLE_HEIGHT + 5);
	createSpinBox(&dlg, &sbMiliTeslaTens, 5, 15 + 10 + 70 * 2, TITLE_HEIGHT + 5);
	createSpinBox(&dlg, &sbMiliTeslaUnits, 6, 15 + 10 + 70 * 3, TITLE_HEIGHT + 5);
	createLabel(&dlg, &labelTesla, 8, 15 + 10 + 70 * 4, TITLE_HEIGHT + 40, 0, 0);
	setLabelText(&labelTesla, "T", C_WHITE, &FONT_16X26);
	setSpinBoxInterval(&sbTesla, 0, 5);
	setSpinBoxInterval(&sbMiliTeslaHundreds, 0, 9);
	setSpinBoxInterval(&sbMiliTeslaTens, 0, 9);
	setSpinBoxInterval(&sbMiliTeslaUnits, 0, 9);
	levelMiliTesla = g_settings.triggerLevelMiliTesla;
	setSpinBoxValue(&sbTesla, levelMiliTesla / 1000);
	setSpinBoxValue(&sbMiliTeslaHundreds, levelMiliTesla % 1000 / 100);
	setSpinBoxValue(&sbMiliTeslaTens, levelMiliTesla % 100 / 10);
	setSpinBoxValue(&sbMiliTeslaUnits, levelMiliTesla % 10);
	do{
	  updateDialog(&dlg);
	}
	while(getDialogState(&dlg) == DLG_STATE_SHOWN);
}

void cbSetLengthError(DIALOG *dlg, DLG_MESSAGE *msg){
	if(msg->id == OBJ_TYPE_BUTTON){
		setDialogState(dlg, DLG_STATE_CLOSE);
	}
}

void cbSetLength(DIALOG *dlg, DLG_MESSAGE *msg){
	BOOL isValid = FALSE;
	if((msg->id == OBJ_TYPE_BUTTON) && (msg->sub_id == 0)){
		DLG_SPINBOX *sbHundreds, *sbTens, *sbUnits, *sbTimeUnits;
		uint32_t convertedLength_us;
		sbHundreds = (DLG_SPINBOX*)(getDialogObjectById(dlg, 2)->data);
		sbTens = (DLG_SPINBOX*)(getDialogObjectById(dlg, 3)->data);
		sbUnits = (DLG_SPINBOX*)(getDialogObjectById(dlg, 4)->data);
		sbTimeUnits = (DLG_SPINBOX*)(getDialogObjectById(dlg, 5)->data);
		convertedLength_us = sbHundreds->valueCurrent * 100 + sbTens->valueCurrent * 10 + sbUnits->valueCurrent;
		if(sbTimeUnits->valueCurrent == 1)
			convertedLength_us *= 1000;
		else if(sbTimeUnits->valueCurrent == 2)
			convertedLength_us *= 1000000UL;
		if(convertedLength_us >= 1 && convertedLength_us <= 5000000UL){
			g_settings.pulseLength_us = convertedLength_us*12; //*12 nes 12 langeliu
			//recalculateHorizontalDivAccordingPulseLengthAndMode();
			{
				g_settings.hDiv = convertedLength_us;
				if(g_status.isMeasurementStarted)
					updateSamplingFrequency();
			}
			memset(g_previewBuffer, 0, GRAPH_WIDTH * sizeof(uint16_t));
			clearBuffers();
			isValid = TRUE;
		}
		else if(convertedLength_us == 0){
			sbHundreds->valueCurrent = 0;
			sbTens->valueCurrent = 1;
			sbUnits->valueCurrent = 0;
			sbTimeUnits->valueCurrent = 1;
		}
		else if(convertedLength_us > 5000000UL){
			sbHundreds->valueCurrent = 0;
			sbTens->valueCurrent = 0;
			sbUnits->valueCurrent = 5;
			sbTimeUnits->valueCurrent = 2;
		}
	}
	if(msg->id == OBJ_TYPE_BUTTON){
		if(msg->sub_id == 0 && !isValid){
			DIALOG dlgError;
			DLG_OBJECT objects[3];
			DLG_BUTTON butt[1];
			DLG_LABEL label;
			DLG_STANDARD_ICON standardIcon;
			uint8_t ids[1] = {0};
			char *captions[1] = {"OK"};
			createDialog(&dlgError, objects, 3, cbSetLengthError);
			setDialogSize(&dlgError, 270, 80);
			setDialogTitleText(&dlgError, "Error");
			if(!createButtonsInButtonBox(&dlgError, 1, butt, ids, captions))
				return;

			createLabel(&dlgError, &label, 1, 78, TITLE_HEIGHT + 7, 0, 0);
			setLabelText(&label, "Value must be\nbetween 1\xE6s\nand 5s.", C_WHITE, &FONT_12X20);
			standardIcon = CRITICAL;
			createStandardIcon(&dlgError, &standardIcon, 2, 5, TITLE_HEIGHT + 5);
			do{
				updateDialog(&dlgError);
			}
			while(getDialogState(&dlgError) == DLG_STATE_SHOWN);
			redrawDialog(dlg);
		}
		else
			setDialogState(dlg, DLG_STATE_CLOSE);
	}
}

void rmenu_setLength(){
	DIALOG dlg;
	DLG_OBJECT objects[6];
	DLG_BUTTON butt[2];
	DLG_SPINBOX sbHundreds, sbTens, sbUnits, sbTimeUnits;
	static char *timeUnits[] = {"\xE6s", "ms", "s"};
	uint8_t ids[2] = {0, 1};
	char *captions[2];

	captions[0] = "OK";
	captions[1] = "Cancel";
	createDialog(&dlg, objects, 6, cbSetLength);
	setDialogSize(&dlg, 320, 100);
	setDialogTitleText(&dlg, "Set Scale");
	if(!createButtonsInButtonBox(&dlg, 2, butt, ids, captions))
	  return;

	createSpinBox(&dlg, &sbHundreds, 2, 15, TITLE_HEIGHT + 5);
	createSpinBox(&dlg, &sbTens, 3, 15 + 70, TITLE_HEIGHT + 5);
	createSpinBox(&dlg, &sbUnits, 4, 15 + 70 * 2, TITLE_HEIGHT + 5);
	createSpinBox(&dlg, &sbTimeUnits, 5, 15 + 10 + 70 * 3, TITLE_HEIGHT + 5);
	setSpinBoxInterval(&sbHundreds, 0, 9);
	setSpinBoxInterval(&sbTens, 0, 9);
	setSpinBoxInterval(&sbUnits, 0, 9);
	setSpinBoxTextItems(&sbTimeUnits, timeUnits, 3);

	uint32_t length = g_settings.pulseLength_us/12;
	//uint16_t length16;
	uint8_t units_id = 0;
	if(length / 1000000UL){
		units_id = 2;
		length /= 1000000UL;
	}
	else if(length / 1000U){
		units_id = 1;
		length /= 1000U;
	}
	//length16 = (uint16_t)(1ength);

	setSpinBoxValue(&sbHundreds, length / 100);
	setSpinBoxValue(&sbTens, length % 100 / 10);
	setSpinBoxValue(&sbUnits, length % 10);
	setSpinBoxValue(&sbTimeUnits, units_id);

	do{
	  updateDialog(&dlg);
	}
	while(getDialogState(&dlg) == DLG_STATE_SHOWN);
}

//-----------
void cbSetDate(DIALOG *dlg, DLG_MESSAGE *msg){
	if((msg->id == OBJ_TYPE_BUTTON) && (msg->sub_id == 0)){
		DLG_SPINBOX *yearSpin, *monthSpin, *daySpin, *hSpin, *minSpin;
		int year, month, day, h, min;
		TIME sTime;
		DATE sDate;

		yearSpin = (DLG_SPINBOX*)(getDialogObjectById(dlg, 3)->data);
		monthSpin = (DLG_SPINBOX*)(getDialogObjectById(dlg, 4)->data);
		daySpin = (DLG_SPINBOX*)(getDialogObjectById(dlg, 5)->data);
		hSpin = (DLG_SPINBOX*)(getDialogObjectById(dlg, 6)->data);
		minSpin = (DLG_SPINBOX*)(getDialogObjectById(dlg, 7)->data);
		year = yearSpin->valueCurrent;
		month = monthSpin->valueCurrent + 1;
		day = daySpin->valueCurrent;
		h = hSpin->valueCurrent;
		min = minSpin->valueCurrent;

		sDate.year = year - 2000;
		sDate.month = month;
		sDate.day = day;
		RTC_setDate(&sDate);

		sTime.hours = h;
		sTime.minutes = min;
		sTime.seconds = 0;
		RTC_setTime(&sTime);
	}
	if(msg->id == OBJ_TYPE_BUTTON)
		setDialogState(dlg, DLG_STATE_CLOSE);
}

void menu_setDate(){
	static char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	DIALOG dlg;
	DLG_OBJECT objects[11];
	DLG_BUTTON butt[2];
	DLG_LABEL label;
	DLG_SPINBOX sbYear, sbMonth, sbDay, sbHour, sbMin;

	TIME time;
	DATE date;
	time = RTC_getTime();
	date = RTC_getDate();

	  uint8_t ids[2] = {0, 1};
	  char *captions[2];
	  captions[0] = "OK";
	  captions[1] = "Cancel";
	  createDialog(&dlg, objects, 11, cbSetDate);
	  setDialogSize(&dlg, 400, 100);
	  setDialogTitleText(&dlg, "Set Date");

	  if(!createButtonsInButtonBox(&dlg, 2, butt, ids, captions))
		  return;

	  createSpinBox(&dlg, &sbYear, 3, 15, TITLE_HEIGHT + 5);
	  createSpinBox(&dlg, &sbMonth, 4, 15 + 70, TITLE_HEIGHT + 5);
	  createSpinBox(&dlg, &sbDay, 5, 15 + 70 * 2, TITLE_HEIGHT + 5);
	  createSpinBox(&dlg, &sbHour, 6, 15 + 10 + 70 * 3, TITLE_HEIGHT + 5);
	  createSpinBox(&dlg, &sbMin, 7, 15 + 10 + 70 * 4, TITLE_HEIGHT + 5);
	  setSpinBoxInterval(&sbYear, 2016, 2100);
	  setSpinBoxValue(&sbYear, 2000 + date.year);
	  setSpinBoxTextItems(&sbMonth, months, 12);
	  setSpinBoxInterval(&sbDay, 1, 31);
	  setSpinBoxInterval(&sbHour, 0, 23);
	  setSpinBoxInterval(&sbMin, 0, 59);
		//	  setSpinBoxInterval(&sbDay, 1, 31, 1); //TODO: Kaip su men, kur 30 d., kaip su keliamaisiais metais? vasariu?
		//	  setSpinBoxInterval(&sbHour, 0, 23, 12);
		//	  setSpinBoxInterval(&sbMin, 0, 59, 0);
	  setSpinBoxValue(&sbMonth, date.month - 1);
	  setSpinBoxValue(&sbDay, date.day);
	  setSpinBoxValue(&sbHour, time.hours);
	  setSpinBoxValue(&sbMin, time.minutes);
	  createLabel(&dlg, &label, 8, 294, TITLE_HEIGHT + 36, 0, 0);
	  setLabelText(&label, ":", C_WHITE, &FONT_16X26);

	  do{
		  updateDialog(&dlg);
	  }
	  while(getDialogState(&dlg) == DLG_STATE_SHOWN);
}

//-----------

void cbSetOffset(DIALOG *dlg, DLG_MESSAGE *msg){
	uint16_t offsetMiliTesla;
	DLG_SPINBOX *sbSign, *sbTesla, *sbMiliTeslaHundreds, *sbMiliTeslaTens, *sbMiliTeslaUnits;
	if((msg->id == OBJ_TYPE_BUTTON) && (msg->sub_id == 0)){
		sbSign = (DLG_SPINBOX*)(getDialogObjectById(dlg, 8)->data);
		sbTesla = (DLG_SPINBOX*)(getDialogObjectById(dlg, 3)->data);
		sbMiliTeslaHundreds = (DLG_SPINBOX*)(getDialogObjectById(dlg, 4)->data);
		sbMiliTeslaTens = (DLG_SPINBOX*)(getDialogObjectById(dlg, 5)->data);
		sbMiliTeslaUnits = (DLG_SPINBOX*)(getDialogObjectById(dlg, 6)->data);
		offsetMiliTesla = sbTesla->valueCurrent * 1000 + sbMiliTeslaHundreds->valueCurrent * 100 + sbMiliTeslaTens->valueCurrent * 10 + sbMiliTeslaUnits->valueCurrent;
		if(sbSign->valueCurrent == 0){
			offsetMiliTesla = -offsetMiliTesla;
		}
		g_settings.offsetMiliTesla = offsetMiliTesla;
		calibration_setOffset(offsetMiliTesla); //Write new offset to EEPROM
	}
	if(msg->id == OBJ_TYPE_BUTTON)
		setDialogState(dlg, DLG_STATE_CLOSE);
}

void menu_setOffset(){
	static char *signs[] = {"-", "+"};
	DIALOG dlg;
	DLG_OBJECT objects[9];
	DLG_BUTTON butt[2];
	DLG_LABEL labelDot, labelTesla;
	DLG_SPINBOX sbSign, sbTesla, sbMiliTeslaHundreds, sbMiliTeslaTens, sbMiliTeslaUnits;
	uint8_t ids[2] = {0, 1};
	int16_t offsetMiliTesla;
	char *captions[2];
	captions[0] = "OK";
	captions[1] = "Cancel";
	createDialog(&dlg, objects, 9, cbSetOffset);
	setDialogSize(&dlg, 420, 100);
	setDialogTitleText(&dlg, "Set Measurement Offset");
	if(!createButtonsInButtonBox(&dlg, 2, butt, ids, captions))
		return;
	createSpinBox(&dlg, &sbSign, 8, 15, TITLE_HEIGHT + 5);
	setSpinBoxTextItems(&sbSign, signs, 2);
	createSpinBox(&dlg, &sbTesla, 3, 15 + 70, TITLE_HEIGHT + 5);
	createLabel(&dlg, &labelDot, 7, 15 + 70 * 2 - 7, TITLE_HEIGHT + 40, 0, 0);
	setLabelText(&labelDot, ".", C_WHITE, &FONT_16X26);
	createSpinBox(&dlg, &sbMiliTeslaHundreds, 4, 15 + 10 + 70 * 2, TITLE_HEIGHT + 5);
	createSpinBox(&dlg, &sbMiliTeslaTens, 5, 15 + 10 + 70 * 3, TITLE_HEIGHT + 5);
	createSpinBox(&dlg, &sbMiliTeslaUnits, 6, 15 + 10 + 70 * 4, TITLE_HEIGHT + 5);
	createLabel(&dlg, &labelTesla, 8, 15 + 10 + 70 * 5, TITLE_HEIGHT + 40, 0, 0);
	setLabelText(&labelTesla, "T", C_WHITE, &FONT_16X26);
	setSpinBoxInterval(&sbTesla, 0, 5);
	setSpinBoxInterval(&sbMiliTeslaHundreds, 0, 9);
	setSpinBoxInterval(&sbMiliTeslaTens, 0, 9);
	setSpinBoxInterval(&sbMiliTeslaUnits, 0, 9);
	offsetMiliTesla = g_settings.offsetMiliTesla;
	if(offsetMiliTesla < 0){
		offsetMiliTesla = -offsetMiliTesla;
		setSpinBoxValue(&sbSign, 0); //-
	}
	else{
		setSpinBoxValue(&sbSign, 1); //+
	}
	setSpinBoxValue(&sbTesla, offsetMiliTesla / 1000);
	setSpinBoxValue(&sbMiliTeslaHundreds, offsetMiliTesla % 1000 / 100);
	setSpinBoxValue(&sbMiliTeslaTens, offsetMiliTesla % 100 / 10);
	setSpinBoxValue(&sbMiliTeslaUnits, offsetMiliTesla % 10);
	do{
	  updateDialog(&dlg);
	}
	while(getDialogState(&dlg) == DLG_STATE_SHOWN);
}

void cbResetSettings(DIALOG *dlg, DLG_MESSAGE *msg){
	if((msg->id == OBJ_TYPE_BUTTON) && (msg->sub_id == 0)){
		EE_Format();
		EE_SetVirtualSize(8);
		//EE_WriteVariable(0x0100, g_systemCalibrationData.offsetADC);
    EE_WriteVariable(0x0100, 0);
		NVIC_SystemReset();
	}
	if(msg->id == OBJ_TYPE_BUTTON)
		setDialogState(dlg, DLG_STATE_CLOSE);
}

void menu_resetSettings(){
	DIALOG dlg;
	DLG_OBJECT objects[4];
	DLG_BUTTON buttons[2];
	uint8_t ids[2] = {0, 1};
	DLG_LABEL label;
	DLG_STANDARD_ICON standardIcon;
	char *captions[2];

	captions[0] = "Yes";
	captions[1] = "No";
	createDialog(&dlg, objects, 4, cbResetSettings);
	setDialogSize(&dlg, 255, 75);
	setDialogTitleText(&dlg, "Reset Settings");
	if(!createButtonsInButtonBox(&dlg, 2, buttons, ids, captions))
		return;

	createLabel(&dlg, &label, 1, 78, TITLE_HEIGHT + 30, 0, 0);
	setLabelText(&label, "Are you sure?", C_WHITE, &FONT_12X20);
	standardIcon = WARNING;
	createStandardIcon(&dlg, &standardIcon, 2, 5, TITLE_HEIGHT + 5);

	do{
	  updateDialog(&dlg);
	}
	while(getDialogState(&dlg) == DLG_STATE_SHOWN);
}

void cbFormatCard(DIALOG *dlg, DLG_MESSAGE *msg){
	if((msg->id == OBJ_TYPE_BUTTON) && (msg->sub_id == 0)){
		char caption[] = "Format microSD";
		FRESULT result;
		BYTE work[FF_MAX_SS];
		UG_FillScreen(C_BLACK);
		updateBatteryStatus();
		UG_DrawLine(0, 20, 479, 20, C_YELLOW);
		msg_custom("Formatting...");
		result = f_mkfs("", FM_ANY, 0, work, sizeof(work));
		switch(result){
			case FR_OK: msgBox("Done.", caption, 0, NULL); break;
			case FR_DISK_ERR: msgBox("Disk error!", caption, CRITICAL, NULL); break;
			case FR_NOT_READY: msgBox("Card not found!", caption, CRITICAL, NULL); break;
			case FR_NOT_ENOUGH_CORE: msgBox("Out of memory!", caption, CRITICAL, NULL); break;
			default: msgBox("Unknown error occurred!", caption, CRITICAL, NULL);
		}
	}
	if(msg->id == OBJ_TYPE_BUTTON)
		setDialogState(dlg, DLG_STATE_CLOSE);
}

void menu_formatCard(){
	DIALOG dlg;
	DLG_OBJECT objects[4];
	DLG_BUTTON buttons[2];
	uint8_t ids[2] = {0, 1};
	DLG_LABEL label;
	DLG_STANDARD_ICON standardIcon;
	char *captions[2];

	captions[0] = "Yes";
	captions[1] = "No";
	createDialog(&dlg, objects, 4, cbFormatCard);
	setDialogSize(&dlg, 380, 75);
	setDialogTitleText(&dlg, "Format microSD");
	if(!createButtonsInButtonBox(&dlg, 2, buttons, ids, captions))
		return;

	createLabel(&dlg, &label, 1, 78, TITLE_HEIGHT + 7, 300, 64);
	setLabelText(&label, "Do you want to format microSD card? All data will be lost!" , C_WHITE, &FONT_12X20);
	standardIcon = WARNING;
	createStandardIcon(&dlg, &standardIcon, 2, 5, TITLE_HEIGHT + 5);

	do{
	  updateDialog(&dlg);
	}
	while(getDialogState(&dlg) == DLG_STATE_SHOWN);
}

void cbSaveFormat(DIALOG *dlg, DLG_MESSAGE *msg){
	//bool isCSV;
	DLG_SPINBOX *sbFormat;
	if((msg->id == OBJ_TYPE_BUTTON) && (msg->sub_id == 0)){
		sbFormat = (DLG_SPINBOX*)(getDialogObjectById(dlg, 3)->data);
		if(sbFormat->valueCurrent == 0){
			g_settings.saveFormat = TSV;
		}
		else{
			g_settings.saveFormat = CSV;
		}
	}
	if(msg->id == OBJ_TYPE_BUTTON)
		setDialogState(dlg, DLG_STATE_CLOSE);
}

void menu_saveFormat(){
	static char *formats[] = {"TSV", "CSV"};
	bool isCSV;
	DIALOG dlg;
	DLG_OBJECT objects[4];
	DLG_BUTTON butt[2];
	DLG_LABEL labelSaveFormat;
	DLG_SPINBOX sbFormat;
	uint8_t ids[2] = {0, 1};
	char *captions[2];
	captions[0] = "OK";
	captions[1] = "Cancel";
	createDialog(&dlg, objects, 4, cbSaveFormat);
	setDialogSize(&dlg, 280, 100);
	setDialogTitleText(&dlg, "Set Save Format");
	if(!createButtonsInButtonBox(&dlg, 2, butt, ids, captions))
		return;

	createLabel(&dlg, &labelSaveFormat, 2, 15, TITLE_HEIGHT + 40, 0, 0);
	setLabelText(&labelSaveFormat, "Save format: ", C_WHITE, &FONT_12X20);
	createSpinBox(&dlg, &sbFormat, 3, 190, TITLE_HEIGHT + 5);
	setSpinBoxTextItems(&sbFormat, formats, 2);

	isCSV = (g_settings.saveFormat == CSV); //Currently only CSV and TSV are supported
	if(isCSV){
		setSpinBoxValue(&sbFormat, 1);
	}
	else{
		setSpinBoxValue(&sbFormat, 0);
	}
	do{
	  updateDialog(&dlg);
	}
	while(getDialogState(&dlg) == DLG_STATE_SHOWN);
}

//--- automaticaly calibrate adc offset
void menu_calibrateAdc(){
  g_buttonStatus.button = BUTTON_NONE;
  g_buttonStatus.isPressed = false;
  g_status.startStopMeasurementButtonPressed = true;
  g_settings.vDiv = DEFAULT_V_DIV;//default_vDiv;
	g_settings.hDiv = DEFAULT_H_DIV;
  adcCal_init();
  adcCal.enable = true;
  //STMPE_isPressed() == FALSE && !g_status.startStopMeasurementButtonPressed && g_buttonStatus.button != BUTTON_REC
  return;
}

void adcCal_init()
{
  adcCal.enable = false;
  adcCal.sum = 0;
  adcCal.rej_cnt = 0;
  adcCal.cnt = 0;
  adcCal.est_cnt = 0;
  adcCal.try_cnt = 0;
  adcCal.timer_ms = 0;
  adcCal.B_avg_mT = 0;
}
//---

//TODO: Abieju mygtuku int turetu turet vienoda prioriteta! Tad butu gerai perkelt, kad abu gal butu ant EXTI9_5_IRQHandler()

//--- save button 
void EXTI1_IRQHandler(void){
  /*
	if(EXTI->PR & 0x0100){
		EXTI->PR |= 0x0002;
	}
	else if(EXTI->PR & 0x0002){
		g_buttonStatus.button = BUTTON_REC;
		g_buttonStatus.isPressed = ((GPIOB->IDR & 0x0002) == 0);
		delay_us(10000); // Delay for button debounce. There I cannot use delay_ms().
		EXTI->PR |= 0x0002;
	}*/
  
  if(EXTI->PR & 0x0100){
		EXTI->PR |= 0x0002;
	}
	else if(EXTI->PR & 0x0002){
		//g_buttonStatus.button = BUTTON_REC;
		//g_buttonStatus.isPressed = ((GPIOB->IDR & 0x0002) == 0);
		//delay_us(10000); // Delay for button debounce. There I cannot use delay_ms().
		EXTI->PR |= 0x0002;
	}
}

//--- start/stop button
void EXTI9_5_IRQHandler(void){
	/*if(EXTI->PR & 0x0002){
		EXTI->PR |= 0x0100;
	}
	else if(EXTI->PR & 0x0100){
		g_buttonStatus.button = BUTTON_OK;
		if((GPIOA->IDR & 0x0100) == 0){
			delay_us(10000); //Delay for button debounce
			g_buttonStatus.isPressed = TRUE;
			g_status.startStopMeasurementButtonPressed = true;
		}
		else{
			delay_us(10000); //Delay for button debounce
			g_buttonStatus.isPressed = FALSE;
		}

		EXTI->PR |= 0x0102;
	}
	if(EXTI->PR & 0x0080){
		STMPE_PressCheck();
		EXTI->PR |= 0x0080;
	}*/
  
  if(EXTI->PR & 0x0002){
		EXTI->PR |= 0x0100;
	}
	else if(EXTI->PR & 0x0100){
		//g_buttonStatus.button = BUTTON_OK;
		//if((GPIOA->IDR & 0x0100) == 0){
		//	delay_us(10000); //Delay for button debounce
		//	g_buttonStatus.isPressed = TRUE;
		//	g_status.startStopMeasurementButtonPressed = true;
		//}
		//else{
		//	delay_us(10000); //Delay for button debounce
		//	g_buttonStatus.isPressed = FALSE;
		//}

		EXTI->PR |= 0x0102;
	}
	if(EXTI->PR & 0x0080){
		STMPE_PressCheck();
		EXTI->PR |= 0x0080;
	}
}

void EXTI15_10_IRQHandler(void){ //REMOVE!!!
	EXTI->PR |= 0xFFFF;
}


/** System Clock Configuration
*/
void SystemClock_Config(void)
{
	FLASH->ACR |= 1; //Flash latency: 1 wait state (jei tarp 48 ir 72 MHz, tai reik 2 ws)

	RCC->CR |= 16 << 8; //HSICAL (HSI clock calibration)
	RCC->CR |= RCC_CR_HSION;

	RCC->CFGR |= RCC_CFGR_PLLMUL_2; //Multiply by 6 RCC_CFGR_PLLMUL_2
	RCC->CFGR |= RCC_CFGR_PLLSRC; //Src: HSE
	RCC->CR |= RCC_CR_PLLON;
	RCC->CR |= RCC_CR_HSEON;

	RCC->CFGR |= RCC_CFGR_PPRE1_2; //APB1 clock div 2
	RCC->CFGR |= RCC_CFGR_PPRE2_2; //APB2 clock div 2 - gali buti ir didesnis daug arba nebuti net?
	RCC->CFGR |= RCC_CFGR_SW_1; //HSE (via PLL) as SYSCLK
//	RCC_CFGR_SW_1 turi tapti 1

//	RCC->APB1ENR |= RCC_APB1ENR_PWREN; //PWR CLK enable
//	PWR->CR |= PWR_CR_DBP; //Enable access to BDCR (Backup Domain Control Register)
//	RCC->BDCR |= RCC_BDCR_RTCSEL_0; //LSE as RTC CLK
//	RCC->BDCR |= RCC_BDCR_LSEON; //LSE ON
//	RCC->BDCR |= RCC_BDCR_RTCEN; //RTC enable

//	RCC->CFGR |= RCC_CFGR_USBPRE; //Div 1

	SysTick->CTRL |= 4; //SYSTICK_CLKSOURCE_HCLK

	/* SysTick_IRQn interrupt configuration */
	NVIC_SetPriority(SysTick_IRQn, 0);

	SysTick_Config(48000); //Has to be 1000 times less than HCLK (HCLK sets clock to 48MHz) //Turi buti 1000 kartu mazesnis uz HCLK (48MHz dabar)

	SysTick->CTRL |= 4; //SYSTICK_CLKSOURCE_HCLK

	/* SysTick_IRQn interrupt configuration */
	NVIC_SetPriority(SysTick_IRQn, 0);
}

/* ADC1 init function */
//Function has been written according traineeship's Monika's work
void InternalADC1_Init(void){
	RCC->CFGR2 |= RCC_CFGR2_ADCPRE12_DIV256;

	// Configure the ADC clock
	RCC->AHBENR |= RCC_AHBENR_ADC12EN; //Enable ADC1 clock

	/* Calibration procedure */
	ADC1->CR &= ~ADC_CR_ADVREGEN;
	ADC1->CR |= ADC_CR_ADVREGEN_0; // 01: ADC Voltage regulator enabled
	delay_ms(1); // Insert delay equal to 10 μs
	ADC1->CR &= ~ADC_CR_ADCALDIF; // calibration in Single-ended inputs Mode.
	ADC1->CR |= ADC_CR_ADCAL; // Start ADC calibration
	// Read at 1 means that a calibration in progress.
	while (ADC1->CR & ADC_CR_ADCAL); // wait until calibration done
	//calibration_value = ADC1->CALFACT; // Get Calibration Value ADC1

	// ADC configuration
	ADC1->CFGR &= ~ADC_CFGR_CONT; //<-- single
	// ADC_ContinuousConvMode_Enable
	ADC1->CFGR |= ADC_CFGR_RES_0; // 10-bit data resolution
	ADC1->CFGR &= ~ADC_CFGR_ALIGN; // Right data alignment
	/* ADC1 regular channel1 configuration */
	ADC1->SQR1 |= ADC_SQR1_SQ1_0;
	ADC1->SQR1 &= ~ADC_SQR1_L; // ADC regular channel sequence length = 0 => 1 conversion/sequence
	ADC1->SMPR1 |= ADC_SMPR1_SMP7_2 | ADC_SMPR1_SMP7_1 | ADC_SMPR1_SMP7_0; // 0x07 => sampling time 601.5 ADC clock cycles //
	ADC1->CR |= ADC_CR_ADEN;

	// Enable ADC1
	while(!ADC1->ISR & ADC_ISR_ADRD); // wait for ADRDY
	ADC1->CR |= ADC_CR_ADSTART;
}

void I2C_Init(){
    RCC -> AHBENR |= RCC_AHBENR_GPIOBEN;
    GPIOB -> OSPEEDR &= ~GPIO_OSPEEDER_OSPEEDR6 | ~GPIO_OSPEEDER_OSPEEDR7;
    GPIOB -> MODER |= GPIO_MODER_MODER6_1 | GPIO_MODER_MODER7_1;
    GPIOB -> OTYPER |= GPIO_OTYPER_OT_6 | GPIO_OTYPER_OT_7;
    GPIOB -> AFR[0] |= (4 << 24) | (4 << 28);
    RCC->APB1ENR |= RCC_APB1ENR_I2C1EN ;

    RCC -> AHBENR |= RCC_AHBENR_GPIOAEN;
	GPIOA -> OSPEEDR &= ~GPIO_OSPEEDER_OSPEEDR9 | ~GPIO_OSPEEDER_OSPEEDR10;
	GPIOA -> MODER |= GPIO_MODER_MODER0;  // Configure ADC1 Channel1 as analog input
	GPIOA -> MODER |= GPIO_MODER_MODER9_1 | GPIO_MODER_MODER10_1;
	GPIOA -> OTYPER |= GPIO_OTYPER_OT_9 | GPIO_OTYPER_OT_10;
	GPIOA -> AFR[1] |= (4 << 4) | (4 << 8);
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR9_0 | GPIO_PUPDR_PUPDR10_0;
	RCC->APB1ENR |= RCC_APB1ENR_I2C2EN ;

    I2C_setMode(I2C1, STANDARD_100);
    I2C_setMode(I2C2, STANDARD_100);
}

void GPIO_Init(void){
	/* GPIO Ports Clock Enable */
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
	RCC->AHBENR |= RCC_AHBENR_GPIODEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOEEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOFEN;

	//---? SYSCFG del mygtuku int, tad gal perkelt i kita funkcija
	//RCC->APB2RSTR |= RCC_APB2RSTR_SYSCFGRST;
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;

	//Pagal default GPIOE jau yra input, floating. Tad nereik konfiguruoti.

	/* LCD COMMUNICATION GPIO CONFIGURATION */

	//GPIOC (in short GPIOC->MODER = 0x55555155UL)
	TFT_CONTROL_GPIO_Port->MODER = 1 << (TFT_RST__Pin * 2);
	TFT_CONTROL_GPIO_Port->MODER |= 1 << (TFT_CS__Pin * 2);
	TFT_CONTROL_GPIO_Port->MODER |= 1 << (TFT_D_C__Pin * 2);
	TFT_CONTROL_GPIO_Port->MODER |= 1 << (TFT_RD__Pin * 2);
	TFT_CONTROL_GPIO_Port->MODER |= 1 << (TFT_WR__Pin * 2);
	//TFT_CONTROL_GPIO_Port->MODER &= ~(1 << (TFT_TE_Pin * 2)); - default

	//Medium speed (10 MHz)
	TFT_CONTROL_GPIO_Port->OSPEEDR |= 1 << (TFT_RST__Pin * 2);
	TFT_CONTROL_GPIO_Port->OSPEEDR |= 1 << (TFT_CS__Pin * 2);
	TFT_CONTROL_GPIO_Port->OSPEEDR |= 1 << (TFT_D_C__Pin * 2);
	TFT_CONTROL_GPIO_Port->OSPEEDR |= 1 << (TFT_RD__Pin * 2);
	TFT_CONTROL_GPIO_Port->OSPEEDR |= 1 << (TFT_WR__Pin * 2);

	//PORTD
	TFT_DATA_GPIO_Port->MODER = 0x55555555UL; //All outputs
	TFT_DATA_GPIO_Port->OSPEEDR = 0x55555555UL; //All medium speed (10 MHz)



	// Not used:
	//  LED0_GPIO_Port->MODER |= 1 << (LED0_Pin * 2);
	//  LED1_GPIO_Port->MODER |= 1 << (LED1_Pin * 2);
	//  BUZZER_GPIO_Port->MODER |= 1 << (BUZZER_Pin * 2);
	//  POWER_ON_OFF_GPIO_Port->MODER |= 1 << (POWER_ON_OFF_Pin * 2);

	//SD_EXIST_GPIO_Port->MODER = 1 << (SD_EXIST_Pin * 2);

	/* BUTTONS GPIO CONFIGURATION */

	BUTTON_REC_GPIO_Port->PUPDR = 1 << (BUTTON_REC_Pin * 2);
	BUTTON_RESERVED_GPIO_Port->PUPDR = 1 <<(BUTTON_RESERVED_Pin * 2);

	//EXT_TRIGGER_GPIO_Port->MODER &= ~(1 << (EXT_TRIGGER_Pin * 2)); - default
/////////

	SYSCFG->EXTICR[0] |= SYSCFG_EXTICR1_EXTI1_PB;

	EXTI->IMR |= 0x182; //Interrupt mask register
	EXTI->FTSR |= 0x182; //Falling trigger select register (enable)
	EXTI->RTSR |= 0x182; //Rising trigger select register (enable)



	NVIC_EnableIRQ(EXTI1_IRQn);
	NVIC_EnableIRQ(EXTI9_5_IRQn); //REC button and Touch Screen

	/*Configure GPIO pin : TP_INT_Pin */
	//  GPIO_InitStruct.Pin = TP_INT_Pin;
	//  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	//  GPIO_InitStruct.Pull = GPIO_NOPULL;
	//  HAL_GPIO_Init(TP_INT_GPIO_Port, &GPIO_InitStruct);
}


static void updateBatteryStatus(){
  static uint8_t batteryStatus_n1 = 0;
	static uint8_t previousBatteryStatus = 255;
	uint8_t batteryStatus = getBatteryStatus();
	char batteryStatusStr[4];
	bool isCharging;
  
  //--- there is no point update if nothing changed
  if(batteryStatus_n1 == batteryStatus)
    return;
  batteryStatus_n1 = batteryStatus;

	if(previousBatteryStatus < batteryStatus){ //To forbid battery level rising due to measurement error when battery is discharging
		batteryStatus = previousBatteryStatus;
	}
	else{
		previousBatteryStatus = batteryStatus;
	}

	isCharging = false;
	if(GPIOC->IDR & 0x2000){
		isCharging = true;
	}

	if(batteryStatus == 0 && !isCharging){
		//Battery fully discharged. Power off everything.
		ExtADC_stop();
		EXTADC_POWERDOWN();
		//SSD_WriteCommand(SSD1963_SOFT_RESET);
		//SSD_WriteCommand(SSD1963_SET_DISPLAY_OFF);
		SSD_WriteCommand(SSD1963_ENTER_SLEEP_MODE);
		__disable_irq();
		NVIC_SystemReset();
	}

	UG_SetForecolor(C_YELLOW);
	UG_SetBackcolor(C_BLACK);
	UG_FontSelect(&FONT_10X16);

	if(batteryStatus < 10){
		batteryStatusStr[0] = batteryStatus + '0';
		batteryStatusStr[1] = '%';
		batteryStatusStr[2] = '\0';
	}
	else if(batteryStatus < 100) {
		batteryStatusStr[0] = batteryStatus / 10 + '0';
		batteryStatusStr[1] = batteryStatus % 10 + '0';
		batteryStatusStr[2] = '%';
		batteryStatusStr[3] = '\0';
	}


	UG_SetForecolor(C_YELLOW);
	UG_SetBackcolor(C_BLACK);
#define BATTERY_STATUS_X	370 /* TODO: Cia turi buti 375 bagal logika,o is apacios po penkis atimt */
	UG_FillFrame(BATTERY_STATUS_X + 5, 0, 480, 18, C_BLACK);

	//Battery status
	//    _________________
	//  _|                 |
	// [_                  |
	//   |_________________|

	UG_DrawLine(BATTERY_STATUS_X+5, 6, BATTERY_STATUS_X+5, 12, C_YELLOW);
	UG_DrawLine(BATTERY_STATUS_X+5, 6, BATTERY_STATUS_X + 10, 6, C_YELLOW);
	UG_DrawLine(BATTERY_STATUS_X+5, 12, BATTERY_STATUS_X + 10, 12, C_YELLOW);
	UG_DrawLine(BATTERY_STATUS_X + 10, 3, BATTERY_STATUS_X + 10, 6, C_YELLOW);
	UG_DrawLine(BATTERY_STATUS_X + 10, 12, BATTERY_STATUS_X + 10, 15, C_YELLOW);
	UG_DrawLine(BATTERY_STATUS_X + 10, 3, BATTERY_STATUS_X + 60, 3, C_YELLOW);
	UG_DrawLine(BATTERY_STATUS_X + 10, 15, BATTERY_STATUS_X + 60, 15, C_YELLOW);
	UG_DrawLine(BATTERY_STATUS_X + 60, 3, BATTERY_STATUS_X + 60, 15, C_YELLOW);
	if(isCharging)
		UG_PutString(BATTERY_STATUS_X + 65, 3, "Chg");
	else if(batteryStatus < 100)
		UG_PutString(BATTERY_STATUS_X + 65, 3, batteryStatusStr);
	else
		UG_PutString(BATTERY_STATUS_X + 65, 3, "100%");

	if(!isCharging)
		UG_FillFrame(BATTERY_STATUS_X + 61 - batteryStatus / 2, 4, BATTERY_STATUS_X + 59, 14, C_GREEN);

	if(isCharging){
		UG_DrawLine(BATTERY_STATUS_X + 20, 7, BATTERY_STATUS_X + 35, 11, C_YELLOW);
		UG_DrawLine(BATTERY_STATUS_X + 30, 7, BATTERY_STATUS_X + 50, 11, C_YELLOW);
		UG_DrawLine(BATTERY_STATUS_X + 30, 7, BATTERY_STATUS_X + 35, 11, C_YELLOW);
	}
}

static uint8_t getBatteryStatus(){
	uint16_t ADC_ConvertedValue = 0;
	uint8_t batteryStatus;
	uint8_t try;
	for(try = 0; try < 16; try++){
		ADC1->CR |= ADC_CR_ADSTART;
		while(!(ADC1->ISR & ADC_ISR_EOC)); // Test EOC flag
		ADC_ConvertedValue += ADC1->DR; // Get ADC1 converted data
	}
	ADC_ConvertedValue = ADC_ConvertedValue / 16;
	if(ADC_ConvertedValue > 837){ //Vout > 2.7 (Vbat > 3.7V)
		batteryStatus = 60 + (ADC_ConvertedValue - 837) * 40 / (930 - 837);
	}
	else if(837 >= ADC_ConvertedValue && ADC_ConvertedValue > 806){ //2.7V >= Vout > 2.6 (3.7V <= Vbat < 3.6V)
		batteryStatus = 10 + (ADC_ConvertedValue - 806) * 50 / (837 - 806);
	}
	else{ // Vout <= 2.6V (Vbat <= 3.5V)
		batteryStatus = (ADC_ConvertedValue - 713) * 10 / (806 - 713);
	}
	return batteryStatus;
}

static void mainWindow(){
  
	//const uint16_t menuWidth = 106;
	//TOUCH_AREA startStopButton, saveButton, touchTriggerButton, extTriggerButton, menuButton;
	const uint16_t startStopButton_ys = STATUS_BAR_HEIGHT;
	const uint16_t saveButton_ys = startStopButton_ys + RIGHT_MENU_ITEM_HEIGHT + 1;
	const uint16_t modeButton_ys = saveButton_ys + RIGHT_MENU_ITEM_HEIGHT + 1;
	const uint16_t setPulseWidthButton_ys = modeButton_ys + RIGHT_MENU_ITEM_HEIGHT + 1;
	const uint16_t otherButton_ys = setPulseWidthButton_ys + RIGHT_MENU_ITEM_HEIGHT + 1;
	const uint16_t menu_xs = DISPLAY_WIDTH - RIGHT_MENU_WIDTH;
	const uint16_t menuHCenter = menu_xs + RIGHT_MENU_WIDTH / 2;

	//UG_FillFrame(14, 20, 372, 258, C_BLACK);
/*
This:
	UG_FillFrame(0, 262, 10, DISPLAY_HEIGHT - 1, C_BLUE);
	UG_FillFrame(0, 20, 10, 260, C_GRAY);
	UG_FillFrame(12, 262, 372, DISPLAY_HEIGHT - 1, C_GRAY);
has been changed to that:
*/
	//UG_FillFrame(0, 20, 10, DISPLAY_HEIGHT - 1, C_GRAY);
	UG_FillFrame(0, 262, 372, DISPLAY_HEIGHT - 1, C_GRAY);

	UG_FillFrame(DISPLAY_WIDTH - RIGHT_MENU_WIDTH, 20, DISPLAY_WIDTH - 1, 271, C_BLUE); //Right menu

/*
This:
	UG_DrawLine(11, 20, 11, DISPLAY_HEIGHT - 1, C_YELLOW);
	UG_DrawLine(0, 261, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 2, 261, C_YELLOW);
	UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1, 20, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1, DISPLAY_HEIGHT - 1, C_YELLOW);
has been changed to that:
*/
	UG_DrawLine(11, 20, 11, 261, C_GRAPGH_CNTR);
	UG_DrawLine(11, 261, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 2, 261, C_GRAPGH_CNTR);
	UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1, 20, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1, DISPLAY_HEIGHT - 1, C_GRAPGH_CNTR);

	UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH, saveButton_ys - 1, DISPLAY_WIDTH - 1, saveButton_ys - 1, C_YELLOW); //After "Start"
	UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH, setPulseWidthButton_ys - 1, DISPLAY_WIDTH - 1, setPulseWidthButton_ys - 1, C_YELLOW); //After "Save"
	UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH, modeButton_ys - 1, DISPLAY_WIDTH - 1, modeButton_ys - 1, C_YELLOW); //After "Set scale length"
	UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH, otherButton_ys - 1, DISPLAY_WIDTH - 1, otherButton_ys - 1, C_YELLOW); //After "Trigger"

	UG_SetBackcolor(C_BLUE);
	UG_SetForecolor(C_YELLOW);
	UG_FontSelect(&FONT_8X14);
	if(g_status.isMeasurementStarted){
		if(g_status.mode == EXTERNAL || g_status.mode == SINGLE_SEQUENCE){
			UG_PutString(menuHCenter - sizeof("Waiting...") * 8 / 2, startStopButton_ys + RIGHT_MENU_ITEM_HEIGHT / 2 - 7, "Waiting...");
			if(g_status.mode == SINGLE_SEQUENCE){
				UG_PutString(menuHCenter - sizeof("Set trigger") * 8 / 2, saveButton_ys + 5, "Set trigger");
				UG_PutString(menuHCenter - sizeof("level") * 8 / 2, saveButton_ys + 5 + 14, "level");
			}
		}
		else{
			UG_PutString(menuHCenter - sizeof("Stop") * 8 / 2, startStopButton_ys + 35, "Stop");
			UG_FillFrame(menuHCenter - 15, startStopButton_ys + 5, menuHCenter + 15, startStopButton_ys + 31, C_GREEN);
		}
	}
	else{
		UG_PutString(menuHCenter - sizeof("Start") * 8 / 2, startStopButton_ys + 35, "Start");
		fillTriangle(menuHCenter - 15, startStopButton_ys + 5, menuHCenter - 15, startStopButton_ys + 31, menuHCenter + 15, startStopButton_ys + 18, C_GREEN);

		UG_PutString(menuHCenter - sizeof("Save") * 8 / 2, saveButton_ys + 35, "Save");
		UG_FillCircle(menuHCenter, saveButton_ys + 5 + 13, 13, C_RED);
	}

	UG_PutString(menuHCenter - sizeof("Mode") * 8 / 2, modeButton_ys + RIGHT_MENU_ITEM_HEIGHT / 2 - 14 - 3, "Mode");

	if(g_status.mode == EXTERNAL || g_status.mode == SINGLE_SEQUENCE || g_status.mode == FORCE){
		UG_PutString(menuHCenter - sizeof("Scale") * 8 / 2, setPulseWidthButton_ys + RIGHT_MENU_ITEM_HEIGHT / 2 - 14 - 3, "Scale"); //setPulseWidthButton_ys nuo siol reiks scale, nustatyta per si meniu :) . Cia buvo "Set scale length" anksciau, kas yra impulso ilgiui lygus
	}

	UG_PutString(menuHCenter - sizeof("Other") * 8 / 2, otherButton_ys + RIGHT_MENU_ITEM_HEIGHT / 2 - 14 - 3, "Other");
	UG_DrawLine(menu_xs + 25, otherButton_ys + 25, menu_xs + 35, otherButton_ys + 25, C_YELLOW);
	UG_DrawLine(menu_xs + 25, otherButton_ys + 30, menu_xs + 35, otherButton_ys + 30, C_YELLOW);
	UG_DrawLine(menu_xs + 25, otherButton_ys + 35, menu_xs + 35, otherButton_ys + 35, C_YELLOW);

	UG_FontSelect(&FONT_8X14);
	UG_SetForecolor(C_GREEN);

	char str[12];
	if(g_status.mode == EXTERNAL || g_status.mode == SINGLE_SEQUENCE || (g_status.mode == FORCE && !g_status.isMeasurementStarted)){
		uint16_t value;
		char units[3];

		if(g_settings.pulseLength_us / 12 / 1000000UL){
			value = g_settings.pulseLength_us / 12 / 1000000UL;
			units[0] = 's';
			units[1] = '\0';
		}
		else if(g_settings.pulseLength_us / 12 / 1000){
			value = g_settings.pulseLength_us / 12 / 1000;
			units[0] = 'm';
			units[1] = 's';
			units[2] = '\0';
		}
		else{
			value = g_settings.pulseLength_us / 12;
			units[0] = '\xE6';
			units[1] = 's';
			units[2] = '\0';
		}
		sprintf(str, "%u%s/div", value, units);
		UG_PutString(menu_xs + RIGHT_MENU_WIDTH / 2 - strlen(str) * 8 / 2, (setPulseWidthButton_ys * 2 + RIGHT_MENU_ITEM_HEIGHT) / 2, str);
	}

	if(g_status.mode == EXTERNAL){
		strcpy(str, "External");
	}
	else if(g_status.mode == FORCE){
		strcpy(str, "Force");
	}
	else if(g_status.mode == CONTINUOUS){
		strcpy(str, "Continuous");
	}
	else{ //Internal
		sprintf(str, "Int %lumT", g_settings.triggerLevelMiliTesla);
	}
	UG_PutString(menu_xs + RIGHT_MENU_WIDTH / 2 - strlen(str) * 8 / 2, (modeButton_ys * 2 + RIGHT_MENU_ITEM_HEIGHT) / 2, str);
	refreshPositionArrows();
}

void refreshPositionArrows(){
  
	int32_t triggerLevelMiliTesla = g_settings.triggerLevelMiliTesla;
	uint16_t triggerHorizontalLine = GRAPH_HEIGHT - (g_settings.triggerLevelMiliTesla + g_status.offsetMiliTeslaTmp) * GRID_DENSITY / g_settings.vDiv + STATUS_BAR_HEIGHT;

	UG_FillFrame(0, 19, 10, DISPLAY_HEIGHT - 1, GRAPH_BACK_C);
	UG_FillFrame(0, DISPLAY_HEIGHT - 10, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1, DISPLAY_HEIGHT - 1, GRAPH_BACK_C);

	//Zero position:
  int offset_y = -GRAPH_ZERO_POS - 1;
  int zero_pos_c = GRAPH_ZERO_POS_C;
  if(adcCal.enable)
    zero_pos_c = RGB888(C_USR_LINE_ADC_CAL_16b);
	if (g_status.offsetMiliTeslaTmp == 0){
		UG_DrawLine(0, 260+offset_y, 9, 260+offset_y, zero_pos_c);
		UG_DrawLine(0, 261+offset_y, 10, 261+offset_y, zero_pos_c);
		UG_DrawLine(0, 262+offset_y, 9, 262+offset_y, zero_pos_c);
		UG_DrawLine(4, 256+offset_y, 10, 260+offset_y, zero_pos_c);
		UG_DrawLine(4, 257+offset_y, 10, 261+offset_y, zero_pos_c);
		UG_DrawLine(4, 265+offset_y, 10, 261+offset_y, zero_pos_c);
		UG_DrawLine(4, 266+offset_y, 10, 262+offset_y, zero_pos_c);
	}
	else{
		UG_DrawLine(0, 260+offset_y, 10, 260+offset_y, zero_pos_c);
		UG_DrawLine(0, 261+offset_y, 10, 261+offset_y, zero_pos_c);
		UG_DrawLine(0, 262+offset_y, 10, 262+offset_y, zero_pos_c);
		UG_DrawLine(4, 250+offset_y, 4, 258+offset_y, zero_pos_c);
		UG_DrawLine(5, 250+offset_y, 5, 259+offset_y, zero_pos_c);
		UG_DrawLine(6, 250+offset_y, 6, 258+offset_y, zero_pos_c);
		UG_DrawLine(1, 255+offset_y, 5, 259+offset_y, zero_pos_c);
		UG_DrawLine(1, 256+offset_y, 5, 260+offset_y, zero_pos_c);
		UG_DrawLine(9, 255+offset_y, 5, 259+offset_y, zero_pos_c);
		UG_DrawLine(9, 256+offset_y, 5, 260+offset_y, zero_pos_c);
	}

	//Horizontal trigger arrow:

	if(g_status.mode == SINGLE_SEQUENCE){
		if (g_status.offsetMiliTeslaTmp + triggerLevelMiliTesla < 0){
			UG_DrawLine(1, 260+offset_y, 9, 260+offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(1, 261+offset_y, 9, 261+offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(1, 262+offset_y, 9, 262+offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(4, 251+offset_y, 4, 258+offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(5, 251+offset_y, 5, 259+offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(6, 251+offset_y, 6, 258+offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(1, 255+offset_y, 5, 259+offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(2, 255+offset_y, 6, 259+offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(8, 255+offset_y, 6, 259+offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(9, 255+offset_y, 5, 259+offset_y, GRAPH_TRIGGER_C);
		}
		else if (g_status.offsetMiliTeslaTmp + triggerLevelMiliTesla >= 0 &&
             g_status.offsetMiliTeslaTmp + triggerLevelMiliTesla <= g_settings.vDiv * 8)
    {
      triggerHorizontalLine = triggerHorizontalLine + offset_y; 
			UG_DrawLine(1, triggerHorizontalLine - 1, 8, triggerHorizontalLine - 1, GRAPH_TRIGGER_C);
			UG_DrawLine(1, triggerHorizontalLine, 9, triggerHorizontalLine, GRAPH_TRIGGER_C);
			UG_DrawLine(1, triggerHorizontalLine + 1, 8, triggerHorizontalLine + 1, GRAPH_TRIGGER_C);
			UG_DrawLine(3, triggerHorizontalLine - 4, 9, triggerHorizontalLine, GRAPH_TRIGGER_C);
			UG_DrawLine(3, triggerHorizontalLine - 4 - 1, 9, triggerHorizontalLine - 1, GRAPH_TRIGGER_C);
			UG_DrawLine(3, triggerHorizontalLine + 4, 9, triggerHorizontalLine, GRAPH_TRIGGER_C);
			UG_DrawLine(3, triggerHorizontalLine + 4 + 1, 9, triggerHorizontalLine + 1, GRAPH_TRIGGER_C);
		}
		else{ //if (g_status.offsetMiliTeslaTmp + g_settings.triggerLevelMiliTesla >= 0){
			UG_DrawLine(1, 20+ offset_y, 9, 20+ offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(1, 21+ offset_y, 9, 21+ offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(1, 22+ offset_y, 9, 22+ offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(4, 23+ offset_y, 4, 32+ offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(5, 22+ offset_y, 5, 32+ offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(6, 23+ offset_y, 6, 32+ offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(5, 21+ offset_y, 9, 26+ offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(5, 22+ offset_y, 9, 27+ offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(5, 22+ offset_y, 1, 27+ offset_y, GRAPH_TRIGGER_C);
			UG_DrawLine(5, 21+ offset_y, 1, 26+ offset_y, GRAPH_TRIGGER_C);
		}
	}

	if((g_status.mode == SINGLE_SEQUENCE || g_status.mode == EXTERNAL) && g_status.isDataReceived){

		//Vertical trigger arrow:
		if(g_status.verticalTriggerLinePositionPx <= 1){
			UG_DrawLine(12, DISPLAY_HEIGHT - 5 - 1, 12 + 10, DISPLAY_HEIGHT - 5 - 1, GRAPH_TRIGGER_C);
			UG_DrawLine(12, DISPLAY_HEIGHT - 5, 12 + 10, DISPLAY_HEIGHT - 5, GRAPH_TRIGGER_C);
			UG_DrawLine(12, DISPLAY_HEIGHT - 5 + 1, 12 + 10, DISPLAY_HEIGHT - 5 + 1, GRAPH_TRIGGER_C);

			UG_DrawLine(17, DISPLAY_HEIGHT - 8 - 1, 12, DISPLAY_HEIGHT - 5 - 1, GRAPH_TRIGGER_C);
			UG_DrawLine(17, DISPLAY_HEIGHT - 8, 12, DISPLAY_HEIGHT - 5, GRAPH_TRIGGER_C);

			UG_DrawLine(17, DISPLAY_HEIGHT - 2 + 1, 12, DISPLAY_HEIGHT - 5 + 1, GRAPH_TRIGGER_C);
			UG_DrawLine(17, DISPLAY_HEIGHT - 2, 12, DISPLAY_HEIGHT - 5, GRAPH_TRIGGER_C);

			UG_DrawLine(10, DISPLAY_HEIGHT - 10, 10, DISPLAY_HEIGHT - 1, GRAPH_TRIGGER_C);
			UG_DrawLine(11, DISPLAY_HEIGHT - 10, 11, DISPLAY_HEIGHT - 1, GRAPH_TRIGGER_C);
			UG_DrawLine(12, DISPLAY_HEIGHT - 10, 12, DISPLAY_HEIGHT - 1, GRAPH_TRIGGER_C);
		}
		else if(g_status.verticalTriggerLinePositionPx > 1 && g_status.verticalTriggerLinePositionPx < GRAPH_WIDTH){
			UG_DrawLine((12 + g_status.verticalTriggerLinePositionPx-1), DISPLAY_HEIGHT - 10+1, (12 + g_status.verticalTriggerLinePositionPx-1), DISPLAY_HEIGHT - 1, GRAPH_TRIGGER_C);
			UG_DrawLine((12 + g_status.verticalTriggerLinePositionPx), DISPLAY_HEIGHT - 10, (12 + g_status.verticalTriggerLinePositionPx), DISPLAY_HEIGHT - 1, GRAPH_TRIGGER_C);
			UG_DrawLine((12 + g_status.verticalTriggerLinePositionPx+1), DISPLAY_HEIGHT - 10+1, (12 + g_status.verticalTriggerLinePositionPx+1), DISPLAY_HEIGHT - 1, GRAPH_TRIGGER_C);
			UG_DrawLine((12 + g_status.verticalTriggerLinePositionPx), DISPLAY_HEIGHT - 10-1, (12 + g_status.verticalTriggerLinePositionPx) + 5, DISPLAY_HEIGHT - 5-1, GRAPH_TRIGGER_C);
			UG_DrawLine((12 + g_status.verticalTriggerLinePositionPx), DISPLAY_HEIGHT - 10, (12 + g_status.verticalTriggerLinePositionPx) + 5, DISPLAY_HEIGHT - 5, GRAPH_TRIGGER_C);
			UG_DrawLine((12 + g_status.verticalTriggerLinePositionPx), DISPLAY_HEIGHT - 10-1, (12 + g_status.verticalTriggerLinePositionPx) - 5, DISPLAY_HEIGHT - 5-1, GRAPH_TRIGGER_C);
			UG_DrawLine((12 + g_status.verticalTriggerLinePositionPx), DISPLAY_HEIGHT - 10, (12 + g_status.verticalTriggerLinePositionPx) - 5, DISPLAY_HEIGHT - 5, GRAPH_TRIGGER_C);
		}
		else{ //g_status.verticalTriggerLinePositionPx >= GRAPH_WIDTH
			UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 - 10 - 1, DISPLAY_HEIGHT - 5-1, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 - 1, DISPLAY_HEIGHT - 5-1, GRAPH_TRIGGER_C);
			UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 - 10 - 1, DISPLAY_HEIGHT - 5, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 - 1, DISPLAY_HEIGHT - 5, GRAPH_TRIGGER_C);
			UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 - 10 - 1, DISPLAY_HEIGHT - 5+1, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 - 1, DISPLAY_HEIGHT - 5+1, GRAPH_TRIGGER_C);
			UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 - 5 - 1, DISPLAY_HEIGHT - 8-1, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 - 1, DISPLAY_HEIGHT - 5-1, GRAPH_TRIGGER_C);
			UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 - 5 - 1, DISPLAY_HEIGHT - 8, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 - 1, DISPLAY_HEIGHT - 5-1, GRAPH_TRIGGER_C);
			UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 - 5 - 1, DISPLAY_HEIGHT - 2, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 - 1, DISPLAY_HEIGHT - 5, GRAPH_TRIGGER_C);
			UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 - 5 - 1, DISPLAY_HEIGHT - 2+1, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1 - 1, DISPLAY_HEIGHT - 5+1, GRAPH_TRIGGER_C);
			UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1-1, DISPLAY_HEIGHT - 10, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1-1, DISPLAY_HEIGHT - 1, GRAPH_TRIGGER_C);
			UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1, DISPLAY_HEIGHT - 10, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1, DISPLAY_HEIGHT - 1, GRAPH_TRIGGER_C);
			UG_DrawLine(DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1+1, DISPLAY_HEIGHT - 10, DISPLAY_WIDTH - RIGHT_MENU_WIDTH - 1+1, DISPLAY_HEIGHT - 1, GRAPH_TRIGGER_C);
		}
	}
}

static enum zoomButtonPressed zoomButtons(){
	const uint16_t xStart = GRAPH_START_X;
	const uint16_t yStart = GRAPH_START_Y;
	const uint16_t xEnd = xStart + GRAPH_WIDTH;
	const uint16_t yEnd = yStart + GRAPH_HEIGHT;
	enum zoomButtonPressed pressed = NONE;
	uint16_t ts_x, ts_y;
	ts_x = STMPE_getX0();
	ts_y = STMPE_getY0();

	if(STMPE_isPressed()){
		if(ts_x > xStart + 10 && ts_x < xStart + 50){
			if(ts_y > yStart + 10 && ts_y < yStart + 50)
				pressed = VZoomIn;
			else if(ts_y > yStart + 10 + 60 && ts_y < yStart + 50 + 60)
				pressed = VZoomOut;
		}
		else if(ts_y < yEnd - 10 && ts_y > yEnd - 50){
			if(ts_x < xEnd - 10 && ts_x > xEnd - 50)
				pressed = HZoomOut;
			else if(ts_x < xEnd - 10 - 60 && ts_x > xEnd - 50 - 60)
				pressed = HZoomIn;
		}
	}

	if(pressed == VZoomIn){
		//Vertical -
		UG_FillFrame(xStart + 10, yStart + 10, xStart + 50, yStart + 50, 0x505050);
		UG_DrawLine(xStart + 9, yStart + 9, xStart + 51, yStart + 9, 0x404040);
		UG_DrawLine(xStart + 9, yStart + 9, xStart + 9, yStart + 51, 0x404040);
		UG_DrawLine(xStart + 51, yStart + 9, xStart + 51, yStart + 51, 0xA0A0A0);
		UG_DrawLine(xStart + 9, yStart + 51, xStart + 51, yStart + 51, 0xA0A0A0);
		UG_DrawLine(xStart + 15, yStart + 30, xStart + 45, yStart + 30, C_BLACK);
		UG_DrawLine(xStart + 15, yStart + 29, xStart + 45, yStart + 29, C_BLACK);
		UG_DrawLine(xStart + 15, yStart + 31, xStart + 45, yStart + 31, C_BLACK);
		UG_DrawLine(xStart + 30, yStart + 15, xStart + 30, yStart + 45, C_BLACK);
		UG_DrawLine(xStart + 29, yStart + 15, xStart + 29, yStart + 45, C_BLACK);
		UG_DrawLine(xStart + 31, yStart + 15, xStart + 31, yStart + 45, C_BLACK);
	}
	else if(pressed == VZoomOut){
		//Vertical +
		UG_FillFrame(xStart + 10, yStart + 10 + 60, xStart + 50, yStart + 50 + 60, 0x505050);
		UG_DrawLine(xStart + 9, yStart + 9 + 60, xStart + 51, yStart + 9 + 60, 0x404040);
		UG_DrawLine(xStart + 9, yStart + 9 + 60, xStart + 9, yStart + 51 + 60, 0x404040);
		UG_DrawLine(xStart + 51, yStart + 9 + 60, xStart + 51, yStart + 51 + 60, 0xA0A0A0);
		UG_DrawLine(xStart + 9, yStart + 51 + 60, xStart + 51, yStart + 51 + 60, 0xA0A0A0);
		UG_DrawLine(xStart + 15, yStart + 30 + 60, xStart + 45, yStart + 30 + 60, C_BLACK);
		UG_DrawLine(xStart + 15, yStart + 29 + 60, xStart + 45, yStart + 29 + 60, C_BLACK);
		UG_DrawLine(xStart + 15, yStart + 31 + 60, xStart + 45, yStart + 31 + 60, C_BLACK);
	}
	else if(pressed == HZoomIn){
		//Horizontal -
		UG_FillFrame(xEnd - 10 - 60, yEnd - 10, xEnd - 50 - 60, yEnd - 50, 0x505050);
		UG_DrawLine(xEnd - 9 - 60, yEnd - 9, xEnd - 51 - 60, yEnd - 9, 0xA0A0A0);
		UG_DrawLine(xEnd - 9 - 60, yEnd - 9, xEnd - 9 - 60, yEnd - 51, 0xA0A0A0);
		UG_DrawLine(xEnd - 51 - 60, yEnd - 9, xEnd - 51 - 60, yEnd - 51, 0x404040);
		UG_DrawLine(xEnd - 9 - 60, yEnd - 51, xEnd - 51 - 60, yEnd - 51, 0x404040);
		UG_DrawLine(xEnd - 15 - 60, yEnd - 30, xEnd - 45 - 60, yEnd - 30, C_BLACK);
		UG_DrawLine(xEnd - 15 - 60, yEnd - 29, xEnd - 45 - 60, yEnd - 29, C_BLACK);
		UG_DrawLine(xEnd - 15 - 60, yEnd - 31, xEnd - 45 - 60, yEnd - 31, C_BLACK);
	}
	else if(pressed == HZoomOut){
		//Horizontal +
		UG_FillFrame(xEnd - 10, yEnd - 10, xEnd - 50, yEnd - 50, 0x505050);
		UG_DrawLine(xEnd - 9, yEnd - 9, xEnd - 51, yEnd - 9, 0xA0A0A0);
		UG_DrawLine(xEnd - 9, yEnd - 9, xEnd - 9, yEnd - 51, 0xA0A0A0);
		UG_DrawLine(xEnd - 51, yEnd - 9, xEnd - 51, yEnd - 51, 0x404040);
		UG_DrawLine(xEnd - 9, yEnd - 51, xEnd - 51, yEnd - 51, 0x404040);
		UG_DrawLine(xEnd - 15, yEnd - 30, xEnd - 45, yEnd - 30, C_BLACK);
		UG_DrawLine(xEnd - 15, yEnd - 29, xEnd - 45, yEnd - 29, C_BLACK);
		UG_DrawLine(xEnd - 15, yEnd - 31, xEnd - 45, yEnd - 31, C_BLACK);
		UG_DrawLine(xEnd - 30, yEnd - 15, xEnd - 30, yEnd - 45, C_BLACK);
		UG_DrawLine(xEnd - 29, yEnd - 15, xEnd - 29, yEnd - 45, C_BLACK);
		UG_DrawLine(xEnd - 31, yEnd - 15, xEnd - 31, yEnd - 45, C_BLACK);
	}

	return pressed;
}

static void mainMenu(){
	MENU menu_s;
	g_status.isMainWindow = FALSE;
	memset(&menu_s, '\0', sizeof(MENU));
	menu_s.title = "Sub Menu"; //"Main Menu";
	menu_s.items[0].name = (char *)SubMenuItemNames[0];
	menu_s.items[1].name = (char *)SubMenuItemNames[1];
	menu_s.items[2].name = (char *)SubMenuItemNames[2];
	menu_s.items[3].name = (char *)SubMenuItemNames[3];
	menu_s.items[4].name = (char *)SubMenuItemNames[4];
	menu_s.items[5].name = (char *)SubMenuItemNames[5];
  //menu_s.items[6].name = "Calibrate";
	menu_s.items[0].exec = menu_setDate;
	menu_s.items[1].exec = menu_calibrateAdc;
	menu_s.items[2].exec = menu_resetSettings;
	menu_s.items[3].exec = menu_formatCard;
	menu_s.items[4].exec = menu_information;
	menu_s.items[5].exec = menu_saveFormat;
  //menu_s.items[6].exec = menu_calibrateAdc;

	menu(&menu_s, &g_buttonStatus);
}

static void serviceMenu(){
	MENU menu_s;
	memset(&menu_s, 1, sizeof(MENU));
	menu_s.title = "Service Menu";
	menu_s.items[0].name = "ADC offset";
	menu_s.items[1].name = "Status";
	menu_s.items[2].name = "";//"Touch Calibr"; - TODO: Implement touch screen calibration
	menu_s.items[3].name = "";
	menu_s.items[4].name = "";
	menu_s.items[5].name = "";
	menu_s.items[6].name = "";
	menu_s.items[7].name = "";
	menu_s.items[0].exec = smenu_setAdcOffset;
	menu_s.items[1].exec = smenu_status;
	menu_s.items[2].exec = NULL; //smenu_touchScreenCalibration;
	menu_s.items[3].exec = NULL;
	menu_s.items[4].exec = NULL;
	menu_s.items[5].exec = NULL;
	menu_s.items[6].exec = NULL;
	menu_s.items[7].exec = NULL;

	menu(&menu_s, &g_buttonStatus);
}

void cbSetAdcOffset(DIALOG *dlg, DLG_MESSAGE *msg){
	uint16_t offsetADC;
	DLG_SPINBOX *sbSign, *sbThousands, *sbHundreds, *sbTens, *sbUnits;
	if((msg->id == OBJ_TYPE_BUTTON) && (msg->sub_id == 0)){
		sbSign = (DLG_SPINBOX*)(getDialogObjectById(dlg, 2)->data);
		sbThousands = (DLG_SPINBOX*)(getDialogObjectById(dlg, 3)->data);
		sbHundreds = (DLG_SPINBOX*)(getDialogObjectById(dlg, 4)->data);
		sbTens = (DLG_SPINBOX*)(getDialogObjectById(dlg, 5)->data);
		sbUnits = (DLG_SPINBOX*)(getDialogObjectById(dlg, 6)->data);
		offsetADC = sbThousands->valueCurrent * 1000 + sbHundreds->valueCurrent * 100 + sbTens->valueCurrent * 10 + sbUnits->valueCurrent;
		if(sbSign->valueCurrent == 0){
			offsetADC = -offsetADC;
		}
		g_systemCalibrationData.offsetADC = offsetADC;
		EE_WriteVariable(0x0100, offsetADC);
	}
	if(msg->id == OBJ_TYPE_BUTTON)
		setDialogState(dlg, DLG_STATE_CLOSE);
}

static void smenu_setAdcOffset(){
	static char *signs[] = {"-", "+"};
	DIALOG dlg;
	DLG_OBJECT objects[7];
	DLG_BUTTON butt[2];
	DLG_SPINBOX sbSign, sbThousands, sbHundreds, sbTens, sbUnits;
	uint8_t ids[2] = {0, 1};
	int16_t offsetADC;
	char *captions[2];
	captions[0] = "OK";
	captions[1] = "Cancel";
	createDialog(&dlg, objects, 7, cbSetAdcOffset);
	setDialogSize(&dlg, 420, 100);
	setDialogTitleText(&dlg, "Set ADC Offset");
	if(!createButtonsInButtonBox(&dlg, 2, butt, ids, captions))
		return;
	createSpinBox(&dlg, &sbSign, 2, 15, TITLE_HEIGHT + 5);
	setSpinBoxTextItems(&sbSign, signs, 2);
	createSpinBox(&dlg, &sbThousands, 3, 15 + 70, TITLE_HEIGHT + 5);
	createSpinBox(&dlg, &sbHundreds, 4, 15 + 70 * 2, TITLE_HEIGHT + 5);
	createSpinBox(&dlg, &sbTens, 5, 15 + 70 * 3, TITLE_HEIGHT + 5);
	createSpinBox(&dlg, &sbUnits, 6, 15 + 70 * 4, TITLE_HEIGHT + 5);
	setSpinBoxInterval(&sbThousands, 0, 9);
	setSpinBoxInterval(&sbHundreds, 0, 9);
	setSpinBoxInterval(&sbTens, 0, 9);
	setSpinBoxInterval(&sbUnits, 0, 9);
	offsetADC = g_systemCalibrationData.offsetADC;
	if(offsetADC < 0){
		offsetADC = -offsetADC;
		setSpinBoxValue(&sbSign, 0); //-
	}
	else{
		setSpinBoxValue(&sbSign, 1); //+
	}
	setSpinBoxValue(&sbThousands, offsetADC / 1000);
	setSpinBoxValue(&sbHundreds, offsetADC % 1000 / 100);
	setSpinBoxValue(&sbTens, offsetADC % 100 / 10);
	setSpinBoxValue(&sbUnits, offsetADC % 10);
	do{
	  updateDialog(&dlg);
	}
	while(getDialogState(&dlg) == DLG_STATE_SHOWN);
}

//static void smenu_touchScreenCalibration(){
//	const uint16_t xDisplay[3] = {25, 440, 240};
//	const uint16_t yDisplay[3] = {25, 245, 135};
//	uint16_t /*xTouchController[3], yTouchController[3], */ n;
//	//int16_t xDeltas[3];
//	//int16_t yDeltas[3];
//	//int16_t xOffset, yOffset;
//	STMPE_SetDefaultCalibrationData();
//	for(n = 0; n < 3; n++){
//		UG_FillScreen(C_WHITE);
//		UG_DrawLine(xDisplay[n] - 6, yDisplay[n], xDisplay[n] + 6, yDisplay[n], C_BLACK);
//		UG_DrawLine(xDisplay[n], yDisplay[n] - 5, xDisplay[n], yDisplay[n] + 6, C_BLACK);
//		while(!STMPE_isPressed());
//		//xTouchController[n] = STMPE_getX0(); //g_touchScreenStatus.xTouchController;
//		//yTouchController[n] = STMPE_getY0(); //g_touchScreenStatus.yTouchController;
//		//xDeltas[n] = (int32_t)xTouchController[n] - (int32_t)xTouchController[n];
//		//yDeltas[n] = (int32_t)yTouchController[n] - (int32_t)yTouchController[n];
//	}
//	//xOffset = (xTouchController[0] + xTouchController[1] + xTouchController[2]) / 3;
//	//yOffset = (yTouchController[0] + yTouchController[1] + yTouchController[2]) / 3;
//	//TODO: Tada paskaiciuoti ir issaugoti i flash siuos parametrus:
//	//g_systemCalibrationData.touchScreenCalibrationData.xOffset;
//	//g_systemCalibrationData.touchScreenCalibrationData.yOffset;
//	//TODO: Realizuoti ^siu parametru nuskaityma is flash inicializacijos metu.
//}

static void smenu_status(){
	char str[50];
	UG_SetBackcolor(C_WHITE);
	UG_SetForecolor(C_BLACK);
	UG_FillScreen(C_WHITE);
	do{
		sprintf(str, "ADC value: %d     \nTemperature: %d     ", g_adcBuffer[1], g_temperatureBuffer[1]);
		UG_PutString(1, 25, str);
	}
	while(!STMPE_isPressed());
}

#define IN_ZOOM_BUTTONS_BOUNDS (((9 <= column && column <= 51) && ((9 <= row && row <= 51) || (69 <= row && row <= 111))) || ((189 <= row <= 231) && ((249 <= column <= 291) || (309 <= column <= 351))))

static FRESULT drawGraph(enum saveFormat saveFormat, FIL *dstFile){
	const uint16_t xStart = GRAPH_START_X;        //12;
	const uint16_t yStart = GRAPH_START_Y;        //20;
	const uint16_t xEnd = xStart + GRAPH_WIDTH;   //360;
	const uint16_t yEnd = yStart + GRAPH_HEIGHT;  //240;
	const uint16_t div = GRID_DENSITY;
	const uint16_t div5 = div / 5;
	FRESULT fresult;
	uint16_t y;
	uint32_t row, column;
	uint8_t xBackToZeroIcon = 0, yBackToZeroIcon = 0;
	uint16_t y_top = 0, y_bot = 0;//, yLineLength = 0, yLineLengthNext = 0, yLineLengthPrevious = 0; //Line for sample dots connection
	uint32_t samplesToTake;
	uint16_t samplesToTakeTemperature;
	bool drawZoomButtons = g_status.isZoomButtonsEnabled;
  bool drawBackToZeroButton = g_status.isBackToZeroButtonEnabled && (g_status.graphOffsetX != 0 || g_status.offsetMiliTeslaTmp != 0);

	g_drawInProgress = TRUE;
  //NVIC_DisableIRQ(EXTI9_5_IRQn);

  SSD_WriteCommand(SSD1963_SET_ADDRESS_MODE);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x20);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	SSD_WriteCommand(SSD1963_SET_COLUMN_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(xStart >> 8); //First Col
	SSD_WriteData(xStart);
	SSD_WriteData(xEnd >> 8); //Last Col
	SSD_WriteData(xEnd);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteCommand(SSD1963_SET_PAGE_ADDRESS);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(yStart >> 8); //First Page
	SSD_WriteData(yStart);
	SSD_WriteData(yEnd >> 8); //Last Page
	SSD_WriteData(yEnd);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

	SSD_WriteCommand(SSD1963_WRITE_MEMORY_START);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_DC);

	uint16_t prescaler = EXTADC_GET_PRESCALER;
	uint32_t T_sampling;
	T_sampling = (4 * (prescaler + 1)) / 24;
  
	if(g_status.isMeasurementStarted){
		if(g_settings.hDiv < 625){
			samplesToTake = g_settings.hDiv * 2 * 12; //Samplas kas 500 ns, 12 langeliu
		}
		else{
				uint16_t x = 24 * g_settings.hDiv / ADC_MAX_SAMPLES;
				samplesToTake = 24 * g_settings.hDiv / (x + 1) + 1;
		}
	}
	else{
		//Measurement stopped; a user wants to see a measurement history.
		//P.S.: Currently T_sampling MUST be an integer number of microseconds or a 0.5 microsecond.
		if(prescaler >= 5){ //T_sampling is 1 us or more
			samplesToTake = 12 * g_settings.hDiv / T_sampling;
		}
		else{
			samplesToTake = g_settings.hDiv * 2 * 12; //Samplas kas 500 ns, 12 langeliu
		}
	}
  
	if(T_sampling > 1) //Jei visas buferis trunka 15 ms, tai
		samplesToTakeTemperature = g_settings.hDiv * 12 / ((g_settings.hDiv * 12 > 6000000UL) ? 100000UL : 10000UL);
	else
		samplesToTakeTemperature = 1;

	DMA1_Channel1->CCR &= ~DMA_CCR_EN; //DMA and temperature measurements must be stopped before conversion
	int32_t samplesOffset = 0;

	g_status.samplesToTakeForDrawing = samplesToTake;
	samplesOffset = -(int32_t)samplesToTake * (int32_t)g_status.graphOffsetX / GRAPH_WIDTH;

	bool wasMeasurementStarted = g_status.isMeasurementStarted;
	g_status.isMeasurementStarted = false;

  fresult = generatePreview(samplesToTake, samplesToTakeTemperature, samplesOffset, saveFormat, dstFile, wasMeasurementStarted);

  if(saveFormat == TSV || saveFormat == CSV){
		//NVIC_EnableIRQ(EXTI9_5_IRQn);
		return fresult;
	}

	//Conversion completed thus we have to enable temperature measurements and DMA again if required
	g_status.isMeasurementStarted = wasMeasurementStarted;
	if(g_status.isSensorConnected)
		DMA1_Channel1->CCR |= DMA_CCR_EN;

  int zero_pos_offset = -GRAPH_ZERO_POS;
	//uint16_t columnData = 0, prevColumnData = 0 , nextColumnData = 0;
	uint16_t triggerLevelLine = GRAPH_HEIGHT + zero_pos_offset - (g_settings.triggerLevelMiliTesla + g_status.offsetMiliTeslaTmp) * div / g_settings.vDiv;
  int data = 0, data_n1 = 0;
  for(column = 0; column <= GRAPH_WIDTH; column++){
    //--- exit draw in faster way.
    if(STMPE_isPressed())
      return FR_OK;
    else if(g_status.startStopMeasurementButtonPressed){
      if(g_status.isMeasurementStarted)
        return FR_OK;
    }
    
    y_top = 0;                                                                  // zero - no line to draw!!!
    data_n1 = data;                                                             // last data
    data = zero_pos_offset + (g_previewBuffer[column] & 0x7FFF);                // current data
    if( (data > zero_pos_offset) && (data_n1 > zero_pos_offset)){
      if(data > data_n1){
        y_bot = data;
        y_top = data_n1;
      }
      else{
        y_bot = data_n1;
        y_top = data;
      }
      y_top -= (GRAPH_LINE_WIDTH+1)/2;
      y_bot += GRAPH_LINE_WIDTH/2;
    }
    
		for(row = 0; row <= GRAPH_HEIGHT; row++){
			//TODO: Nenumatyta situacija, kai linija turi prasideti zoom mygtuko braizymo metu,
			//t. y. $yLineStart yra mygtuko viduje. Suo metu ji tikriausia yra ignoruojama ir neparodoma.
			//Tai labai smulkus bug'as todel taisymo prioritetas super zemas.

			if(drawZoomButtons && (row >=9 && row <= 111 || row >= 189 && row <= 231) )
      {
          if((column == 9   && ( ((row >= 9) && (row <= 51)) || ((row >= 69) && (row <= 111)) )) ||
				   ((column == 249 || column == 309) && ((row >= 189) && (row <= 231)))){
					//Vertical lighted
					SSD1963_DATA_BUS->ODR = C_ZOOM_U_16b;//rgb888torgb565(C_USR1); //0xA514
					SSD1963_CONTROL_BUS->BSRRH = SSD1963_SIGNAL_WR;
					SSD1963_CONTROL_BUS->BSRRL = SSD1963_SIGNAL_WR;
					continue;
				}
				else if((column == 51 && ( ((row >= 9) && (row <= 51)) || ((row >= 69) && (row <= 111)) )) ||
						((column == 291 || column == 351) && ((row >= 189) && (row <= 231)))){
					//Vertical shadow
					SSD1963_DATA_BUS->ODR = C_ZOOM_S_16b;//rgb888torgb565(C_USR2); //0x4208;
					SSD1963_CONTROL_BUS->BSRRH = SSD1963_SIGNAL_WR;
					SSD1963_CONTROL_BUS->BSRRL = SSD1963_SIGNAL_WR;
					continue;
				}
				else if(((column > 9 && column < 51) && ((row >= 9 && row <= 51) || (row >= 69 && row <= 111))) ||
						(((column > 249 && column < 291) || (column > 309 && column < 351)) && (row >= 189 && row <= 231)) ){
					if(row == 9 || row == 69 || row == 189){
						//Horizontal lighted
						SSD1963_DATA_BUS->ODR = C_ZOOM_U_16b;//rgb888torgb565(C_USR1); //0xA514
					}
					else if(row == 51 || row == 111 || row == 231){
						//Horizontal shadow
						SSD1963_DATA_BUS->ODR = C_ZOOM_S_16b;//rgb888torgb565(C_USR2); //0x4208;
					}
					else if((((row >= 29 && row <= 31) || (row >= 89 && row <= 91)) && (column >= 15 && column <= 45)) ||
							((row >= 209 && row <= 211) && ((column >= 255 && column <= 285) || (column >= 315 && column <= 345)))){
						//Horizontal dash (-)
						SSD1963_DATA_BUS->ODR = C_SCREEN_16b;
					}
					//else if(((row >= 15 && row <= 45) && (column >= 29 && column <= 31)) ||
					//		((row >= 195 && row <= 225) && (column >= 269 && column <= 271))){
					//^ Commented because for someone + and - buttons looked not logical so now zoom-in is "-" and zoom-out is "+" (2017-06-05)
					else if(((row >= 15 && row <= 45) && (column >= 29 && column <= 31)) ||
							((row >= 195 && row <= 225) && (column >= 60+269 && column <= 60+271))){
						//Vertical dash (|)
						SSD1963_DATA_BUS->ODR = C_SCREEN_16b;
					}
					else{
						//Body
						SSD1963_DATA_BUS->ODR = C_ZOOM_B_16b;//rgb888torgb565(C_USR3); //0x630C;
					}
					SSD1963_CONTROL_BUS->BSRRH = SSD1963_SIGNAL_WR;
					SSD1963_CONTROL_BUS->BSRRL = SSD1963_SIGNAL_WR;
					continue;
				}
			}
			if(drawBackToZeroButton){
				if((column == 309) && (row >= 9 && row <= 51)){
					//Vertical lighted
					SSD1963_DATA_BUS->ODR = C_ZOOM_U_16b;//rgb888torgb565(C_USR1); //0xA514
					SSD1963_CONTROL_BUS->BSRRH = SSD1963_SIGNAL_WR;
					SSD1963_CONTROL_BUS->BSRRL = SSD1963_SIGNAL_WR;
					continue;
				}
				else if((column == 351) && (row >= 9 && row <= 51)){
					//Vertical shadow
					SSD1963_DATA_BUS->ODR = C_ZOOM_S_16b;//rgb888torgb565(C_USR2); //0x4208;
					SSD1963_CONTROL_BUS->BSRRH = SSD1963_SIGNAL_WR;
					SSD1963_CONTROL_BUS->BSRRL = SSD1963_SIGNAL_WR;
					continue;
				}
				else if((column > 309 && column < 351) && (row >= 9 && row <= 51)){

					if(row == 9){
						//Horizontal lighted
						SSD1963_DATA_BUS->ODR = C_ZOOM_U_16b;//rgb888torgb565(C_USR1);//0xA514;
					}
					else if(row == 51){
						//Horizontal shadow
						SSD1963_DATA_BUS->ODR = C_ZOOM_S_16b;//rgb888torgb565(C_USR2); //0x4208;
					}
					else{
						//Body
						if(xBackToZeroIcon < 40 && (g_backToZeroIcon[xBackToZeroIcon][yBackToZeroIcon / 8] & (0x80 >> yBackToZeroIcon % 8))){
							SSD1963_DATA_BUS->ODR = C_SCREEN_16b;
						}
						else{
							SSD1963_DATA_BUS->ODR = C_ZOOM_B_16b;//rgb888torgb565(C_USR3); //0x630C;
						}
						if(yBackToZeroIcon++ == 40){
							yBackToZeroIcon = 0;
							xBackToZeroIcon++;
						}
					}
					SSD1963_CONTROL_BUS->BSRRH = SSD1963_SIGNAL_WR;
					SSD1963_CONTROL_BUS->BSRRL = SSD1963_SIGNAL_WR;
					continue;
				}
			}

			//Save vertical trigger position
			if(g_previewBuffer[column] & 0x8000){
				g_status.verticalTriggerLinePositionPx = column;
			}
      
      //--- vertical trigger line
			//Check whether I have to put vertical marker in this whole column
			if((g_previewBuffer[column] & 0x8000) && column!=1){ //0x8000 - triggered flag
				SSD1963_DATA_BUS->ODR = C_TRIGGER_16b;//rgb888torgb565(C_USR4); //0x07FF;   //CYAN (it is inverted Red(0xF800))
				SSD1963_CONTROL_BUS->BSRRH = SSD1963_SIGNAL_WR;
				SSD1963_CONTROL_BUS->BSRRL = SSD1963_SIGNAL_WR;
				continue; //Done. Go to next column.
			}/**/
      
      if(row == GRAPH_HEIGHT - GRAPH_ZERO_POS ){ //|| row == GRAPH_HEIGHT + 1 - GRAPH_ZERO_POS
        SSD1963_DATA_BUS->ODR = C_ZERO_POS_16b;
					SSD1963_CONTROL_BUS->BSRRH = SSD1963_SIGNAL_WR;
					SSD1963_CONTROL_BUS->BSRRL = SSD1963_SIGNAL_WR;
          continue;
      }

			if(y_top == row + yStart){
				//Draw sampling line
				for(y = y_top; y <= y_bot; y++, row++)
        {
					//if(IN_ZOOM_BUTTONS_BOUNDS){row++; break;}
          if(adcCal.enable)
            SSD1963_DATA_BUS->ODR = C_USR_LINE_ADC_CAL_16b; 
          else
            SSD1963_DATA_BUS->ODR = C_USR_LINE_16b; 
          SSD1963_CONTROL_BUS->BSRRH = SSD1963_SIGNAL_WR;
          SSD1963_CONTROL_BUS->BSRRL = SSD1963_SIGNAL_WR;
					if((drawZoomButtons && (((9 <= column && column <= 51) && ((9 - 1 <= row && row <= 51) || 
             (69 - 1 <= row && row <= 111))) || ((189 - 1 <= row && row <= 231) && 
             ((249 <= column && column <= 291) || (309 <= column && column <= 351)) ))) ||
					   (drawBackToZeroButton && ((309 <= column && column <= 351) && (9 - 1 <= row && row <= 51)) )){
						//^This "if" is used to keep zoom buttons on the top of graph
						//if(y + 44 < yLineEnd)
						//	yLineStart = y + 44;
						row++;
						break;
					}
				}
				row--;
			}
			else  // ticckets and grid
      { 
				if(((row <= 2) && (column % div5 == 0)) || ((row >= 2 && row <= 4) && (column % div == 0)) ||
				   ((row > yEnd - yStart - 3) && (column % div5 == 0)) || 
           ((row == yEnd - yStart - 3 || row == yEnd - yStart - 4) && (column % div == 0)) ||
				   ((row % div5 == 0 && column <= 2) || (row % div == 0 && column <= 4)) ||
				   ((row % div5 == 0 && column > xEnd - xStart - 3) || 
           (row % div == 0 && column > xEnd - xStart - 5)))
				{
					SSD1963_DATA_BUS->ODR = C_TICKETS_16b;
					SSD1963_CONTROL_BUS->BSRRH = SSD1963_SIGNAL_WR;
					SSD1963_CONTROL_BUS->BSRRL = SSD1963_SIGNAL_WR;
				}
				else if((column % div5 == 0 && row % div == 0) || (column % div == 0 && row % div5 == 0)){
					SSD1963_DATA_BUS->ODR = C_GRID_16b;
					SSD1963_CONTROL_BUS->BSRRH = SSD1963_SIGNAL_WR;
					SSD1963_CONTROL_BUS->BSRRL = SSD1963_SIGNAL_WR;
				}
				else{
					if(row == triggerLevelLine && g_status.mode == SINGLE_SEQUENCE)
						SSD1963_DATA_BUS->ODR = C_TRIGGER_16b;
					else
						SSD1963_DATA_BUS->ODR = C_SCREEN_16b;
					SSD1963_CONTROL_BUS->BSRRH = SSD1963_SIGNAL_WR;
					SSD1963_CONTROL_BUS->BSRRL = SSD1963_SIGNAL_WR;
				}
			}
		}
	}
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	g_drawInProgress = FALSE;

	SSD_WriteCommand(SSD1963_SET_ADDRESS_MODE);
	GPIO_ResetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);
	SSD_WriteData(0x00);
	GPIO_SetBits(SSD1963_CONTROL_BUS, SSD1963_SIGNAL_CS);

  //NVIC_EnableIRQ(EXTI9_5_IRQn);

	return FR_OK;
}

void touchTriggerSelectedAction(){
	//Jei dabar mygtukas "REC" reiskia "Set trigger level"
	UG_FontSelect(&FONT_8X14);
	UG_SetForecolor(C_WHITE);
	UG_SetBackcolor(C_BLACK);
	UG_PutString(67, 35, "Touch graph to set trigger level.");
	while(STMPE_isPressed());
	while(!STMPE_isPressed()); //Waiting for choice
	if(getRightMenuSelectedItemId() >= 0){
		ExtADC_clearBuffer();
	}
	else{
		g_settings.triggerLevelMiliTesla = -((int32_t)g_settings.vDiv * ((int32_t)STMPE_getY0() - DISPLAY_HEIGHT + GRAPH_START_Y) / GRID_DENSITY + g_status.offsetMiliTeslaTmp);
		while(STMPE_isPressed());
		ExtADC_clearBuffer();
		mainWindow(); //To refresh trigger level representation in the bottom of "Mode" button.
	}
	drawGraph(UNUSED, NULL);
}

bool isTemperatureInvalid(){
	bool status = true;
	if(g_status.temperature > *g_calibrationTable.tempMin * 10 && g_status.temperature < *g_calibrationTable.tempMax * 10){
		status = false;
	}
	return status;
}

void startMeasurement(){
  if(adcCal.enable == 0)
    adcCal_init();
  
  
	//while(GPIOB->IDR & 2 == 0); //Wait until "Cancel" button is released if it is pressed
	g_status.startStopMeasurementButtonPressed = false;
	g_status.graphOffsetX = 0;
	g_status.offsetMiliTeslaTmp = 0;
	if(g_status.temperature < *g_calibrationTable.tempMin * 10){
		stopMeasurement();
		msgBox("Temperature is too low. Measurement \n stopped.", "Measurement", CRITICAL, isTemperatureInvalid);
		return;
	}
	if(g_status.temperature > *g_calibrationTable.tempMax * 10){
		stopMeasurement();
		msgBox("Temperature is too high. Measurement \n stopped.", "Measurement", CRITICAL, isTemperatureInvalid);
		return;
	}

	g_status.graphOffsetX = 0;
	updateSamplingFrequency();
	ExtADC_clearBuffer();
	memset(g_previewBuffer, 0, GRAPH_WIDTH * sizeof(uint16_t));
	if(g_status.mode != CONTINUOUS){
		//ExtADC_clearBuffer();
		//STMPE_forceUnpress();
		g_status.isMeasurementStarted = TRUE;
		mainWindow(); //Refresh
		drawGraph(UNUSED, NULL); //Refresh
	}

	if(g_status.mode == FORCE){
    if(g_status.isMeasurementStarted == false){ //enable to measure temperature
      g_status.isMeasurementStarted = true;
    }
    
		uint16_t samplesToTake;
		recalculateHorizontalDivAccordingPulseLengthAndMode();
		ExtADC_stop(); //If already started

		//Bendras atvejis:
		uint16_t x = 24 * g_settings.hDiv / ADC_MAX_SAMPLES;
		samplesToTake = 24 * g_settings.hDiv / (x + 1); // + 1;
    
    //if(g_settings.pulseLength_us >= 1000000UL) //If impulse is very long then show "Collecting data..." message
    msg_collectingData();
    ExtADC_startSingle(samplesToTake);
    while(EXTADC_IN_PROGRESS()){
      if(g_status.startStopMeasurementButtonPressed){
        break;
      }
    }
  
		stopMeasurement();
    g_isInited = false; // enable to  reinit external-adc after force mode
	}
	else if(g_status.mode == EXTERNAL)
  {
		int32_t length;
		bool isTriggerValid = false;
		//900 <= triggerPulseLength <= 1100:
		const uint16_t triggerPulseLengthMin = 900;
		const uint16_t triggerPulseLengthMax = 1100;

		msg_waiting();
		recalculateHorizontalDivAccordingPulseLengthAndMode();
		ExtADC_startCircular();
		do{
			//Time sensitive case! (temporary disable IRQ)
			if(g_settings.pulseLength_us <= triggerPulseLengthMax){ //Impulse length less than max trigger (sync) pulse length 900 <= sync_us <= 1100 us
				length = 0;
				__disable_irq();
				while((GPIOB->IDR & 1) && (GPIOA->IDR & 0x0100) /*&& (STMPE_isPressed() == false)*/); //Waiting for a rising edge (falling edge about optocoupler).
				if((GPIOA->IDR & 0x0100) == 0){
					delay_us(100000);
					while((GPIOA->IDR & 0x0100) == 0);
					g_status.startStopMeasurementButtonPressed = true;
				}
				g_status.triggeredPosition = EXTADC_BUFFER_POSITION();
				delay_us(g_settings.pulseLength_us); //Measuring
				stopMeasurement();
				while(!(GPIOB->IDR & 1) && length < triggerPulseLengthMax){ //Waiting for the end of sync impulse and calculating its length
					delay_us(100);
					length += 100;
				} 
				if((triggerPulseLengthMin <= length + g_settings.pulseLength_us /*&& length + g_settings.pulseLength_us <= triggerPulseLengthMax*/)){
					isTriggerValid = true;
				}
				__enable_irq();
				if(g_status.startStopMeasurementButtonPressed /*STMPE_isPressed()*/){
					//^This "if" checks whether the length of trigger (sync) impulse is correct and operation was not canceled. If true then results will be deleted:
					ExtADC_clearBuffer();
				   STMPE_PressCheck();
				   STMPE_Init();
					return;
				}
			}
			//Time insensitive case (leave IRQ enabled)
			else{ //Impulse length is greater than max trigger (sync) pulse length g_settings.pulseLength_us > 1100
				length = 0;
__disable_irq();
				while((GPIOB->IDR & 1) && (GPIOA->IDR & 0x0100) /*(STMPE_isPressed() == false)*/);
				if((GPIOA->IDR & 0x0100) == 0 /* STMPE_isPressed() */){
					delay_us(100000); //For button debouncing
					//delay_us(100000); //For button debouncing
					while((GPIOA->IDR & 0x0100) == 0);
					g_status.startStopMeasurementButtonPressed = true;
					stopMeasurement();
					ExtADC_clearBuffer();
					__enable_irq();
//					  STMPE_PressCheck();
//					  STMPE_Init();
					return;
				}

				g_status.triggeredPosition = EXTADC_BUFFER_POSITION();

				while(!(GPIOB->IDR & 1) && length < triggerPulseLengthMax){
					delay_us(100);
					length += 100;
				}
				__enable_irq();

				if(triggerPulseLengthMin < length /*&& length < triggerPulseLengthMax*/){
					isTriggerValid = true;

					if(g_settings.pulseLength_us >= 1000000UL && isTriggerValid) //If impulse is very long then show "Collecting data..." message
						msg_collectingData();

					// 900 <= length <= 1100 us (triggerPulseLengthMin < length <= triggerPulseLengthMax)
					uint32_t time_us = g_settings.pulseLength_us - length;
					uint16_t time_ms = time_us / 1000;
					uint16_t time_s = time_ms / 1000;
					uint16_t timeLeft_s = time_s;
					while(timeLeft_s && g_status.startStopMeasurementButtonPressed == false && STMPE_isPressed() == false){
						delay_ms(1000);
						timeLeft_s--;
					}
					delay_ms(time_ms - time_s * 1000);
					delay_us(time_us % 1000);
					stopMeasurement();
				}

				if(g_status.startStopMeasurementButtonPressed /*STMPE_isPressed()*/){
					//delay_us(100000); //For button debouncing
					stopMeasurement();
					ExtADC_clearBuffer();
				  STMPE_PressCheck();
				  STMPE_Init();
					return;
				}
			}
		}
		while(!isTriggerValid);
		//g_settings.timeOffset_us = -g_settings.hDiv;

	}
	else if(g_status.mode == SINGLE_SEQUENCE){ //"Internal trigger"
		uint16_t adcValue, adcValuePosition;
		int16_t temperature;
		uint16_t magneticField_mT;

		msg_waiting();

		recalculateHorizontalDivAccordingPulseLengthAndMode();
		ExtADC_startCircular();
		g_temperatureBufferPosition = 0;
		delay_ms(200); //TODO: Sitas ADC uzlaikymas reikalingas, kad ADC buferyje atsirastu duomenu ir gal susikauptu temperaturos duomenu(?). Pagal ideja jo nereiketu arba jis turetu buti labai trumpas - cia kazkas netaip, aiskintis.

		//TODO: Algoritmas netestuotas su impulsais, kuriu trukme < 20ms (?)

		//Time sensitive case:
		if(g_settings.pulseLength_us < 1){ // <= 10000U) For impulses up to 10 ms
			//int16_t oldTemperature = g_status.temperature;
			uint16_t triggerAdcValue = mag2adc(g_settings.triggerLevelMiliTesla, g_status.temperature);
		delay_ms(200);
			while(STMPE_isPressed());
			__disable_irq();
			//do{
			//	STMPE_PressCheck(); //TODO: This function might be executed too slowly. Perhaps I need to find alternative.
			//}
//			while(triggerAdcValue < g_adcBuffer[EXTADC_BUFFER_POSITION()] && STMPE_getX0() == 0); //STMPE_isPressed() can ONLY be used when STMPE_pressCheck() is called from touch screen interrupt.
			while(triggerAdcValue < g_adcBuffer[EXTADC_BUFFER_POSITION2()] && (GPIOA->IDR & 0x0100)); //Direct button check because interrupts are disabled (g_status.startStopMeasurementButtonPressed doesn't change
			g_status.triggeredPosition = EXTADC_BUFFER_POSITION2();
			delay_us(g_settings.pulseLength_us); //I hope this function can deal with 10 ms delays
			stopMeasurement();
			if((GPIOA->IDR & 0x0100) == 0){ //STMPE_getX0() != 0){
				//while(STMPE_isPressed());
				delay_us(100000); //For button debouncing
				while((GPIOA->IDR & 0x0100) == 0);
				g_status.startStopMeasurementButtonPressed = true;
				//__enable_irq();
//				if(getRightMenuSelectedItemId() == 1){
//					touchTriggerSelectedAction();
//				}
				ExtADC_clearBuffer();
			}
			__enable_irq();
		}
		//Time insensitive case:
		else{ //For impulses more than 10 ms(todo: kolkas perdaryta, kad nuo 0, t.y. sis algoritmas naudojamas visada)
			do{
				adcValuePosition = EXTADC_BUFFER_POSITION();
				//assert(adcValuePosition < g_currentADC_BufferLength);
				adcValuePosition = ((adcValuePosition > 0) ? adcValuePosition : g_currentADC_BufferLength) - 1;
				//assert(adcValuePosition < g_currentADC_BufferLength);
				adcValue = g_adcBuffer[adcValuePosition];
				temperature = g_temperatureBuffer[(g_temperatureBufferPosition > 0) ? g_temperatureBufferPosition - 1 : TEMPERATURE_BUFFER_LENGTH - 1];
				magneticField_mT = adc2mag(adcValue, temperature);
				if(STMPE_isPressed() && getRightMenuSelectedItemId() == 1){
					touchTriggerSelectedAction();
					continue;
				}
				if(STMPE_isPressed() || g_status.startStopMeasurementButtonPressed){
					//stopMeasurement();
					ExtADC_clearBuffer();
					//return;
					break;
				}
			}
			while(magneticField_mT < g_settings.triggerLevelMiliTesla);
			if(magneticField_mT >= g_settings.triggerLevelMiliTesla){
				//If was not canceled
				g_status.triggeredPosition = EXTADC_BUFFER_POSITION();
				if(g_settings.pulseLength_us < 1000){
					delay_us(g_settings.pulseLength_us);
				}
				else{
					if(g_settings.pulseLength_us > 1000000UL){
						int32_t msToDelay = g_settings.pulseLength_us / 1000;
						msg_collectingData();
						do{
							if(g_status.startStopMeasurementButtonPressed){
								ExtADC_clearBuffer();
								break;
							}
							delay_ms(100);
							msToDelay -= 100;
						}
						while(msToDelay > 0);
					}
					else{
						delay_ms(g_settings.pulseLength_us / 1000);
						delay_us(g_settings.pulseLength_us % 1000);
					}
				}
				//g_settings.timeOffset_us = -g_settings.hDiv;
			}
			stopMeasurement();
		}
	}
	else{ //Continuous
		ExtADC_startCircular();
		g_temperatureBufferPosition = 0;
		g_status.isMeasurementStarted = TRUE;
		//g_settings.timeOffset_us = 0;
	}
	if(!g_status.startStopMeasurementButtonPressed){
		g_status.isDataReceived = true;
	}
}

void stopMeasurement(){
	g_status.isMeasurementStarted = FALSE;
	ExtADC_stop();
  //--- restart adc cal if not calibration are started
}

//----------------- DISPLAY
//__attribute__( ( always_inline ) ) uint16_t getValueFromPreviewBuffer(uint32_t dot){
//	int32_t position;
//	if(dot >= GRAPH_WIDTH){
//		return false;
//	}
//	position = g_previewBufferPosition - GRAPH_WIDTH + dot;
//	if(position < 0){
//		position = GRAPH_WIDTH + position;
//	}
//	return g_previewBuffer[position];
//}

//__attribute__( ( always_inline ) ) bool getValueFromBufferADC(int32_t sampleNumber, uint16_t *value){
bool getValueFromBufferADC(int32_t sampleNumber, uint16_t *value){
	int32_t lastPosition = EXTADC_BUFFER_POSITION();
	int32_t position;
	if(sampleNumber > 0 || sampleNumber <= -g_currentADC_BufferLength){
		return false;
	}
	position = lastPosition + sampleNumber;
	if(position < 0){
		position = g_currentADC_BufferLength + position;
	}
  if((position < 0) && (position >= sizeof(g_adcBuffer)/sizeof(g_adcBuffer[0])) )
    return false;
  
	*value = g_adcBuffer[position];
	if(*value == 0){ //After looking at a few tables I decided that ADC value can't be zero even if B=10T and t=45C (there it is usually somewhere around 30000)
		return false;
	}
	return true;
}

__attribute__( ( always_inline ) ) bool sampleNumber2index(int32_t sampleNumber, uint16_t *index){
	int32_t lastPosition = EXTADC_BUFFER_POSITION();
	int32_t position;
	if(sampleNumber > 0 || sampleNumber < -g_currentADC_BufferLength){
		return false;
	}
	position = lastPosition + sampleNumber;
	if(position < 0){
		position = g_currentADC_BufferLength + position;
	}
	*index = position; //Position and index are simply synonyms
	return true;
}

__attribute__( ( always_inline ) ) int16_t getValueFromBufferTemp(int32_t sampleNumber){
	int32_t lastPosition = (g_temperatureBufferPosition > 0) ? g_temperatureBufferPosition - 1 : TEMPERATURE_BUFFER_LENGTH - 1;
	int32_t position;
	if(sampleNumber > 0){
		//Use the newest temperature data (because there is high possibility that temperature will not change significantly in near future)
		return g_temperatureBuffer[(lastPosition - 1 < 0) ? TEMPERATURE_BUFFER_LENGTH - 1 : lastPosition - 1];
	}
	else if(sampleNumber < -TEMPERATURE_BUFFER_LENGTH){
		//Use the oldest temperature data
		return g_temperatureBuffer[lastPosition];
	}
	position = lastPosition + sampleNumber;
	if(position < 0){
		position = TEMPERATURE_BUFFER_LENGTH + position;
	}
	return g_temperatureBuffer[position];
}

static uint16_t ringBufferDistance(uint16_t p1, uint16_t p2, uint16_t size){
	uint16_t distance;
	assert(p1 < size && p2 < size);
	if(p1 < p2){
		distance = p2 - p1;
	}
	else{
		distance = size - p1 + p2;
	}
	return distance;
}

static FRESULT generatePreview(uint32_t srcSamplesToTake, uint16_t samplesToTakeTemperature, int32_t samplesOffset, enum saveFormat saveFormat, FIL *dstFile, bool m){ //m - usually false. See drawGraph() code for detailed explanation
  //, g_adcBuffer, g_currentADC_BufferLength, beginAdcBufferPosition,
  //uint16_t *src = g_adcBuffer; //TODO: Paskui perrasyti graziai ir aiskiai :)
  uint16_t *dst = g_previewBuffer;
  uint16_t dstLength = GRAPH_WIDTH + 1; //g_previewBufferPosition;
  // EDIT: added +1 to account for an early rounding problem
  int32_t srcIndexRatio = (int32_t)((srcSamplesToTake<<8)/dstLength) +1;
  int32_t srcIndex, srcIndexPrevious, dstIndex, temperatureIndex, currentSampleNumber;
  int32_t counter, adc_currentSample, /*adc_previousSample,*/ counterNoReset; //, srcCounter;
  int magneticField_mT;
  uint16_t temperatureBufferValuesCount;
  bool isTriggeredFlagSet = false;
  static uint16_t adc_rawPreviousPosition = 0; //EXTADC_BUFFER_POSITION();
  uint16_t distance;
  uint16_t dstDelta = 0;

  FRESULT fresult;
  if(saveFormat == TSV || saveFormat == CSV){
    char line[21];
    UINT bytesWritten;
    line[0] = '2';
    line[1] = '0';
    line[2] = (uint8_t)(((RTC->DR) >> 20) & 0x0000000F) + '0';
    line[3] = (uint8_t)(((RTC->DR) >> 16) & 0x0000000F) + '0';
    line[5] = (uint8_t)(((RTC->DR) >> 12) & 0x00000001) + '0';
    line[6] = (uint8_t)(((RTC->DR) >> 8) & 0x0000000F) + '0';
    line[8] = (uint8_t)(((RTC->DR) >> 4) & 0x00000003) + '0';
    line[9] = (uint8_t)((RTC->DR) & 0x0000000F) + '0';
    line[11] = (uint8_t)(((RTC->TR) >> 20) & 0x00000003) + '0';
    line[12] = (uint8_t)(((RTC->TR) >> 16) & 0x0000000F) + '0';
    line[14] = (uint8_t)(((RTC->TR) >> 12) & 0x00000007) + '0';
    line[15] = (uint8_t)(((RTC->TR) >> 8) & 0x0000000F) + '0';
    line[17] = (uint8_t)(((RTC->TR) >> 4) & 0x00000007) + '0';
    line[18] = (uint8_t)((RTC->TR) & 0x0000000F) + '0';
    line[19] = '\0';

    fresult = f_write(dstFile, line, strlen(line), &bytesWritten);
    sprintf(line, "\r\n\r\n");
    fresult = f_write(dstFile, line, strlen(line), &bytesWritten);
    if(saveFormat == TSV){
      sprintf(line, "No.\t");
      fresult = f_write(dstFile, line, strlen(line), &bytesWritten);
      fresult = sprintf(line, "time, us\t");
      fresult = f_write(dstFile, line, strlen(line), &bytesWritten);
      sprintf(line, "B, mT\r\n\r\n");
      fresult = f_write(dstFile, line, strlen(line), &bytesWritten);
    }
    else if(saveFormat == CSV){
      sprintf(line, "No.,\"");
      fresult = f_write(dstFile, line, strlen(line), &bytesWritten);
      
      sprintf(line, "time, us\"");
      fresult = f_write(dstFile, line, strlen(line), &bytesWritten);
      
      sprintf(line, ",\"B, mT\"\r\n\r\n");
      fresult = f_write(dstFile, line, strlen(line), &bytesWritten);
    }
    

    if(fresult != FR_OK || bytesWritten == 0){
      if(bytesWritten == 0){
        return 0xFF; //Unknown error but according examples such situation is possible (?)
      }
      return fresult;
    }

    dstLength = srcSamplesToTake;
    srcIndexRatio = (int32_t)((srcSamplesToTake<<8)/dstLength) +1;
  }

    temperatureBufferValuesCount = samplesToTakeTemperature;
///    temperatureIndex = beginTemperatureBufferPosition;

    counterNoReset = 0;

	int32_t srcFirstSample = 0 - srcSamplesToTake;

	dstIndex = 0;

	srcFirstSample += samplesOffset;

    counter = (srcFirstSample * 256)/(int32_t)srcIndexRatio;

//    //$counter correction because of possible error if (almost) all buffer is taken

    //---

    dstIndex = 0;
    if(g_settings.hDiv > 200000UL){ //Tas bugas kuris cia workaround'intas pastebimas tik tuo atveju, tad apsisaugojimui del galimo naujo bug'o :)
		if(m && (saveFormat == UNUSED)){
			distance = ringBufferDistance(adc_rawPreviousPosition, EXTADC_BUFFER_POSITION(), g_currentADC_BufferLength);
			if((srcIndexRatio >> 8) > 0){
				dstDelta = distance / (srcIndexRatio >> 8);
			}
			else{
				dstDelta = 0;
			}
			dstIndex = GRAPH_WIDTH - dstDelta;
			counter += dstIndex;
		}
    }

    {
    	//Laikinas ir super bukas sprendimas siekiant isvengti bugu. Siaip reiketu naudoti dar viena ziedini buferi, manau.
    	int p;
    	for(p = 0; p < dstIndex; p++){
    		dst[p] = dst[p + dstDelta];
    	}
    }

    	//uint32_t srcIndex1, srcIndex2; //Both srcIndex1 and srcIndex2 should be after ADC buffer position

    adc_currentSample = 0; ///((counterNoReset*srcIndexRatio)>>8);  //Gaunas visad 0!!! Nes counterNoReset visad 0 iki cia. TODO: Gal tiesiog prilygint nuliui?
    currentSampleNumber = 0;
    uint16_t adcValue;
    bool adcValueExists;
    //srcIndexRatio = 10572;
    for (srcIndex = counter*srcIndexRatio/256; dstIndex < dstLength; dstIndex++) {
    	srcIndexPrevious = srcIndex;
    	srcIndex = counter*srcIndexRatio/256;
    	//adc_previousSample = adc_currentSample;
    	adc_currentSample = counter*srcIndexRatio/256;

    	assert(dstIndex < dstLength);
      
    	adcValueExists = getValueFromBufferADC(srcIndex, &adcValue);
    	if(!adcValueExists){
    		if(saveFormat != CSV && saveFormat != TSV){
    			dst[dstIndex] = 0;
    		}
			counter++;
			counterNoReset++;
			continue;
    	}
    	temperatureIndex = adc_currentSample * (int32_t)temperatureBufferValuesCount / (int32_t)srcSamplesToTake;
    	//----------------- Calculate magnetic field from ADC value
    	{
    	    uint16_t row, column;
    	    uint32_t valueGenerated, valueGeneratedNext;
    	    uint16_t value, valueNext;
    	    uint32_t t_step_local;
          int d_step_local;
    	    uint16_t tempMin, tempStep, tempMax;
    	    uint16_t magnMin, magnStep, magnMax;
    	    uint16_t adcCorrectedValue = adcValue + g_systemCalibrationData.offsetADC;
    	    int16_t temperature;

    	    magnMin = *g_calibrationTable.magnMin;
    	    magnStep = *g_calibrationTable.magnStep;
    	    magnMax = *g_calibrationTable.magnMax;
    	    tempMin = *g_calibrationTable.tempMin * 10;
    	    tempStep = *g_calibrationTable.tempStep * 10;
    	    tempMax = *g_calibrationTable.tempMax * 10;
    	    if(samplesToTakeTemperature == 1){
    	    	temperature = getValueFromBufferTemp(0); //g_temperatureBuffer[g_temperatureBufferPosition ? g_temperatureBufferPosition - 1 : TEMPERATURE_BUFFER_LENGTH - 1] * 10;
    	    }
    	    else
    	    	temperature = getValueFromBufferTemp(temperatureIndex); //g_temperatureBuffer[temperatureIndex] * 10;

    	    g_calibrationTable.columns = (magnMax - magnMin + magnStep) / magnStep;
    	    g_calibrationTable.rows = (tempMax - tempMin + tempStep) / tempStep;

    	    row = temperature / tempStep; //Find out which row we have to use for calculations
    	    if(row > g_calibrationTable.rows) //To be sure that we will not go out of bounds
    	        row = g_calibrationTable.rows;
    	   
          if(false){
            //--- by pass for now
          }
          else{
            column = 1;
            do{
              /* There is a big possibility that there is no record for temperature which we read from the sensor  */
              /* Then we have to use approximation to get row with data exactly for this temperature. Now we know  */
              /* that read temperature is more than or equal (if we are lucky) to temperature of selected row, but */
              /* it is always less than temperature in next row. So we will use these rows for approximation.      */
              value = getValueFromCalibrationTable(row, column);
              valueNext = getValueFromCalibrationTable(row + 1, column);
              /* We have to calculate a step */
              t_step_local = (((value - valueNext) * 10000) / tempStep);
              valueGenerated = value - (temperature - row * tempStep) * t_step_local / 10000;
              column++;
            }
            while((valueGenerated > adcCorrectedValue) && (column < g_calibrationTable.columns - 1));
            
            if(column >= g_calibrationTable.columns - 1)
              magneticField_mT = magnMax;
            else{
              column-=2;
              valueGeneratedNext = valueGenerated;

              value = getValueFromCalibrationTable(row, column);
              valueNext = getValueFromCalibrationTable(row + 1, column);
              t_step_local = (((value - valueNext) * 10000) / tempStep);
              valueGenerated = value - (temperature - row * tempStep) * t_step_local / 10000;
              
              int tmp = 0;
              d_step_local = (valueGenerated - valueGeneratedNext);
              if(d_step_local != 0)
                tmp = (int)((valueGenerated - adcCorrectedValue)*(int)magnStep)/d_step_local;
              magneticField_mT = column * magnStep + tmp;
              
              adcCal.sum += magneticField_mT; adcCal.cnt++;                         // magnetic field value not limited
              if(magneticField_mT < 0)
                magneticField_mT = 0;
            }
          }
    	}
    	//----------------- END: Calculate magnetic field from ADC value

    	if(saveFormat == TSV || saveFormat == CSV){
			char line[100];
			UINT bytesWritten;
			if(g_settings.hDiv == 1){
				if(saveFormat == TSV){
					snprintf(line, 100,
							"%lu\t%lu.%c\t%u\r\n",
							currentSampleNumber,
							currentSampleNumber * g_settings.hDiv * 12 / dstLength,
							(currentSampleNumber % 2) ? '5' : '0',
							magneticField_mT);
				}
				else if(saveFormat == CSV){
					snprintf(line, 100,
							"%lu,%lu.%c,%u\r\n",
							currentSampleNumber,
							currentSampleNumber * g_settings.hDiv * 12 / dstLength,
							(currentSampleNumber % 2) ? '5' : '0',
							magneticField_mT);
				}
			}
			else{
				if(saveFormat == TSV){
					snprintf(line, 100, "%lu\t%s\t%u\r\n", currentSampleNumber,
							uint64ToDecimal((uint64_t)currentSampleNumber * (uint64_t)g_settings.hDiv * 12 / (uint64_t)dstLength),
							magneticField_mT);
				}
				else if(saveFormat == CSV){
					snprintf(line, 100, "%lu,%s,%u\r\n", currentSampleNumber,
							uint64ToDecimal((uint64_t)currentSampleNumber * (uint64_t)g_settings.hDiv * 12 / (uint64_t)dstLength),
							magneticField_mT);
				}
			}
			fresult = f_write(dstFile, line, strlen(line), &bytesWritten);
			if(fresult != FR_OK){
				return fresult;
			}
		}

    	currentSampleNumber++;

      //--- why 2 times added offset ???
      //--- it is afer measure usser can add offset to view graph top?
    	//magneticField_mT += g_status.offsetMiliTeslaTmp;
    	if((int16_t)magneticField_mT + g_status.offsetMiliTeslaTmp > 0)
        magneticField_mT += g_status.offsetMiliTeslaTmp;
      //else
      //  magneticField_mT = 0;

    	if(magneticField_mT > (int)(g_settings.vDiv * 8)){
    		magneticField_mT = g_settings.vDiv * 8; //Limit
    	}

    	if(saveFormat != CSV && saveFormat != TSV){
			dst[dstIndex] = GRAPH_HEIGHT - magneticField_mT * GRAPH_HEIGHT / (g_settings.vDiv * 8) + GRAPH_START_Y ;

			if((g_status.mode == SINGLE_SEQUENCE || g_status.mode == EXTERNAL) && !isTriggeredFlagSet){
				uint16_t convertedCurrentIndex, convertedPreviousIndex;
				if(sampleNumber2index(srcIndex, &convertedCurrentIndex) && sampleNumber2index(srcIndexPrevious, &convertedPreviousIndex)){
					//^If only to ensure. Function should always return true in this case.
					if(convertedPreviousIndex < convertedCurrentIndex){
						if(convertedPreviousIndex < g_status.triggeredPosition && g_status.triggeredPosition <= convertedCurrentIndex){
							//Situation 1: -------a-t-b----------------------------- (a < t <= b)
							dst[dstIndex] |= 0x8000U; //Set flag "triggered"
							isTriggeredFlagSet = true;
						}
					//}
					//else if(convertedPreviousIndex < convertedCurrentIndex){ //TODO: Why same as first if??? Possible BUG
						else if((convertedPreviousIndex > g_status.triggeredPosition && g_status.triggeredPosition <= convertedCurrentIndex) ||
								(convertedPreviousIndex <= g_status.triggeredPosition && g_status.triggeredPosition > convertedCurrentIndex)){
							//Situation 2: t-b------------------------------------a- (a > t <= b)
							//Situation 3: -b------------------------------------a-t (a <= t > b)
							dst[dstIndex] |= 0x8000U; //Set flag "triggered"
							isTriggeredFlagSet = true;
						}
					}
					else{
						//$triggeredPosition is outside dst bounds
						//dst[1] |= 0x8000U;
						//isTriggeredFlagSet = true;
					}
				}
			}
    	}
		counter++;
		counterNoReset++;
    }
    
    //--- adc auto calibration start ---
    if(adcCal.enable){
      if(adcCal.cnt >= ADC_CAL_SUM_CNT){
        adcCal.rej_cnt++;
        if(adcCal.rej_cnt > ADC_CAL_REJ_CNT){
          adcCal.rej_cnt--;
          adcCal.B_avg_mT = adcCal.sum/adcCal.cnt;
          int offset = ADC_CAL_SCALE(adcCal.B_avg_mT);// (B_avg_mT*231)/100;    // scale factor determinet experimental
          g_systemCalibrationData.offsetADC += offset;
          if (g_systemCalibrationData.offsetADC > ADC_CAL_ADC_MAX)
            g_systemCalibrationData.offsetADC = ADC_CAL_ADC_MAX;
          else if (g_systemCalibrationData.offsetADC < -ADC_CAL_ADC_MAX)
            g_systemCalibrationData.offsetADC = -ADC_CAL_ADC_MAX;
          if((adcCal.B_avg_mT >= -ADC_CAL_ERROR_MT) && (adcCal.B_avg_mT <= ADC_CAL_ERROR_MT)){
            adcCal.est_cnt++;
            if(adcCal.est_cnt > ADC_CAL_EST_CNT){
              saveAdcOffset();                                                  // save adc offset to memory
              adcCal_init();
            }
          }
          else
            adcCal.est_cnt = 0;
          adcCal.sum = 0;
          adcCal.cnt = 0;
          adcCal.try_cnt++;
          //--- force adc calibration to stop if some fail occur
          if(adcCal.try_cnt > ADC_CAL_TRY_CNT){
            adcCal.enable = false;
            saveAdcOffset();  
            adcCal_init();
          }
        }
        else{
          adcCal.sum = 0;
          adcCal.cnt = 0;
        }
      }
    }
    else{
      //--- calibration are disabled sow use average to calc mean value of field
      if(adcCal.timer_ms == 0 && adcCal.cnt > 3){ 
        adcCal.rej_cnt++;
        if(adcCal.rej_cnt > 5){
          adcCal.rej_cnt--;
          adcCal.B_avg_mT = adcCal.sum/adcCal.cnt;
        }
        else
          adcCal.B_avg_mT = 0;
        adcCal.sum = 0;
        adcCal.cnt = 0;
        adcCal.timer_ms = ADC_CAL_TMR_P;
      }
    }
    //--- end adc auto calibration
    adc_rawPreviousPosition = EXTADC_BUFFER_POSITION();

    if((g_status.mode == SINGLE_SEQUENCE || g_status.mode == EXTERNAL) && !isTriggeredFlagSet){
//    	uint16_t convertedSrcIndex;
//    	if(sampleNumber2index(srcIndex, &convertedSrcIndex)){
//    		if(g_status.triggeredPosition >= convertedSrcIndex){
//    			dst[dstLength - 1] |= 0x8000;
//    		}
//    		else{
//    			dst[0] |= 0x8000;
//    		}
//    	}

    	//I didn't think much about this solution and I think that it is dirty and not very correct. But it looks like that it works well.
      if(g_status.graphOffsetX > 0)
      {
        //--- for condition in sd card save not to fail: external trigger, offset was. scale-400us. 500mT.
        // and I changed  to 5ms/div and save button pressed (dstLength is not in range!!!)
        if(dstLength - 1 >= 0 && dstLength - 1 <= GRAPH_WIDTH)
          dst[dstLength - 1] |= 0x8000;
      }
    	else
    		dst[1] |= 0x8000;
    }
    return FR_OK;
}

//Min 6
static void temperatureAsString(char *temperatureStr){
	temperatureStr[0] = (g_status.temperature <= -1000) ? '-' : ' ';
	if(g_status.temperature < 0 && g_status.temperature > -1000)
		temperatureStr[1] = '-';
	else if(ABS(g_status.temperature) >= 1000)
		temperatureStr[1] = ABS(g_status.temperature) / 1000 + '0';
	else
		temperatureStr[1] = ' ';
	temperatureStr[2] = ABS(g_status.temperature) % 1000 / 100 + '0';
	temperatureStr[3] = '.';
	temperatureStr[4] = ABS(g_status.temperature) % 100 / 10 + '0';
	temperatureStr[5] = '\0';
}

void refreshMainWindowStatus(){
	char mainWindowStatusStr[40]; //33
	char hDivPrefix, vDivPrefix;
	uint16_t hDivValue, vDivValue;
	bool showHorizontalDivValue;

	uint8_t n;
	/*char time[] = "HH:MM";
	n = (uint8_t)(((RTC->TR) >> 20) & 0x00000003);
	time[0] = n + '0';
	n = (uint8_t)(((RTC->TR) >> 16) & 0x0000000F);
	time[1] = n + '0';
	n = (uint8_t)(((RTC->TR) >> 12) & 0x00000007);
	time[3] = n + '0';
	n = (uint8_t)(((RTC->TR) >> 8) & 0x0000000F);
	time[4] = n + '0';*/
  
	if(g_settings.hDiv / 1000000UL){
		hDivValue = g_settings.hDiv / 1000000UL;
		hDivPrefix = ' ';
		showHorizontalDivValue = (g_settings.hDiv % 1000000UL == 0);
	}
	else if(g_settings.hDiv / 1000){
		hDivValue = g_settings.hDiv / 1000UL;
		hDivPrefix = 'm';
		showHorizontalDivValue = (g_settings.hDiv % 1000UL == 0);
	}
	else{
		hDivValue = g_settings.hDiv;
		hDivPrefix = '\xE6';
		showHorizontalDivValue = true;
	}
	if(g_settings.vDiv / 1000UL){
		vDivValue = g_settings.vDiv / 1000UL;
		vDivPrefix = ' ';
	}
	else{
		vDivValue = g_settings.vDiv;
		vDivPrefix = 'm';
	}

	char temperatureStr[6];
	temperatureAsString(temperatureStr);
  
	//if(showHorizontalDivValue)
  int avg100 = 0;
  for(int i = 0; i < 100; i++)
    avg100 += g_adcBuffer[i];
  avg100 = avg100/100;
  
  if(ABS(adcCal.B_avg_mT) <= 1000)
    sprintf(mainWindowStatusStr, "\t%3u%cT/div\t%3u%cs/div\tB(t)=%dmT\t\t", 
            vDivValue, vDivPrefix, hDivValue, hDivPrefix, adcCal.B_avg_mT);
	else
    sprintf(mainWindowStatusStr, "\t%3u%cT/div\t%3u%cs/div\tB(t)=%.2fT\t\t", 
            vDivValue, vDivPrefix, hDivValue, hDivPrefix, (float)adcCal.B_avg_mT/1000.0);

  //sprintf(mainWindowStatusStr, "\t%3u%cT/div\t%3u%cs/div\tB(t)=%dmT\t\t", 
  //          vDivValue, vDivPrefix, hDivValue, hDivPrefix, avg100);
  
  //else
	//	sprintf(mainWindowStatusStr, "%3u %cT/div\t\t\t", adcCal.B_avg_mT, vDivValue, vDivPrefix, temperatureStr);

	UG_SetForecolor(C_YELLOW);
	UG_SetBackcolor(C_BLACK);
	UG_FontSelect(&FONT_10X16);
	UG_PutString(1, 3, mainWindowStatusStr);
}

void changeStdVerticalDiv(bool toIncrease){
	uint32_t vDiv = g_settings.vDiv;
	uint32_t base = vDiv;
	uint8_t exponent = 0;
	if((vDiv < MAX_MT_DIV && toIncrease) || (vDiv > MILITESLA_MIN && !toIncrease)){ //MAX_MT_DIV - max mT/Div.
		while(base / 10){
			base /= 10;
			exponent++;
		}
		if(toIncrease){
			vDiv *= 2;
			if(base == 2){
				vDiv += vDiv / 4;
			}
		}
		else{
			if(base == 1 || base == 2)
				vDiv /= 2;
			else if(base == 5)
				vDiv = vDiv / 5 * 2;
		}
		g_settings.vDiv = vDiv;
		//if(g_status.isMeasurementStarted)
		//	updateSamplingFrequency();
		//updateAdcBufferSize();

	}
}

void changeStdHorizontalDiv(BOOL toIncrease){
	uint32_t hDiv = g_settings.hDiv;
	uint32_t base = hDiv;
	uint32_t multiplier = 1;
	if((hDiv < MAX_MKS_DIV && toIncrease) || (hDiv > 1 && !toIncrease)){ //MAX_MKS_DIV - max us/Div.
		//Find base and exponent
		while(base / 10){
			base /= 10;
			multiplier *= 10;
		}

		//Convertion to std div if needed
		if(base == 3 || base == 4)
			base = 5;
		else if(base > 5){
			base = 1;
			multiplier *= 10;
		}
		hDiv = base * multiplier;

		//Change horizontal std div from std div
		if(toIncrease){
			hDiv *= 2;
			if(base == 2){
				hDiv += hDiv / 4;
			}
		}
		else{
			if(base == 1 || base == 2)
				hDiv /= 2;
			else if(base == 5)
				hDiv = hDiv / 5 * 2;
		}
		g_settings.hDiv = hDiv;
		if(g_status.isMeasurementStarted)
			updateSamplingFrequency();
		//updateAdcBufferSize();

		if(!g_status.isMeasurementStarted){
			//TODO: Galvoti apie isdidinima. Apacioje du nesekmingi bandymai


			//1:
	//		int32_t samplesOffsetInGraphMiddle = -g_status.samplesToTakeForDrawing * ((int32_t)g_status.graphOffsetX - GRAPH_WIDTH / 2) / GRAPH_WIDTH;
	//		int32_t sampleOffset = samplesOffsetInGraphMiddle + g_settings.hDiv /2;
	//		g_status.graphOffsetX = sampleOffset * GRAPH_WIDTH / g_status.samplesToTakeForDrawing + g_status.graphOffsetX;

			//2:
//			int32_t graphMiddle = g_status.samplesToTakeForDrawing / 2;
//
//			drawGraph(); //To recalculate g_status.samplesToTakeForDrawing according to new g_settings.hDiv
//
//
//			g_status.graphOffsetX = (-graphMiddle - g_status.samplesToTakeForDrawing/2) * GRAPH_WIDTH / g_status.samplesToTakeForDrawing + g_status.graphOffsetX;
//
//
//			//g_status.graphOffsetX = -(int32_t)graphMiddle - g_status.samplesToTakeForDrawing / 2;
		}
	}
}

//static void adjustStdHorizontalDiv(uint32_t toThisValue){
//	if(5 > toThisValue && toThisValue > 2)
//		g_settings.hDiv = 5;
//
//	else if(10 > toThisValue && toThisValue > 5)
//		g_settings.hDiv = 10;
//	else if(20 > toThisValue && toThisValue > 10)
//		g_settings.hDiv = 20;
//	else if(50 > toThisValue && toThisValue > 20)
//		g_settings.hDiv = 50;
//
//	else if(100 > toThisValue && toThisValue > 50)
//		g_settings.hDiv = 100;
//	else if(200 > toThisValue && toThisValue > 100)
//		g_settings.hDiv = 200;
//	else if(500 > toThisValue && toThisValue > 200)
//		g_settings.hDiv = 500;
//
//	else if(1000 > toThisValue && toThisValue > 500)
//		g_settings.hDiv = 1000;
//	else if(2000 > toThisValue && toThisValue > 1000)
//		g_settings.hDiv = 2000;
//	else if(5000 > toThisValue && toThisValue > 2000)
//		g_settings.hDiv = 5000;
//
//	else if(10000 > toThisValue && toThisValue > 5000)
//		g_settings.hDiv = 10000;
//	else if(20000 > toThisValue && toThisValue > 10000)
//		g_settings.hDiv = 20000;
//	else if(50000 > toThisValue && toThisValue > 20000)
//		g_settings.hDiv = 50000;
//
//	else if(100000UL > toThisValue && toThisValue > 50000)
//		g_settings.hDiv = 100000UL;
//	else if(200000UL > toThisValue && toThisValue > 100000UL)
//		g_settings.hDiv = 200000UL;
//	else if(500000UL > toThisValue && toThisValue > 200000UL)
//		g_settings.hDiv = 500000UL;
//
//	else if(1000000UL > toThisValue && toThisValue > 500000UL)
//		g_settings.hDiv = 1000000UL;
//	else if(2000000UL > toThisValue && toThisValue > 1000000UL)
//		g_settings.hDiv = 2000000UL;
//	else if(5000000UL > toThisValue && toThisValue > 2000000UL)
//		g_settings.hDiv = 5000000UL;
//
//	else
//		g_settings.hDiv = toThisValue;
//
//	if(g_status.isMeasurementStarted)
//		updateSamplingFrequency();
//}

static void updateSamplingFrequency(){
	uint32_t x, p;
	//See documentation for formulas explanation
	x = 24 * g_settings.hDiv / g_currentADC_BufferLength;
	p = 2 + 3 * x;
	if(p == 2)
		EXTADC_WARP();
	else
		EXTADC_NORMAL();
	ExtADC_changeClkPrescaler(p);
}

//static void updateAdcBufferSize(){
//	if(g_settings.hDiv > 50000){ //If >50 ms, we can see slowly signal changes
//		uint32_t _2T_clk_us = 96 * (EXTADC_GET_PRESCALER + 1) / 12;
//		g_currentADC_BufferLength = 12 * g_settings.hDiv / _2T_clk_us;
//		g_currentADC_BufferLength *= 2;
//	}
//	else
//		g_currentADC_BufferLength = ADC_MAX_SAMPLES;
//	ExtADC_setActiveBufferSize(g_currentADC_BufferLength);
//}

void f(){

}
void EE_Wrt_Word(int adr, int data)
{
  EE_WriteVariable(adr,   (uint16_t)data);
	EE_WriteVariable(adr+1, (uint16_t)(data>>16));
}
int EE_Read_Word(int adr)
{
  int data32;
  unsigned short data;
  EE_ReadVariable(adr, &data);
  data32 = data;
  EE_ReadVariable(adr+1, &data);
  data32 |= (data << 16);
  return data32;
}

static void saveAdcOffset()
{
  EE_Wrt_Word(EE_ADC_ID_ADR, 0);                                              // clear id first
  EE_Wrt_Word(EE_ADC_OFFSET, g_systemCalibrationData.offsetADC);
  EE_Wrt_Word(EE_ADC_ID_ADR, EE_ID);  
}

static void saveSettings(){
	//Virtual EEPROM is already formatted.
	//P.S.: Simple flash wearing protection algorithm has been implemented in the library
	/*EE_WriteVariable(0x0000, (uint16_t)(g_settings.pulseLength_us >> 16));
	EE_WriteVariable(0x0001, (uint16_t)g_settings.pulseLength_us);
	EE_WriteVariable(0x0002, (uint16_t)(g_settings.triggerLevelMiliTesla >> 16));
	EE_WriteVariable(0x0003, (uint16_t)g_settings.triggerLevelMiliTesla);
  
	EE_WriteVariable(0x0004, (uint16_t)(g_settings.vDiv >> 16));
	EE_WriteVariable(0x0005, (uint16_t)g_settings.vDiv);
  
	EE_WriteVariable(0x0006, (uint16_t)(g_settings.hDiv >> 16));
	EE_WriteVariable(0x0007, (uint16_t)g_settings.hDiv);
  
	EE_WriteVariable(0x0008, (uint16_t)g_settings.saveFormat);*/

  EE_Wrt_Word(EE_PULSE_LEN_us, g_settings.pulseLength_us);
  EE_Wrt_Word(EE_TRIG_LEV_mT, g_settings.triggerLevelMiliTesla);
  EE_Wrt_Word(EE_VERTICAL_DIV, g_settings.vDiv);
  EE_Wrt_Word(EE_HORIZONTAL_DIV, g_settings.hDiv);
  EE_Wrt_Word(EE_SAVE_FORMAT, g_settings.saveFormat);
  EE_Wrt_Word(EE_ID_ADR, EE_ID);
  
  //--- adc offset save only after calibration
  //EE_Wrt_Word(EE_ADC_ID_ADR, 0);                                              // clear id first
  //EE_Wrt_Word(EE_ADC_OFFSET, g_systemCalibrationData.offsetADC);
  //EE_Wrt_Word(EE_ADC_ID_ADR, EE_ID);                                          // set id after
  
}

static void restoreSettings(){
	const uint32_t default_pulseLength_us = 12;
	const uint32_t default_triggerLevelMiliTesla = MILITESLA_MIN;
	const uint32_t default_vDiv = DEFAULT_V_DIV;//MILITESLA_MIN;
	const uint32_t default_hDiv = DEFAULT_H_DIV;//1;            // in us
	uint16_t data;

	g_settings.isSingleSeqTouchModeSelected = FALSE;
 
	//if(EE_ReadVariable(0x0100, &data) == 0){
  if(EE_Read_Word(EE_ID_ADR) == EE_ID){
    /*
    //There is something in virtual EEPROM
		g_systemCalibrationData.offsetADC = data;
    
		EE_ReadVariable(0x0000, &data);
		g_settings.pulseLength_us = data << 16;
		EE_ReadVariable(0x0001, &data);
		g_settings.pulseLength_us |= data;

		EE_ReadVariable(0x0002, &data);
		g_settings.triggerLevelMiliTesla = data << 16;
		EE_ReadVariable(0x0003, &data);
		g_settings.triggerLevelMiliTesla |= data;

		EE_ReadVariable(0x0004, &data);
		g_settings.vDiv = data << 16;
		EE_ReadVariable(0x0005, &data);
		g_settings.vDiv |= data;

		EE_ReadVariable(0x0006, &data);
		g_settings.hDiv = data << 16;
		EE_ReadVariable(0x0007, &data);
		g_settings.hDiv |= data;

		EE_ReadVariable(0x0008, &data);
		g_settings.saveFormat = data;*/
    g_settings.pulseLength_us = EE_Read_Word(EE_PULSE_LEN_us);
    g_settings.triggerLevelMiliTesla = EE_Read_Word(EE_TRIG_LEV_mT);
    g_settings.vDiv = EE_Read_Word(EE_VERTICAL_DIV);
    g_settings.hDiv = EE_Read_Word(EE_HORIZONTAL_DIV);
    g_settings.saveFormat = EE_Read_Word(EE_SAVE_FORMAT);
    
    g_systemCalibrationData.offsetADC = 0;
    if(EE_Read_Word(EE_ADC_ID_ADR) == EE_ID)                                    // if id exist restore adc offset too.
      g_systemCalibrationData.offsetADC = EE_Read_Word(EE_ADC_OFFSET);

		//Now check if data is valid. If not, then use default values
		if(g_settings.pulseLength_us < 12 || g_settings.pulseLength_us > 60000000UL || (32000000UL <= g_settings.pulseLength_us && g_settings.pulseLength_us <= 32999999UL)) //g_settings.pulseLength_us == 32******UL - this is a workaround to strange bug
			g_settings.pulseLength_us = default_pulseLength_us;
		if(g_settings.triggerLevelMiliTesla > TESLA_MAX * 1000)
			g_settings.triggerLevelMiliTesla = default_triggerLevelMiliTesla;
		if(g_settings.vDiv > TESLA_MAX * 1000UL || g_settings.vDiv < MILITESLA_MIN)
			g_settings.vDiv = default_vDiv;
		if(g_settings.hDiv > MAX_MKS_DIV || g_settings.hDiv == 0)
			g_settings.hDiv = default_hDiv;
		if(g_settings.saveFormat != TSV && g_settings.saveFormat !=CSV)
			g_settings.saveFormat = CSV;

		updateSamplingFrequency(); //Restore ADC frequency
		ExtADC_clearBuffer();
	}
	else{
    //--- after born
		EE_Format();
		EE_SetVirtualSize(8);
		g_systemCalibrationData.offsetADC = 0;
		g_settings.pulseLength_us = default_pulseLength_us;
		g_settings.triggerLevelMiliTesla = default_triggerLevelMiliTesla;
		g_settings.vDiv = DEFAULT_V_DIV;//default_vDiv;
		g_settings.hDiv = DEFAULT_H_DIV;
		g_settings.saveFormat = CSV;
    g_systemCalibrationData.offsetADC = 0;

    //--- can't save data after born ???
    saveSettings();
	}
}

void cbSensorConnectedMsg(DIALOG *dlg, DLG_MESSAGE *msg){
	if(msg->id == OBJ_TYPE_BUTTON){
		setDialogState(dlg, DLG_STATE_CLOSE);
	}
}

void msg_sensorConnected(char *msg){
	DIALOG dlgSenCon;
	DLG_OBJECT objects[2];
	DLG_BUTTON butt[1];
	DLG_LABEL label;
	uint8_t ids[1] = {0};
	char *captions[1] = {"OK"};
	createDialog(&dlgSenCon, objects, 2, cbSensorConnectedMsg);
	setDialogSize(&dlgSenCon, 270, 80);
	setDialogTitleText(&dlgSenCon, "Information");
	if(!createButtonsInButtonBox(&dlgSenCon, 1, butt, ids, captions))
		return;

	createLabel(&dlgSenCon, &label, 1, 5, TITLE_HEIGHT + 7, 0, 0);
	setLabelText(&label, msg, C_WHITE, &FONT_12X20);
	//TODO: Put more information inside dialog
	do{
		updateDialog(&dlgSenCon);
		if(!g_status.isSensorConnected)
			break;
	}
	while(getDialogState(&dlgSenCon) == DLG_STATE_SHOWN);
	//STMPE_forceUnpress();
}

void cbSensorDisconnectedMsg(DIALOG *dlg, DLG_MESSAGE *msg){
	if(msg->id == OBJ_TYPE_BUTTON){
		setDialogState(dlg, DLG_STATE_CLOSE);
	}
}

static void msg_sensorDisconnected(char *msg){
	DIALOG dlgSenCon;
	DLG_OBJECT objects[2];
	DLG_BUTTON butt[1];
	DLG_LABEL label;
	uint8_t ids[1] = {0};
	char *captions[1] = {"OK"};
	createDialog(&dlgSenCon, objects, 2, cbSensorDisconnectedMsg);
	setDialogSize(&dlgSenCon, 270, 80);
	setDialogTitleText(&dlgSenCon, "Information");
	if(!createButtonsInButtonBox(&dlgSenCon, 1, butt, ids, captions))
		return;

	createLabel(&dlgSenCon, &label, 1, 5, TITLE_HEIGHT + 7, 0, 0);
	setLabelText(&label, msg, C_WHITE, &FONT_12X20);
	do{
		updateDialog(&dlgSenCon);
		if(g_status.isSensorConnected)
			break;
	}
	while(getDialogState(&dlgSenCon) == DLG_STATE_SHOWN);
}

static void msg_collectingData(){
	const uint32_t textColor = C_YELLOW;
	const uint32_t backgroundColor = 0x004667;
	const uint32_t shadowColor = 0x0000AA;
	uint16_t x1, y1, x2, y2;
	UG_FontSelect(&FONT_16X26);
	x1 = DISPLAY_WIDTH / 2 - sizeof("Collecting data...") * 16 / 2 ;//16 - font width
	y1 = DISPLAY_HEIGHT / 2 - 26 / 2; //26 - font height
	x2 = x1 + sizeof("Collecting data...") * 16; //16 - font width
	y2 = y1 + 26;//26 - font height
	UG_FillFrame(x1 - 10, y1 - 10, x2 + 10, y2 + 10, backgroundColor);
	UG_DrawFrame(x1 - 11, y1 - 11, x2 + 11, y2 + 11, C_WHITE);
	UG_FillFrame(x1 - 5, y2 + 12, x2 + 15, y2 + 17, shadowColor);
	UG_FillFrame(x2 + 12, y1 - 5, x2 + 17, y2 + 17, shadowColor);
	UG_SetForecolor(textColor);
	UG_SetBackcolor(backgroundColor);
	UG_PutString(x1, y1, "Collecting data...");
}

static void msg_waiting(){
	const uint32_t textColor = C_YELLOW;
	const uint32_t backgroundColor = 0x004667;
	const uint32_t shadowColor = 0x0000AA;
	uint16_t x1, y1, x2, y2;
	UG_FontSelect(&FONT_16X26);
	x1 = DISPLAY_WIDTH / 2 - sizeof("Press START/STOP button to cancel.") * 11 / 2; //10 - little font width
	y1 = DISPLAY_HEIGHT / 2 - 26 / 2 - 5 - 16 / 2; //26 - big font height, 5 px - space between lines, 16 - little font height
	x2 = x1 + sizeof("Press START/STOP button to cancel.") * 11 -10; //16 - font width
	y2 = y1 + 26 + 5 + 10; //26 - big font height, 5 - space between linex, 16 - little font height
	UG_FillFrame(x1 - 10, y1 - 10, x2 + 10, y2 + 10, backgroundColor);
	UG_DrawFrame(x1 - 11, y1 - 11, x2 + 11, y2 + 11, C_WHITE);
	UG_FillFrame(x1 - 5, y2 + 12, x2 + 15, y2 + 17, shadowColor);
	UG_FillFrame(x2 + 12, y1 - 5, x2 + 17, y2 + 17, shadowColor);

	UG_SetForecolor(textColor);
	UG_SetBackcolor(backgroundColor);
	UG_PutString(x1, y1, "Waiting for trigger..."); //26 - big font height, 5 - space between lines

	UG_FontSelect(&FONT_10X16);
	x1 = DISPLAY_WIDTH / 2 - sizeof("Press START/STOP button to cancel.") * 11 / 2 ;//10 - font width
	y1 = DISPLAY_HEIGHT / 2 - 16 / 2; //16 - font height
	//x2 = x1 + sizeof("Press START/STOP button to cancel.") * 10; //10 - font width
	//y2 = y1 + 53;//53 - font height
	UG_SetForecolor(textColor);
	UG_SetBackcolor(backgroundColor);
	UG_PutString(x1, y1 + 10 + 2, "Press START/STOP button to cancel.");
	//todo: apacioj parasyti: "Press START/STOP button to cancel." - prie "collecting data..." irgi tai prirasyti.
}

static void msg_custom(char *message){
	const uint32_t textColor = C_YELLOW;
	const uint32_t backgroundColor = 0x004667;
	const uint32_t shadowColor = 0x0000AA;
	uint16_t x1, y1, x2, y2;
	UG_FontSelect(&FONT_16X26);
	x1 = DISPLAY_WIDTH / 2 - strlen(message) * 16 / 2 ;//16 - font width
	y1 = DISPLAY_HEIGHT / 2 - 26 / 2; //26 - font height
	x2 = x1 + strlen(message) * 16; //16 - font width
	y2 = y1 + 26;//26 - font height
	UG_FillFrame(x1 - 10, y1 - 10, x2 + 10, y2 + 10, backgroundColor);
	UG_FillFrame(x1 - 5, y2 + 12, x2 + 15, y2 + 17, shadowColor);
	UG_FillFrame(x2 + 12, y1 - 5, x2 + 17, y2 + 17, shadowColor);
	UG_SetForecolor(textColor);
	UG_SetBackcolor(backgroundColor);
	UG_PutString(x1, y1, message);
	UG_DrawFrame(x1 - 11, y1 - 11, x2 + 11, y2 + 11, C_WHITE);
}

void menu_information(){
	information(&g_status.temperature, g_systemCalibrationData.offsetADC);
}

void infoSensorConnected(enum cal_status calStatus){
	switch(calStatus){
	case SUCCESSFUL: msg_sensorConnected("Sensor connected."); break;
	case INVALID_DATA: msg_sensorConnected("INVALID_DATA"); break;
	case COPY_ERROR: msg_sensorConnected("COPY_ERROR"); break;
	case SAME_SENSOR: msg_sensorConnected("SAME_SENSOR"); break;
	case ERASE_ERROR: msg_sensorConnected("ERASE_ERROR");
	}
}


//----------------- Calculate magnetic field from ADC value
//Unused. Perhaps I will use in the future somehow. Implementation of this function is duplicated in the resizeAndConvertData() with slight modifications for speed.
static uint16_t adc2mag(uint16_t adcValue, int16_t temperature){
    uint16_t row, column;
    uint32_t valueGenerated, valueGeneratedNext;
    uint16_t value, valueNext;
    uint32_t t_step_local, d_step_local;
    uint16_t magneticField_mT;
    uint16_t /*tempMin,*/ tempStep /*, tempMax*/;
    uint16_t /*magnMin,*/ magnStep, magnMax;
    adcValue += g_systemCalibrationData.offsetADC;

    //magnMin = *g_calibrationTable.magnMin;
    magnStep = *g_calibrationTable.magnStep;
    magnMax = *g_calibrationTable.magnMax;
    //tempMin = *g_calibrationTable.tempMin * 10;
    tempStep = *g_calibrationTable.tempStep * 10;
    //tempMax = *g_calibrationTable.tempMax * 10;

    //temperature *= 10;

    row = temperature / tempStep; //Find out which row we have to use for calculations
    if(row > g_calibrationTable.rows) //To be sure that we will not go out of bounds
        row = g_calibrationTable.rows - 1;
    //If the first number in the row is less than ADC value then there is no point for calculation. Result is always 0 in this case.
    if(getValueFromCalibrationTable(row, 0) <= adcValue)
    	magneticField_mT = 0;
    else{
		column = 1;
		do{
			/* There is a big possibility that there is no record for temperature which we read from the sensor  */
			/* Then we have to use approximation to get row with data exactly for this temperature. Now we know  */
			/* that read temperature is more than or equal (if we are lucky) to temperature of selected row, but */
			/* it is always less than temperature in next row. So we will use these rows for approximation.      */
			value = getValueFromCalibrationTable(row, column);
			valueNext = getValueFromCalibrationTable(row + 1, column);
			/* We have to calculate a step */
			t_step_local = (((value - valueNext) * 10000) / tempStep);
			valueGenerated = value - (temperature - row * tempStep) * t_step_local / 10000;
			column++;
		}
		while((valueGenerated > adcValue) && (column < g_calibrationTable.columns - 1));
		if(column >= g_calibrationTable.columns - 1)
			magneticField_mT = magnMax;
		else{
			column-=2;
			valueGeneratedNext = valueGenerated;

			value = getValueFromCalibrationTable(row, column);
			valueNext = getValueFromCalibrationTable(row + 1, column);
			t_step_local = (((value - valueNext) * 10000) / tempStep);
			valueGenerated = value - (temperature - row * tempStep) * t_step_local / 10000;
			if(adcValue > valueGenerated)
				magneticField_mT = 0;
			else{
				d_step_local = ((valueGenerated - valueGeneratedNext) << 16) / magnStep;
				magneticField_mT = column * magnStep + ((valueGenerated - adcValue) << 16) / d_step_local;
			}
		}
    }

	if((int16_t)magneticField_mT + g_settings.offsetMiliTesla > 0)
		magneticField_mT += g_settings.offsetMiliTesla;
	else
		magneticField_mT = 0;

	return magneticField_mT;
}

static uint16_t mag2adc(uint32_t magneticField, int32_t temperature){
	uint16_t rows, columns;
    uint16_t row, column;
    uint32_t selectedCellValue, rightCellValue, belowCellValue, bellowRightCellValue;
    uint32_t selectedRowCalculated, nextRowCalculated;
    uint32_t selectedColMagneticField, selectedRowTemperature;
    uint32_t deltaQueriedValueAndSelectedColumnValue_B;
    uint32_t deltaQueriedValueAndSelectedRowValue_t;
    uint32_t temperatureAdcValueStep;
    uint16_t tempStep = *g_calibrationTable.tempStep;
    uint16_t magnStep = *g_calibrationTable.magnStep;
    uint16_t adcValue;

    //       |                |                      |
    //-------+----------------+----------------------+--------------
    //       | thisCellValue  | rightCellValue       |
    //-------+----------------+----------------------+--------------
    //       | belowCellValue | bellowRightCellValue |
    //-------+----------------+----------------------+--------------
    //       |                |                      |

    rows = g_calibrationTable.rows;
    columns = g_calibrationTable.columns;

    row = temperature / tempStep; //Findout which row we have to use for calculations
    if(row > rows) //To be sure that we will not go out of bounds
        row = rows - 1;
    column = magneticField / magnStep;
    if(column > columns)
        column = columns - 1;
    selectedColMagneticField = column * magnStep;
    selectedRowTemperature = row * tempStep;

    selectedCellValue = getValueFromCalibrationTable(row, column);
    rightCellValue = getValueFromCalibrationTable(row, column + 1);
    belowCellValue = getValueFromCalibrationTable(row + 1, column);
    bellowRightCellValue = getValueFromCalibrationTable(row + 1, column + 1);

    //B
    deltaQueriedValueAndSelectedColumnValue_B = magneticField - selectedColMagneticField;
    selectedRowCalculated = selectedCellValue - (selectedCellValue - rightCellValue      ) * deltaQueriedValueAndSelectedColumnValue_B / magnStep; //Get ADC value step in current row and multiply it by $deltaSelectedCellValueAndRightCellValue_B, then subtract that from selected cell value
    nextRowCalculated     = belowCellValue    - (belowCellValue    - bellowRightCellValue) * deltaQueriedValueAndSelectedColumnValue_B / magnStep; //Like ^

    //t
    deltaQueriedValueAndSelectedRowValue_t = temperature - selectedRowTemperature;
    temperatureAdcValueStep = (selectedRowCalculated - nextRowCalculated) * 1000 / tempStep; //1000 - multiplier to increase precision without floating numbers
    adcValue = selectedRowCalculated - temperatureAdcValueStep * deltaQueriedValueAndSelectedRowValue_t / 1000;

    return adcValue;
}

//----------------- END: Calculate magnetic field from ADC value

static void enterStandbyMode(){
//	PWR->CR &= ~PWR_CR_PDDS; //Stop mode
//	PWR->CR |= PWR_CR_PDDS; //Voltage regulator in low power mode during sleep
//	SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk; //Set SLEEPDEEP bit of Cortex System Control Register
//	__WFE();
//	SCB->SCR &= (uint32_t)~((uint32_t)SCB_SCR_SLEEPDEEP_Msk); //Reset SLEEPDEEP bit of Cortex System Control Register

	//Remiantis: http://www.cs.indiana.edu/~bhimebau/f3lib/html/group__PWR__Group4.html#ga00ddae00a9c327b81b24d2597b0052f3

	/* Clear Wakeup flag */
	PWR->CR |= PWR_CR_CWUF;

	/* Select STANDBY mode */
	PWR->CR |= PWR_CR_PDDS;

	/* Set SLEEPDEEP bit of Cortex System Control Register */
	SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

	/* Request Wait For Interrupt */
	__WFI();
}

bool save(enum saveFormat format){
	bool cardDetected = false;
	bool success = false;
	//uint8_t sector[512];  // dont't use sow many stack for it, instead use file1.buf memory buffer
	uint16_t counter;
	FATFS FatFs;
	FIL file1;
	FRESULT fr; /* FatFs return code */
	char caption[]  = "Save";
	char fname[14] ;
	SDCSTATE Flag;

	if(format != TSV && format !=CSV){
		return false;
	}

	/*
  __disable_irq();
	msg_custom("Saving...");
	if(MediaInitialize(&Flag) == sdcValid){
		cardDetected = true;
		SectorRead(0, sector);
		if(sector[0] == 0xAF && sector[1] == 0xAB){
			counter = (sector[2] << 8) | sector[3];
		}
		else{
			sector[0] = 0xAF;
			sector[1] = 0xAB;
			sector[2] = 0;
			sector[3] = 0;
			//Now lets make some fun then :) .
			{
				//uint8_t message[] = "Hi. If you want to receive the source code of the FTMC " \
								 "magnetic field meter for free write to afaustas@gmail.com . " \
								 "Keep in mind: it is quite messy but the firmware is open source :)";
        uint8_t message[] = "Hello";         
				strcpy(&sector[4], message);
			}
			counter = 0;
		}
		sprintf(fname, "MEA%.5u.%s", counter, (format == TSV) ? "TSV" : "CSV");

		fr = f_mount(&FatFs, "", 0);
		if(fr == FR_OK){
			if(f_open(&file1, fname, FA_CREATE_ALWAYS | FA_WRITE) == FR_OK){
				fr = drawGraph(format, &file1);
				f_close(&file1);
				success = true;
			}
			counter++;
			sector[2] = (counter >> 8);
			sector[3] = counter;
			SectorWrite(0, sector);
		}

		f_mount(NULL, "", 0);
	}

	__enable_irq();*/
   __disable_irq();
	msg_custom("Saving...");
	if(MediaInitialize(&Flag) == sdcValid){
		cardDetected = true;
		SectorRead(0, file1.buf);
		if(file1.buf[0] == 0xAF && file1.buf[1] == 0xAB){
			counter = (file1.buf[2] << 8) | file1.buf[3];
		}
		else
			counter = 0;
    
		sprintf(fname, "MEA%.5u.%s", counter, (format == TSV) ? "TSV" : "CSV");

		fr = f_mount(&FatFs, "", 0);
		if(fr == FR_OK){
			if(f_open(&file1, fname, FA_CREATE_ALWAYS | FA_WRITE) == FR_OK){
				fr = drawGraph(format, &file1);
				f_close(&file1);
				success = true;
			}
			counter++;
      
      //--- read modify zero sector
      SectorRead(0, file1.buf);
      if(file1.buf[0] != 0xAF && file1.buf[1] != 0xAB){                         // if id don't exist then create it
        file1.buf[0] = 0xAF;
        file1.buf[1] = 0xAB;
        file1.buf[2] = 0;
        file1.buf[3] = 0;
        counter = 0;
      }
			file1.buf[2] = (counter >> 8);
			file1.buf[3] = counter;
			SectorWrite(0, file1.buf);
		}
		f_mount(NULL, "", 0);
	}
	__enable_irq();
  
  
  

	if(!cardDetected){
		msgBox("Micro SD card not  found!", caption, CRITICAL, NULL);
	}
	else{
		switch(fr){
			case FR_OK: {
				char doneMsg[sizeof("Saved to: MEAxxxxx.??? ")]; //Only DOS (8.3) filenames supported
				snprintf(doneMsg, sizeof(doneMsg), "Saved to: %s", fname);
				msgBox(doneMsg, caption, 0, NULL);
				break;
			}
			case FR_DISK_ERR: msgBox("Disk error!", caption, CRITICAL, NULL); break;
			case FR_NOT_READY: msgBox("Not ready!", caption, CRITICAL, NULL); break;
			case FR_NO_FILESYSTEM: msgBox("Card not formatted or unsupported filesystem!", caption, CRITICAL, NULL); break;
			case FR_INT_ERR: msgBox("FATFS internal error!", caption, CRITICAL, NULL); break;
			case FR_DENIED: msgBox("Unable to write. Disk may be full.", caption, CRITICAL, NULL); break;
			case FR_TIMEOUT: msgBox("Unable to write. Card error (timeout).", caption, CRITICAL, NULL); break;
			default: msgBox("Unknown error occurred!", caption, CRITICAL, NULL);
		}
	}

	return success;
}

static char* uint64ToDecimal(uint64_t v){
	static char bfr[20+1];
	char* p = bfr + sizeof(bfr);
	bool first;
	*(--p) = '\0';
	for (first = true; v || first; first = false) {
		const uint32_t digit = v % 10;
		const char c = '0' + digit;
		*(--p) = c;
		v = v / 10;
	}
	return p;
}

//Clear ADC & temp buffers. Should be changed to memset().
void clearBuffers(){
	int i;
//	for(i = 0; i < ADC_MAX_SAMPLES; i++){
//		g_adcBuffer[i] = 0;
//	}
	ExtADC_clearBuffer();
	for(i = 0; i < TEMPERATURE_BUFFER_LENGTH; i++){
		g_temperatureBuffer[i] = 0;
	}
	g_status.isDataReceived = false;
}

#ifdef USE_FULL_ASSERT

/* =================================================================================
	Function   : assert_failed()
	Description: Reports the name of the source file and the source line number
                 where the assert_param error has occurred.
	Arguments  : file - pointer to the source file name
	             line - assert_param error line source number
	Return     : -
   --------------------------------------------------------------------------------- */

void assert_failed(uint8_t* file, uint32_t line)
{
	char str[300];
	__disable_irq();
	UG_FillScreen(C_RED);
	UG_ConsoleSetArea(0, 25, 479, 269);
	UG_ConsoleSetForecolor(C_YELLOW);
	UG_ConsoleSetBackcolor(C_RED);
	UG_FontSelect(&FONT_8X14);
	sprintf(str, "Assertion failed:\n\nFile: %s\nLine: %lu\r\n", file, line);
	UG_ConsolePutString(str);
	while(1);
}

#endif

/* END OF FILE */
