#include "hal_flash.h"

//Fixed according https://my.st.com/public/STe2ecommunities/mcu/Lists/cortex_mx_stm32/Flat.aspx?RootFolder=%2Fpublic%2FSTe2ecommunities%2Fmcu%2FLists%2Fcortex_mx_stm32%2FSTM32F105VC%20Flash%20programming%20problem&FolderCTID=0x01200200770978C69A1141439FE559EB459D7580009C4E14902C3CDE46A77F0FFD06506F5B&currentviews=248

void FLASH_unlock(){
	if(FLASH->CR & FLASH_CR_LOCK){ //If locked
	    /* Authorize the FLASH Registers access */
		//Unlock sequence: write 0x45670123 (KEY1), then write 0x45670123 (KEY2)
	    FLASH->KEYR = FLASH_KEY1;
	    FLASH->KEYR = FLASH_KEY2;
	}
}

void FLASH_lock(){
	FLASH->CR |= FLASH_CR_LOCK;
}

FLASH_Status FLASH_erasePage(uint32_t pageAddress){
	uint32_t attempts = ATTEMPTS;

	while((FLASH->SR & FLASH_SR_BSY) && (attempts--));
	if(!attempts) return FLASH_ERROR;

	/* Clear pending flags (if any) */
	FLASH->SR = (FLASH_SR_EOP | FLASH_SR_WRPERR | FLASH_SR_PGERR);

	/* Proceed to erase the page */
	FLASH->CR &= ~FLASH_CR_PG;
	FLASH->CR |= FLASH_CR_PER;
	FLASH->AR = pageAddress;
	FLASH->CR |= FLASH_CR_STRT;

	attempts = ATTEMPTS;
	while((FLASH->SR & FLASH_SR_BSY) && (attempts--));
	if(!attempts) return FLASH_ERROR;

	FLASH->CR &= ~FLASH_CR_STRT;
	FLASH->CR &= ~FLASH_CR_PER;

	return FLASH_COMPLETE;
}

FLASH_Status FLASH_programHalfWord(uint32_t address, uint16_t data){
	uint32_t attempts = ATTEMPTS;

	while((FLASH->SR & FLASH_SR_BSY) && (attempts--));
	if(!attempts) return FLASH_ERROR;

	/* Clear pending flags (if any) */
	FLASH->SR = FLASH_SR_EOP | FLASH_SR_WRPERR | FLASH_SR_PGERR;

	/* Proceed to program the new data */
	FLASH->AR = address;
	FLASH->CR |= FLASH_CR_PG;

	*(__IO uint16_t*)address = data;

	attempts = ATTEMPTS;
	while((FLASH->SR & FLASH_SR_BSY) && (attempts--));
	FLASH->CR  &=  ~FLASH_CR_PG;
	if(!attempts) return FLASH_ERROR;

	return FLASH_COMPLETE;
}
