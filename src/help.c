#include "help.h"

void help(){
	UG_DrawLine(0, 20, 479, 20, C_YELLOW);
	UG_FillFrame(0, 21, 479, 271, C_BLACK);

	//Header
	UG_SetBackcolor(0x000000);
	UG_SetForecolor(C_YELLOW);
	UG_FontSelect(&FONT_12X20);
	UG_PutString(480 / 2 - 12 * 7 / 2 - 12 / 2, 30, "HELP");

	//Body
	UG_ConsoleSetForecolor(C_WHITE);
	UG_ConsoleSetBackcolor(C_BLACK);
	UG_ConsoleSetArea(5, 60, 474, 271);
	UG_ConsoleClear();
	UG_FontSelect(&FONT_10X16);

	UG_ConsolePutString("This device has been designed for \n magnetic field measurements.\n");
	UG_ConsolePutString("\n \n \n \n ");
	UG_ConsoleSetForecolor(C_YELLOW);
	UG_FontSelect(&FONT_8X14);
	UG_ConsolePutString("\n \n \n \n \n SD support done according J. AXELSON code\n " \
						"FATFS library has been used for FS support\n " \
						"uGUI library has been used for graphics primitives");

	while(!STMPE_isPressed());
	while(STMPE_isPressed());
}
