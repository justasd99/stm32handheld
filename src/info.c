#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "info.h"

static void drawWindow();
static void putInformation(int16_t temperature, int adc_offset);
static bool isCloseButtonPressed(const uint16_t x, const uint16_t y, const bool isPressed);

void information(const int16_t *temperature, int adc_offset)
{
	const uint8_t counterLoad = 100;
	uint8_t counter = counterLoad;
	drawWindow();
  int T_last = 0;
	while(1){
    if(T_last != (*temperature / 10))
      putInformation(*temperature / 10, adc_offset);
    T_last = (*temperature / 10);
		while(counter--){
			delay_ms(5);
			if(isCloseButtonPressed(STMPE_getX0(), STMPE_getY0(), STMPE_isPressed())){
				return;
			}
		}
		counter = counterLoad;
	}
}

static void drawWindow(){
	UG_FillFrame(0, 20, 479, 60, 0x004667);
	UG_FillFrame(0, 61, 479, 271, C_BLACK);
	UG_DrawLine(0, 60, 479, 60, C_DARK_GRAY);
	UG_DrawLine(440, 20, 440, 60, C_DARK_GRAY);

	//Close icon ("X")
	UG_DrawLine(445, 25, 475, 55, C_YELLOW);
	UG_DrawLine(446, 25, 476, 55, C_YELLOW);
	UG_DrawLine(444, 25, 474, 55, C_YELLOW);
	UG_DrawLine(445, 55, 475, 25, C_YELLOW);
	UG_DrawLine(446, 55, 476, 25, C_YELLOW);
	UG_DrawLine(444, 55, 474, 25, C_YELLOW);

	//Name
	UG_SetBackcolor(0x004667);
	UG_SetForecolor(C_YELLOW);
	UG_FontSelect(&FONT_12X20);
	UG_PutString(40, 35, "Information");

	UG_ConsoleSetArea(3, 65, 477, 271);
}

static void putInformation(int16_t temperature, int adc_offset)
{
	int x;
	char temperatureStr[6];
//	UG_SetForecolor(C_WHITE);
//	UG_SetBackcolor(C_BLACK);
	UG_FontSelect(&FONT_12X16);

	UG_ConsoleSetForecolor(C_WHITE);
	UG_ConsoleSetBackcolor(C_BLACK);

	UG_ConsoleClear();

	if(calibration_sensorExists()){
//		if(!calibration_sensorValid()){
//			UG_ConsolePutString("This sensor is not supported or invalid.");
//		}
		//else
		{
			uint8_t serialNumber[16];
			char serialNumberStr[33];
			calibration_getSerialNumber(serialNumber);
			UG_ConsolePutString("Serial Number:\n   ");
			const char hex[] = "0123456789ABCDEF";
			for(x = 0; x < 16; x++){
				serialNumberStr[2 * x] = hex[serialNumber[x] >> 4];
				serialNumberStr[2 * x + 1] = hex[serialNumber[x] & 0x0F];
			}
			serialNumberStr[32] = '\0';
			UG_ConsolePutString(serialNumberStr);
			//UG_ConsolePutString("\n Manufacturing Date: ");
			//UG_ConsolePutString("\n Offset: ");
			UG_ConsolePutString("\n Temperature:");
			temperatureStr[0] = (temperature < 0) ? '-' : ' ';
			temperatureStr[1] = temperature / 100 + '0';
			temperatureStr[2] = temperature % 100 / 10 + '0';
			temperatureStr[3] = '.';
			temperatureStr[4] = temperature % 10 + '0';
			temperatureStr[5] = '\0';
			UG_ConsolePutString(temperatureStr);
			UG_ConsolePutString("\xF8\x43\n");
      
      sprintf(serialNumberStr, " External adc offset: %d\n", adc_offset );
      UG_ConsolePutString(serialNumberStr);
		}
	}
	else{
		UG_ConsolePutString("Sensor is not connected.");
	}

}

static bool isCloseButtonPressed(const uint16_t x, const uint16_t y, const bool isPressed){
	if(isPressed){
		if(y > 20 && y < 61 && x > 440)
			return true;
	}
	return false;
}
