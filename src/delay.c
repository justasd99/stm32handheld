#include "delay.h"

volatile uint32_t g_msTick = 0;

void delayInit(){
	RCC_SETUP;
	TIMER->PSC = PRESCALER; //Prescaler
#if COUNTER_REGISTER_SIZE == 32
	TIMER->ARR = 0x00FFFFFFUL; //Auto Reload Register
#elif COUNTER_REGISTER_SIZE == 16
	TIMER->ARR = 0xFFFFU; //Auto Reload Register
#endif
	TIMER->CR1 |= TIM_CR1_CEN;
}

void delayDeInit(){
	TIMER->CR1 &= ~TIM_CR1_CEN;
}

//P.S.: Kazkodel visiskai tikslaus velinimo gauti neiseina (?) jis truputi nestabilus. Gal perrasius asembleriu
//ar paikeitus optimizavimo parametrus situacija pasikeistu?
//TODO: Nu kolkas sueis, bet veliau reikes sutvarkyti :-) . Kazkodel nestabilu lyg, bet si funkcija svarbi tampa!!!
#if COUNTER_REGISTER_SIZE == 32
void delay_us(uint32_t delay){
	uint32_t start = TIMER->CNT;
	uint32_t stop = TIMER->CNT + delay + 1; //+1 because call could arrive just before CNT changes
	uint32_t now;
#elif COUNTER_REGISTER_SIZE == 16
void delay_us(uint16_t delay){
	uint16_t start = TIMER->CNT;
	uint16_t stop = TIMER->CNT + delay + 1; //+1 because call could arrive just before CNT changes
	uint16_t now;
#endif
	uint32_t pri_mask = __get_PRIMASK(); //=0 if interrupts are enabled
	__disable_irq();
	if(start < stop){
		do
			now = TIMER->CNT;
		while(now < stop && now >= start);
	}
	else{
		//if CNT will roll-over
		do
			now = TIMER->CNT;
		while(now >= start || now < stop);
	}
	if(!pri_mask){
		__enable_irq();
	}
}

//void delay_us(uint32_t delay){
//	uint32_t start = TIMER->CNT;
//	while(TIMER->CNT - start < delay);
//}

void increaseMilisecondTick(){
	g_msTick++;
}

uint32_t milisecondTick(){
	return g_msTick;
}

void delay_ms(uint32_t delay){
	//CHANGE WITH CARE, BECAUSE GCC MAY OPTIMIZE OUT!!! Perhaps try to use "volatile".
	uint32_t start = g_msTick;
	uint32_t now;
	while(1){
		//__WFI(); // Enter sleep mode, waiting for (at least) the SysTick interrupt.
		now = g_msTick;
		if(now - start >= delay){
			return;
		}
	}
}
