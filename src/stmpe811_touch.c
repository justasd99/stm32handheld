#include "stmpe811_touch.h"

static STMPE_STATUS g_STMPE_status;
static volatile uint16_t g_touchTime = 0;

/* =========================================================================
   Function : STMPE_Init()
   Description: Touch screen initialization
   ------------------------------------------------------------------------- */
void STMPE_Init(){
	const uint8_t address = 0x44;
	uint8_t buf[10];

	I2C_sendByte(I2C, address, 0x02, 1000);
	delay_ms(5);
	I2C_sendByte(I2C, address, 0x00, 1000);
	delay_ms(2);

	I2C_sendByte(I2C, address, 0x00, 1000); //Nustatyti, ar prijungtas (ID)
	I2C_receiveBytes(I2C, address, buf, 2, 1000);

	I2C_sendByte(I2C, address, 0x02, 1000);
	delay_ms(5);
	I2C_sendByte(I2C, address, 0x00, 1000);
	delay_ms(2);
	//--
	buf[0] = 0x04; //SYS_CTL_2
	buf[1] = 0x0C; // turn on both the touchscreen controller and ADC blocks
	I2C_sendBytes(I2C, address, buf, 2, 1000);

	buf[0] = 0x0A; //INT_EN
	buf[1] = 0x02; //0x07
	I2C_sendBytes(I2C, address, buf, 2, 1000);

	buf[0] = 0x20; //ADC_CTRL1
	buf[1] = 0x49;
	I2C_sendBytes(I2C, address, buf, 2, 1000);

	delay_ms(2);

	buf[0] = 0x21; //ADC_CTRL2
	buf[1] = 0x01;
	I2C_sendBytes(I2C, address, buf, 2, 1000);

	buf[0] = 0x17; //GPIO_AF
	buf[1] = 0x00;
	I2C_sendBytes(I2C, address, buf, 2, 1000);

	buf[0] = 0x41; //TSC_CFG
	buf[1] = 0x9A;
	I2C_sendBytes(I2C, address, buf, 2, 1000);

	buf[0] = 0x4A; //FIFO_TH
	buf[1] = 0x01; //Single point reading
	I2C_sendBytes(I2C, address, buf, 2, 1000);

	buf[0] = 0x4B; //FIFO_CTRL_STA
	buf[1] = 0x01; //Clear FIFO content
	I2C_sendBytes(I2C, address, buf, 2, 1000);

	buf[0] = 0x4B; //FIFO_CTRL_STA
	buf[1] = 0x00; //Put FIFO back into operational mode
	I2C_sendBytes(I2C, address, buf, 2, 1000);

	buf[0] = 0x56; //TSC_FRACT_Z
	buf[1] = 0x07; //set the data format for Z value
	I2C_sendBytes(I2C, address, buf, 2, 1000);

	buf[0] = 0x58; //TSC_I_DRIVE (to set the driving capability of the device for touchscreen controller pins)
	buf[1] = 0x01; //50 mA
	I2C_sendBytes(I2C, address, buf, 2, 1000);

	buf[0] = 0x40; //TSC_CTRL
	buf[1] = 0x03; //Set touchscreen operation mode and enable touchscreen 0x01
	I2C_sendBytes(I2C, address, buf, 2, 1000);

	buf[0] = 0x0B; //INT_STA
	buf[1] = 0xFF; //Clear all interrupt status
	I2C_sendBytes(I2C, address, buf, 2, 1000);

	buf[0] = 0x09; //INT_CTRL
	buf[1] = 0x01; //Enable the interrupt
	I2C_sendBytes(I2C, address, buf, 2, 1000);

	STMPE_SetDefaultCalibrationData();
}

void STMPE_SetDefaultCalibrationData(){
	g_STMPE_status.xDivider = STMPE_DEFAULT_X_DIVIDER;
	g_STMPE_status.yDivider = STMPE_DEFAULT_Y_DIVIDER;
	g_STMPE_status.xOffset = STMPE_DEFAULT_X_OFFSET;
	g_STMPE_status.yOffset = STMPE_DEFAULT_Y_OFFSET;
}

__attribute__( ( always_inline ) ) void STMPE_PressCheck(){
	uint8_t buf[100];
	const uint8_t address = 0x44;

	buf[0] = 0x40; //TSC_CTRL
	buf[1] = 0x02; //Set touchscreen operation mode and enable touchscreen <-- REIK ISJUNGTI DEL SI BUG'O (?). 2016-10-02: Jau manau, kad ateityje tiesiog si draiveri reiketu perrasyti pagal ST rekomendacija AN2807
	I2C_sendBytes(I2C, address, buf, 2, 1);

	uint8_t val[2];
	buf[0] = 0x4D; //TSC_DATA_X
	I2C_sendBytes(I2C, address, buf, 1, 1);
	I2C_receiveBytes(I2C, address, &val[1], 1, 1);
	buf[0] = 0x4E; //TSC_DATA_X
	I2C_sendBytes(I2C, address, buf, 1, 1);
	I2C_receiveBytes(I2C, address, val, 1, 1);
	uint16_t x = (val[1] << 8) | val[0];

	buf[0] = 0x4F; //TSC_DATA_Y
	I2C_sendBytes(I2C, address, buf, 1, 1);
	I2C_receiveBytes(I2C, address, &val[1], 1, 1);
	buf[0] = 0x50; //TSC_DATA_Y
	I2C_sendBytes(I2C, address, buf, 1, 1);
	I2C_receiveBytes(I2C, address, val, 1, 1);
	uint16_t y = (val[1] << 8) | val[0];

	if(x > 0 && y > 0){ //if x and y the both are equal to zero then there is no information in the buffer. Do not calculate.
		x = (int16_t)((x << 4) / g_STMPE_status.xDivider) + g_STMPE_status.xOffset;
		y = (int16_t)((y << 4) / g_STMPE_status.yDivider) + g_STMPE_status.yOffset;
	}

	if(!g_STMPE_status.isPressed){
		g_STMPE_status.x0 = x;
		g_STMPE_status.y0 = y;

		g_STMPE_status.isPressed = true;
	}
	else{
		g_STMPE_status.x1 = x;
		g_STMPE_status.y1 = y;
	}
	//reset FIFO
	buf[0] = 0x4B; //FIFO_CTRL_STA
	buf[1] = 0x00; //Clear FIFO content
	I2C_sendBytes(I2C, address, buf, 2, 1);

	buf[0] = 0x4B; //FIFO_CTRL_STA
	buf[1] = 0x01; //Put FIFO back into operational mode
	I2C_sendBytes(I2C, address, buf, 2, 1);

	buf[0] = 0x40; //TSC_CTRL
	buf[1] = 0x03; //Set touchscreen operation mode and enable touchscreen <-- REENABLE DEL SI BUG'O (?)
	I2C_sendBytes(I2C, address, buf, 2, 1);

	buf[0] = 0x0B; //INT_STA
	buf[1] = 0xFF; //Clear all interrupt status
	I2C_sendBytes(I2C, address, buf, 2, 1);

	g_touchTime = 10;
}

__attribute__( ( always_inline ) ) void STMPE_ReleaseCheck(){
	g_touchTime--;
	if(g_touchTime == 0){
		g_STMPE_status.isPressed = false;
		//g_STMPE_status.x1 =
	}
}

__attribute__( ( always_inline ) ) uint16_t STMPE_getX0(){
	return g_STMPE_status.x0;
}

__attribute__( ( always_inline ) ) uint16_t STMPE_getY0(){
	return g_STMPE_status.y0;
}

__attribute__( ( always_inline ) ) uint16_t STMPE_getX(){
	return g_STMPE_status.x1;
}

__attribute__( ( always_inline ) ) uint16_t STMPE_getY(){
	return g_STMPE_status.y1;
}

__attribute__( ( always_inline ) ) void STMPE_resetX0(){
	g_STMPE_status.x0 = g_STMPE_status.x1;
}

__attribute__( ( always_inline ) ) void STMPE_resetY0(){
	g_STMPE_status.y0 = g_STMPE_status.y1;
}

__attribute__( ( always_inline ) ) bool STMPE_isPressed(){
	return g_STMPE_status.isPressed;
}

__attribute__( ( always_inline ) ) void STMPE_forceUnpress(){
	g_STMPE_status.isPressed = false;
}

__attribute__( ( always_inline ) ) void STMPE_forceSetX0(uint16_t x){
	g_STMPE_status.x0 = x;
}

__attribute__( ( always_inline ) ) void STMPE_forceSetY0(uint16_t y){
	g_STMPE_status.y0 = y;
}


__attribute__( ( always_inline ) ) void STMPE_setOffsetX(int16_t x){
	g_STMPE_status.xOffset = x;
}

__attribute__( ( always_inline ) ) void STMPE_setOffsetY(int16_t y){
	g_STMPE_status.yOffset = y;
}

__attribute__( ( always_inline ) ) void STMPE_setDividerX(uint16_t x){
	g_STMPE_status.xDivider = x;
}

__attribute__( ( always_inline ) ) void STMPE_setDividerY(uint16_t y){
	g_STMPE_status.yDivider = y;
}
