#include "dialog.h"

static DLG_OBJECT *_getFreeObject(DIALOG *dlg);
static DLG_OBJECT* _searchObject(DIALOG *dlg, uint8_t type, uint8_t id);
static void _updateObjects(DIALOG *dlg);
static void _handleEvents(DIALOG* dlg);
static void _updateDialog(DIALOG *dlg);
static void _updateButton(DIALOG *dlg, DLG_OBJECT *obj);
static void _updateSpinBox(DIALOG *dlg, DLG_OBJECT *obj);
static void _processTouchData(DIALOG *dlg);
static void _updateStandardIcon(DIALOG *dlg, DLG_OBJECT *obj);
static void _updateLabel(DIALOG *dlg, DLG_OBJECT *obj);
static void _updateCheckBox(DIALOG *dlg, DLG_OBJECT *obj);
static void _updateRadioButtonList(DIALOG *dlg, DLG_OBJECT *obj);
//static void _cbMsgBox(DIALOG *dlg, DLG_MESSAGE *msg);

void reverse(char s[], int length)
 {
     int i, j;
     char c;
 
     for (i = 0, j = length-1; i<j; i++, j--) {
         c = s[i];
         s[i] = s[j];
         s[j] = c;
     }
 }
 
// Implementation of itoa()
char* itoa(int num, char* str, int base)
{
    int i = 0;
    bool isNegative = false;
 
    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
    if (num == 0)
    {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }
 
    // In standard itoa(), negative numbers are handled only with 
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10)
    {
        isNegative = true;
        num = -num;
    }
 
    // Process individual digits
    while (num != 0)
    {
        int rem = num % base;
        str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
        num = num/base;
    }
 
    // If number is negative, append '-'
    if (isNegative)
        str[i++] = '-';
 
    str[i] = '\0'; // Append string terminator
 
    // Reverse the string
    reverse(str, i);
 
    return str;
}

DLG_OBJECT *getDialogObjectById(DIALOG *dlg, uint8_t id){
	uint8_t i;
	DLG_OBJECT *obj;
	for(i = 0; i < dlg->objCount; i++){
		obj = &dlg->objList[i];
		if(obj->id == id){
			return obj;
		}
	}
	return NULL;
}

DLG_OBJECT *_getFreeObject(DIALOG *dlg){
	uint8_t i;
	DLG_OBJECT *obj;
	for(i = 0; i < dlg->objCount; i++){
		obj = &dlg->objList[i];
		if(obj->isFree){
			return obj;
		}
	}
	return NULL;
}

DLG_OBJECT* _searchObject(DIALOG *dlg, uint8_t type, uint8_t id){
   uint8_t i;
   DLG_OBJECT* obj= dlg->objList;

   for(i = 0; i < dlg->objCount; i++){
      obj = (DLG_OBJECT *)(&dlg->objList[i]);
      if (!obj->isFree){
         if ( (obj->type == type) && (obj->id == id) ){
            /* Requested object found! */
            return obj;
         }
      }
   }
   return NULL;
}

BOOL createDialog(DIALOG *dlg, DLG_OBJECT *objList, uint8_t objCount, void (*callback)(DIALOG *, DLG_MESSAGE*) /*, DLG_TS_STATUS (*touchScreenStatusF)()*/){
	uint8_t i;
	DLG_OBJECT *object;
	if((dlg == NULL) || (objList == NULL) || (objCount == 0))
		return FALSE;
	for(i = 0; i < objCount; i++){
		object = &objList[i];
		//object->state = OBJ_STATE_INIT;
		object->isFree = TRUE;
		object->isPressed = FALSE;
		object->isTouchChanged = FALSE;
		object->needUpdating = FALSE; //TRUE?
		object->isDrawn = FALSE;
		object->data = NULL;
	}
	dlg->objCount = objCount;
	dlg->objList = objList;
	dlg->width = 300;
	dlg->height = 100;
	dlg->callback = callback;
	//dlg->getTouchScreenStatus = touchScreenStatusF;
	dlg->title = NULL;
	dlg->needUpdate = TRUE;
	return TRUE;
}

//void showDialog(DIALOG *dlg){
//
//}

void redrawDialog(DIALOG *dlg){
	_updateDialog(dlg);
}

void _updateDialog(DIALOG *dlg){
	uint8_t i;
	uint8_t objCount;
	int16_t xs, ys, xe, ye, xc, yc;
	DLG_OBJECT *obj;
	xc = UG_GetXDim() / 2;
	yc = UG_GetYDim() / 2;
	xs = xc - dlg->width / 2;
	xe = xc + dlg->width / 2;
	ys = yc - (dlg->height + TITLE_HEIGHT + BUTTON_AREA_HEIGHT) / 2;
	ye = yc + (dlg->height + TITLE_HEIGHT + BUTTON_AREA_HEIGHT) / 2;
	//dlg->state &= ~WND_STATE_UPDATE; //?
	UG_FillFrame(xs, ys, xe, ys + TITLE_HEIGHT, 0x004667); //Top (title)
	UG_FillFrame(xs, ys + TITLE_HEIGHT, xe, ye - BUTTON_AREA_HEIGHT, 0x001637); //Vidurys
	UG_FillFrame(xs, ye - BUTTON_AREA_HEIGHT , xe, ye, 0xCCCCCC); //Apacia
	UG_DrawFrame(xs, ys, xe, ye, 0xEEEEEE);
	UG_FillFrame(xe + 1, ys + 5, xe + 6, ye + 6, 0x0000AA); //TODO: Visas spalvas aprasyti define'ais, pvz DIALOG_SHADOW_COLOR
	UG_FillFrame(xs + 5, ye + 1, xe + 5, ye + 6, 0x0000AA);
	if(dlg->title != NULL){
		UG_FontSelect(&FONT_12X16);
		UG_SetForecolor(C_YELLOW);
		UG_SetBackcolor(0x004667);
		UG_PutString(xs + 5, ys + 7, dlg->title);
	}

	objCount = dlg->objCount;
	for(i = 0; i < objCount; i++){
		obj = &dlg->objList[i];
		if (!obj->isFree)
			obj->needUpdating = TRUE;
	}
	dlg->needUpdate = FALSE;
	dlg->dialogState = DLG_STATE_SHOWN;
}

void setDialogTitleText(DIALOG *dlg, char *title){
	dlg->title = title;
}

void setDialogSize(DIALOG *dlg, uint16_t width, uint16_t height){
	dlg->width = width;
	dlg->height = height;
}

void _updateButton(DIALOG *dlg, DLG_OBJECT *obj){
	DLG_BUTTON *button;
	DLG_AREA area;
	button =  (DLG_BUTTON *)(obj->data);
	//Touch
	if(obj->isTouchChanged){
//		/* Handle 'click' event */
//		if ( obj->touch_state & OBJ_TOUCH_STATE_CLICK_ON_OBJECT ){
//			obj->event = BTN_EVENT_CLICKED;
//			obj->state |= OBJ_STATE_UPDATE;
//		}
//		/* Is the button pressed down? */
//		if ( obj->touch_state & OBJ_TOUCH_STATE_PRESSED_ON_OBJECT ){
//			button->state |= BTN_STATE_PRESSED;
//			obj->state |= OBJ_STATE_UPDATE;
//		}
//		/* Can we release the button? */
//		else if ( button->state & BTN_STATE_PRESSED ){
//			button->state &= ~BTN_STATE_PRESSED;
//			obj->state |= OBJ_STATE_UPDATE;
//		}
		if (obj->isPressed && obj->isTouchChanged){
			button->isPushed = TRUE;
			obj->needUpdating = TRUE;
		}
		if (!obj->isPressed && obj->isTouchChanged){
			button->isPushed = FALSE;
			obj->needUpdating = TRUE;
			obj->event = BTN_EVENT_CLICKED;

		}
		obj->isTouchChanged = FALSE;
	}
	//Update
	if ( obj->needUpdating){
		/* Full redraw necessary? */
		//if ( (obj->state & OBJ_STATE_REDRAW) || (button->state & BTN_STATE_ALWAYS_REDRAW) ){
			int16_t xs, ys, xc, yc, xe, ye, buttonWidth, buttonHeight;
			getDialogArea(dlg, &area);
            obj->a_absolute.xs = obj->a_relative.xs + area.xs;
            obj->a_absolute.ys = obj->a_relative.ys + area.ys;
            obj->a_absolute.xe = obj->a_relative.xe + area.xs;
            obj->a_absolute.ye = obj->a_relative.ye + area.ys;

        	xc = UG_GetXDim() / 2;
        	yc = UG_GetYDim() / 2;
        	xe = xc + dlg->width / 2;
        	ye = yc + (dlg->height + TITLE_HEIGHT + BUTTON_AREA_HEIGHT) / 2;
            if (obj->a_absolute.ye >= ye) return;
            if (obj->a_absolute.xe >= xe) return;

            if(button->isPushed){
            	UG_DrawFrame(obj->a_absolute.xs, obj->a_absolute.ys, obj->a_absolute.xe, obj->a_absolute.ye, C_BLACK);
            	UG_FillFrame(obj->a_absolute.xs + 1, obj->a_absolute.ys + 1, obj->a_absolute.xe - 1, obj->a_absolute.ye - 1, 0x004667);
            	UG_SetForecolor(C_YELLOW);
            	UG_SetBackcolor(0x004667);
            }
            else{
            	UG_DrawFrame(obj->a_absolute.xs, obj->a_absolute.ys, obj->a_absolute.xe, obj->a_absolute.ye, C_GRAY);
            	UG_FillFrame(obj->a_absolute.xs + 1, obj->a_absolute.ys + 1, obj->a_absolute.xe - 1, obj->a_absolute.ye - 1, 0xEEEEEE);
            	UG_SetForecolor(C_BLACK);
				UG_SetBackcolor(0xEEEEEE);
            }

            buttonWidth = obj->a_relative.xe - obj->a_relative.xs;
            buttonHeight = obj->a_relative.ye - obj->a_relative.ys;
            if(button->caption != NULL){
				xs = obj->a_absolute.xs + buttonWidth / 2 - strlen(button->caption) * 12 / 2;
				ys = obj->a_absolute.ys + buttonHeight / 2 - 16 / 2;
				UG_FontSelect(&FONT_12X16);
				UG_PutString(xs, ys, button->caption);
            }
            //obj->state &= ~OBJ_STATE_REDRAW; //!!
		}
		obj->needUpdating = FALSE;
//	}
}

void _updateObjects(DIALOG *dlg){
   int16_t i,objCount;
   DLG_OBJECT* obj;
//   uint8_t objState;
//   uint8_t objTouch;

   /* Check each object, if it needs to be updated? */
   objCount = dlg->objCount;
   for(i = 0; i < objCount; i++)
   {
      obj = &dlg->objList[i];
      //objState = obj->state;
      //objTouch = obj->touch_state;
//TODO: Perziuret sia if'u kruva. Ko nereik?
      if (!obj->isFree)
      {
         if (obj->needUpdating)
         {
            obj->update(dlg, obj);
         }
       //  if ( (objState & OBJ_STATE_VISIBLE) && (objState & OBJ_STATE_TOUCH_ENABLE) )
         {
            if (obj->isTouchChanged | obj->isPressed)
            {
               obj->update(dlg, obj);
            }
         }
      }
   }
}

BOOL createButtonsInButtonBox(DIALOG *dlg, uint8_t buttonCount, DLG_BUTTON *buttons, uint8_t *ids, char **captions){
	//DLG_OBJECT *obj;
	uint8_t currentButton;
	uint16_t buttonYOffset = dlg->height + TITLE_HEIGHT + 5;
	uint16_t buttonPlaceWidth = dlg->width / buttonCount;
	int16_t xs, xe;
	if(buttonCount == 0 || buttonCount > 3 || captions == NULL)
		return FALSE;
	for(currentButton = 0; currentButton < buttonCount; currentButton++){
		buttons[currentButton].caption = NULL;
		buttons[currentButton].isActive = TRUE;
		buttons[currentButton].isPushed = FALSE;
		xs = buttonPlaceWidth * currentButton + 5;
		xe = buttonPlaceWidth * (currentButton + 1) - 5;
		createButton(dlg, &buttons[currentButton], ids[currentButton], xs, buttonYOffset, xe, dlg->height + TITLE_HEIGHT + BUTTON_AREA_HEIGHT - 5);
		setButtonCaption(dlg, ids[currentButton], captions[currentButton]);
	}

	return TRUE;
}

BOOL createButton(DIALOG *dlg, DLG_BUTTON *button, uint8_t id, uint16_t xs, uint16_t ys, uint16_t xe, uint16_t ye){
	DLG_OBJECT *obj;
	//uint8_t currentButton;
	obj = _getFreeObject(dlg);
	if(obj == NULL)
		return FALSE;

	button->caption = NULL;
	button->isActive = TRUE;
	button->isPushed = FALSE;

	obj->update = _updateButton;
	obj->type = OBJ_TYPE_BUTTON;
	obj->event = OBJ_EVENT_NONE;
	obj->needUpdating = TRUE;
	obj->isTouchChanged = FALSE;
	obj->isPressed = FALSE;
	obj->isDrawn = TRUE; //Not required
	obj->data = button;
	obj->id = id;
	obj->a_relative.xs = xs;
	obj->a_relative.ys = ys;
	obj->a_relative.xe = xe;
	obj->a_relative.ye = ye;
	obj->a_absolute.xs = -1;
	obj->a_absolute.ys = -1;
	obj->a_absolute.xe = -1;
	obj->a_absolute.ye = -1;
	obj->isFree = FALSE;

	return TRUE;
}

BOOL setButtonCaption(DIALOG *dlg, uint8_t id, char *caption){
	DLG_OBJECT* obj=NULL;
	DLG_BUTTON* button=NULL;

	obj = _searchObject(dlg, OBJ_TYPE_BUTTON, id);
	if (obj == NULL) return UG_RESULT_FAIL;

	button = (DLG_BUTTON*)(obj->data);
	button->caption = caption;
	obj->needUpdating = TRUE;

	return TRUE;
}

BOOL createStandardIcon(DIALOG *dlg, DLG_STANDARD_ICON *standardIcon, uint8_t id, uint16_t x, uint16_t y){
	DLG_OBJECT *obj;
	obj = _getFreeObject(dlg);
	if(obj == NULL)
		return FALSE;
	obj->update = _updateStandardIcon;
	obj->type = OBJ_TYPE_STANDARD_ICON;
	obj->event = OBJ_EVENT_NONE;
	obj->needUpdating = TRUE;
	obj->isTouchChanged = FALSE;
	obj->isPressed = FALSE;
	obj->data = standardIcon;
	obj->id = id;
	obj->a_relative.xs = x;
	obj->a_relative.ys = y;
	obj->a_relative.xe = x + 64;
	obj->a_relative.ye = y + 64;
	obj->a_absolute.xs = -1;
	obj->a_absolute.ys = -1;
	obj->a_absolute.xe = -1;
	obj->a_absolute.ye = -1;
	obj->isFree = FALSE;

	return TRUE;
}

void _updateStandardIcon(DIALOG *dlg, DLG_OBJECT *obj){
	DLG_STANDARD_ICON *standardIcon;
	DLG_AREA area;
	standardIcon =  (DLG_STANDARD_ICON *)(obj->data);

	//Update
	if ( obj->needUpdating){
		int16_t /*xs, ys,*/ xc, yc, xe, ye; // buttonWidth, buttonHeight;
		getDialogArea(dlg, &area);
		obj->a_absolute.xs = obj->a_relative.xs + area.xs;
		obj->a_absolute.ys = obj->a_relative.ys + area.ys;
		obj->a_absolute.xe = obj->a_relative.xe + area.xs;
		obj->a_absolute.ye = obj->a_relative.ye + area.ys;
		xc = UG_GetXDim() / 2;
		yc = UG_GetYDim() / 2;
		xe = xc + dlg->width / 2;
		ye = yc + (dlg->height + TITLE_HEIGHT + BUTTON_AREA_HEIGHT) / 2;
		if (obj->a_absolute.ye >= ye) return; //todo: OR *s < a_absolute start
		if (obj->a_absolute.xe >= xe) return;
		switch(*standardIcon){
			case CRITICAL:
				UG_FillCircle(obj->a_absolute.xs + 32, obj->a_absolute.ys + 32, 32, C_RED);
				UG_FillFrame(obj->a_absolute.xs + 10, obj->a_absolute.ys + 24, obj->a_absolute.xs + 54, obj->a_absolute.ys + 40, C_WHITE);
				break;
			case WARNING:
				fillTriangle(obj->a_absolute.xs + 32, obj->a_absolute.ys, obj->a_absolute.xs + 64, obj->a_absolute.ys + 64, obj->a_absolute.xs, obj->a_absolute.ys + 64, C_YELLOW);
				UG_FillFrame(obj->a_absolute.xs + 30, obj->a_absolute.ys + 15, obj->a_absolute.xs + 35, obj->a_absolute.ys + 45, C_BLACK);
				UG_FillFrame(obj->a_absolute.xs + 30, obj->a_absolute.ys + 50, obj->a_absolute.xs + 35, obj->a_absolute.ys + 55, C_BLACK);
				break;
		}
		obj->needUpdating = FALSE;
		obj->isDrawn = TRUE;
	}
}

BOOL createLabel(DIALOG *dlg, DLG_LABEL *label, uint8_t id, uint16_t x, uint16_t y, uint16_t width, uint16_t height){
	DLG_OBJECT *obj;
	obj = _getFreeObject(dlg);
	if(obj == NULL)
		return FALSE;
	label->font = NULL;
	label->text = NULL;
	label->textColor = C_WHITE;
	obj->update = _updateLabel;
	obj->type = OBJ_TYPE_LABEL;
	obj->event = OBJ_EVENT_NONE;
	obj->needUpdating = TRUE;
	obj->isTouchChanged = FALSE;
	obj->isPressed = FALSE;
	obj->data = label;
	obj->id = id;
	obj->a_relative.xs = x;
	obj->a_relative.ys = y;
	obj->a_relative.xe = x + width;
	obj->a_relative.ye = y + height;
	obj->a_absolute.xs = -1;
	obj->a_absolute.ys = -1;
	obj->a_absolute.xe = -1;
	obj->a_absolute.ye = -1;
	obj->isFree = FALSE;

	return TRUE;
}

void _updateLabel(DIALOG *dlg, DLG_OBJECT *obj){
	DLG_LABEL *label;
	DLG_AREA area;
	label =  (DLG_LABEL *)(obj->data);

	//Update
	if ( obj->needUpdating){
		int16_t /*xs, ys, */xc, yc, xe, ye, buttonWidth /*, buttonHeight*/ ;
		getDialogArea(dlg, &area);
		obj->a_absolute.xs = obj->a_relative.xs + area.xs;
		obj->a_absolute.ys = obj->a_relative.ys + area.ys;
		obj->a_absolute.xe = obj->a_relative.xe + area.xs;
		obj->a_absolute.ye = obj->a_relative.ye + area.ys;

		xc = UG_GetXDim() / 2;
		yc = UG_GetYDim() / 2;
		xe = xc + dlg->width / 2;
		ye = yc + (dlg->height + TITLE_HEIGHT + BUTTON_AREA_HEIGHT) / 2;
		if (obj->a_absolute.ye >= ye) return; //todo: OR *s < a_absolute start
		if (obj->a_absolute.xe >= xe) return;

		if(label->font != NULL && label->text != NULL){
			UG_FontSelect(label->font);
			if(obj->a_relative.xs == obj->a_relative.xe && obj->a_relative.ys == obj->a_relative.ye){
				UG_SetForecolor(label->textColor);
				UG_SetBackcolor(0x001637);
				UG_PutString(obj->a_absolute.xs, obj->a_absolute.ys, label->text);
			}
			else{
				UG_ConsoleSetForecolor(label->textColor);
				UG_ConsoleSetBackcolor(0x001637);
				UG_ConsoleSetArea(obj->a_absolute.xs, obj->a_absolute.ys, obj->a_absolute.xe, obj->a_absolute.ye);
				UG_ConsoleClear();
				UG_ConsolePutString(label->text);
			}
		}
		obj->needUpdating = FALSE;
		obj->isDrawn = TRUE;
	}
}
//
void setLabelText(DLG_LABEL *label, char *text, uint32_t textColor, const UG_FONT *font){
	label->text = text;
	label->textColor = textColor;
	label->font = font;
}

BOOL createSpinBox(DIALOG *dlg, DLG_SPINBOX *spinBox, uint8_t id, uint16_t x, uint16_t y){
	DLG_OBJECT *obj;
	obj = _getFreeObject(dlg);
	if(obj == NULL)
		return FALSE;

	spinBox->isActive = TRUE;
	spinBox->textValues = NULL;
	spinBox->valueMin = 0;
	spinBox->valueCurrent = 50;
	spinBox->valueMax = 100;

	obj->update = _updateSpinBox;
	obj->type = OBJ_TYPE_SPINBOX;
	obj->event = OBJ_EVENT_NONE;
	obj->needUpdating = TRUE;
	obj->isTouchChanged = FALSE;
	obj->isPressed = FALSE;
	obj->isDrawn = TRUE; //Not required
	obj->data = spinBox;
	obj->id = id;
	obj->a_relative.xs = x;
	obj->a_relative.ys = y;
	obj->a_relative.xe = x + 64;
	obj->a_relative.ye = y + 92;
	obj->a_absolute.xs = -1;
	obj->a_absolute.ys = -1;
	obj->a_absolute.xe = -1;
	obj->a_absolute.ye = -1;
	obj->isFree = FALSE;

	return TRUE;
}

void _updateSpinBox(DIALOG *dlg, DLG_OBJECT *obj){
	DLG_SPINBOX *spinbox;
	DLG_AREA area;
	spinbox =  (DLG_SPINBOX *)(obj->data);
	//Touch
	//if(obj->isTouchChanged){
		if (obj->isPressed){// && obj->isTouchChanged){
			if(obj->touch_y < 30){
				if(spinbox->valueCurrent == spinbox->valueMax)
					spinbox->valueCurrent = spinbox->valueMin;
				else
					spinbox->valueCurrent++;
				spinbox->isPushedUp = TRUE;
				spinbox->isPushedDown = FALSE;
			}
			else if(obj->touch_y > 60){
				if(spinbox->valueCurrent == spinbox->valueMin)
					spinbox->valueCurrent = spinbox->valueMax;
				else
					spinbox->valueCurrent--;
				spinbox->isPushedUp = FALSE;
				spinbox->isPushedDown = TRUE;
			}
			obj->needUpdating = TRUE;
		}
		else{
			spinbox->isPushedUp = FALSE;
			spinbox->isPushedDown = FALSE;
		}
		if (!obj->isPressed && obj->isTouchChanged){
			obj->needUpdating = TRUE;
			obj->event = SPBOX_EVENT_VALUE_CHANGED;
		}
		obj->isTouchChanged = FALSE;
	//}
	//Update
	if ( obj->needUpdating){
		int16_t xs, ys; //, xc, yc, xe, ye;
		char valueStr[7];
		getDialogArea(dlg, &area);
		obj->a_absolute.xs = obj->a_relative.xs + area.xs;
		obj->a_absolute.ys = obj->a_relative.ys + area.ys;
		obj->a_absolute.xe = obj->a_relative.xe + area.xs;
		obj->a_absolute.ye = obj->a_relative.ye + area.ys;

//		xc = UG_GetXDim() / 2;
//		yc = UG_GetYDim() / 2;
//		xe = xc + dlg->width / 2;
//		ye = yc + (dlg->height + TITLE_HEIGHT + BUTTON_AREA_HEIGHT) / 2;
//		if (obj->a_absolute.ye >= ye) return;
//		if (obj->a_absolute.xe >= xe) return;

		xs = obj->a_absolute.xs;
		ys = obj->a_absolute.ys; // + TITLE_HEIGHT;

		UG_FillFrame(xs, ys + 32, xs + 64, ys + 30 + 26, 0x001637);
	  	UG_SetForecolor(C_WHITE);
		UG_SetBackcolor(0x001637);
		UG_FontSelect(&FONT_16X26);
		if(spinbox->textValues == NULL){
			itoa(spinbox->valueCurrent, valueStr, 10);
			UG_PutString(xs + 32 - (strlen(valueStr)*16/2), ys + 34, valueStr); //x_start = center - width / 2
		}
		else
			UG_PutString(xs + 32 - (strlen(spinbox->textValues[spinbox->valueCurrent])*16/2), ys + 34, spinbox->textValues[spinbox->valueCurrent]); //x_start = center - width / 2

		if(spinbox->isPushedUp){
			UG_FillRoundFrame(xs, ys, xs + 64, ys + 31, 5, 0x333333);
			UG_DrawLine(xs + 32, ys + 0, xs + 32 + 17, ys + 30, C_YELLOW);
			UG_DrawLine(xs + 32 - 17, ys + 30, xs + 32 + 17, ys + 30, C_YELLOW);
			UG_DrawLine(xs + 32 - 17, ys + 30, xs + 32, ys, C_YELLOW);
		}
		else if(spinbox->isPushedDown){
			UG_FillRoundFrame(xs, ys + 30 + 29, xs + 64, ys + 30 + 29 + 31, 5, 0x333333);
			UG_DrawLine(xs + 32 - 17, ys + 30 + 30, xs + 32 + 17, ys + 30 + 30, C_YELLOW);
			UG_DrawLine(xs + 32 - 17, ys + 30 + 30, xs + 32, ys + 30 + 30 + 30, C_YELLOW);
			UG_DrawLine(xs + 32 + 17, ys + 30 + 30, xs + 32, ys + 30 + 30 + 30, C_YELLOW);
		}
		else{
			UG_FillFrame(xs, ys, xs + 64, ys + 31, 0x001637);
			UG_DrawLine(xs + 32, ys + 0, xs + 32 + 17, ys + 30, C_GRAY);
			UG_DrawLine(xs + 32 - 17, ys + 30, xs + 32 + 17, ys + 30, C_GRAY);
			UG_DrawLine(xs + 32 - 17, ys + 30, xs + 32, ys, 0xDDDDDD);
			UG_FillFrame(xs, ys + 30 + 29, xs + 64, ys + 30 + 29 + 31, 0x001637);
			UG_DrawLine(xs + 32 - 17, ys + 30 + 30, xs + 32 + 17, ys + 30 + 30, 0xDDDDDD);
			UG_DrawLine(xs + 32 - 17, ys + 30 + 30, xs + 32, ys + 30 + 30 + 30, C_GRAY);
			UG_DrawLine(xs + 32 + 17, ys + 30 + 30, xs + 32, ys + 30 + 30 + 30, C_GRAY);
		}
		if(!obj->isTouchChanged && obj->isPressed)
			delay_ms(100);
	}
	obj->needUpdating = FALSE;
}

void setSpinBoxInterval(DLG_SPINBOX *spinBox, uint16_t min, uint16_t max){
	spinBox->valueMin = min;
	spinBox->valueMax = max;
	spinBox->valueCurrent = min;
}

void setSpinBoxValue(DLG_SPINBOX *spinBox, uint16_t value){
	spinBox->valueCurrent = value;
}

void setSpinBoxTextItems(DLG_SPINBOX *spinBox, char **items, uint8_t itemsCount){
	spinBox->textValues = items;
	spinBox->valueMin = 0;
	spinBox->valueMax = itemsCount - 1;
	spinBox->valueCurrent = 0;
}

void getDialogArea(DIALOG *dlg, DLG_AREA *area){
	int16_t xc, yc;
	xc = UG_GetXDim() / 2;
	yc = UG_GetYDim() / 2;
	area->xs = xc - dlg->width / 2;
	area->xe = xc + dlg->width / 2;
	area->ys = yc - (dlg->height + TITLE_HEIGHT + BUTTON_AREA_HEIGHT) / 2;
	area->ye = yc + (dlg->height + TITLE_HEIGHT + BUTTON_AREA_HEIGHT) / 2;
}

void _handleEvents(DIALOG* dlg){
   uint16_t i, objCount;
   DLG_OBJECT* obj;
   //uint8_t objstate;
   static DLG_MESSAGE msg;
   msg.src = NULL;

   /* Handle object-related events */
   msg.type = MSG_TYPE_OBJECT;
   objCount = dlg->objCount;
   for(i = 0; i < objCount; i++)
   {
      obj = &dlg->objList[i];
      //objstate = obj->state;
      if (!obj->isFree)
      {
         if ( obj->event != OBJ_EVENT_NONE )
         {
            msg.src = &obj;
            msg.id = obj->type;
            msg.sub_id = obj->id;

            dlg->callback(dlg, &msg);

            obj->event = OBJ_EVENT_NONE;
         }
      }
   }
}

void updateDialog(DIALOG *dlg){
	if(dlg->needUpdate)
		_updateDialog(dlg);
	_processTouchData(dlg);
	_updateObjects(dlg);
	_handleEvents(dlg);

}

void _processTouchData(DIALOG *dlg){
	uint16_t i, objCount;
	uint16_t xp, yp;
	DLG_OBJECT *obj;
	DLG_TS_STATUS tsStatus;
//	uint8_t objState;
//	uint8_t objTouch;

	//tsStatus = dlg->getTouchScreenStatus();
	tsStatus.x = STMPE_getX0();
	tsStatus.y = STMPE_getY0();
	tsStatus.isPressed = STMPE_isPressed();

	xp = tsStatus.x;
	yp = tsStatus.y;
	objCount = dlg->objCount;
	for(i = 0; i < objCount; i++){
		obj = &dlg->objList[i];
		//objState = obj->state;
		//objTouch = obj->touch_state;
		if (!obj->isFree && !obj->needUpdating){
			/* Process touch data */
			if(tsStatus.isPressed){
				if(xp >= obj->a_absolute.xs && xp <= obj->a_absolute.xe){
					if(yp >= obj->a_absolute.ys && yp <= obj->a_absolute.ye){
						if(!obj->isPressed){
							obj->isPressed = TRUE;
							obj->isTouchChanged = TRUE;
							obj->touch_x = xp - obj->a_absolute.xs;
							obj->touch_y = yp - obj->a_absolute.ys; // + TITLE_HEIGHT;
						}
					}
				}
			}
			else{
				if(obj->isPressed){
					obj->isPressed = FALSE;
					obj->isTouchChanged = TRUE;
				}
			}
			//obj->isPressed = TRUE;
		}
	}
}

static void _updateCheckBox(DIALOG *dlg, DLG_OBJECT *obj){
	DLG_CHECKBOX *checkbox;
	DLG_AREA area;
	checkbox =  (DLG_CHECKBOX *)(obj->data);
	//Touch
	//if(obj->isTouchChanged){
		if (obj->isPressed && obj->isTouchChanged){
			checkbox->isChecked = !checkbox->isChecked;
			obj->needUpdating = TRUE;
		}
		if (!obj->isPressed && obj->isTouchChanged){
			obj->needUpdating = TRUE;
			obj->event = CHECKBOX_EVENT_STATUS_CHANGED;
		}
		obj->isTouchChanged = FALSE;
	//}
	//Update
	if ( obj->needUpdating){
		int16_t xs, ys; //, xc, yc, xe, ye;
		//char valueStr[7];
		getDialogArea(dlg, &area);
		obj->a_absolute.xs = obj->a_relative.xs + area.xs;
		obj->a_absolute.ys = obj->a_relative.ys + area.ys;
		obj->a_absolute.xe = obj->a_relative.xe + area.xs;
		obj->a_absolute.ye = obj->a_relative.ye + area.ys;

//		xc = UG_GetXDim() / 2;
//		yc = UG_GetYDim() / 2;
//		xe = xc + dlg->width / 2;
//		ye = yc + (dlg->height + TITLE_HEIGHT + BUTTON_AREA_HEIGHT) / 2;
//		if (obj->a_absolute.ye >= ye) return;
//		if (obj->a_absolute.xe >= xe) return;

		xs = obj->a_absolute.xs;
		ys = obj->a_absolute.ys; // + TITLE_HEIGHT;

		if(!obj->isDrawn){
			UG_DrawFrame(xs, ys, xs + 18, ys + 18, C_WHITE);
			UG_DrawFrame(xs + 1, ys + 1, xs + 17, ys + 17, C_WHITE);
			UG_FillFrame(xs + 2, ys + 2, xs + 16, ys + 16, 0x001637);
			UG_SetForecolor(C_WHITE);
			UG_SetBackcolor(0x001637);
			UG_FontSelect(&FONT_12X16);
			if(checkbox->text != NULL)
				UG_PutString(xs + 21, ys + 1, checkbox->text);
			obj->isDrawn = TRUE;
		}
		if(checkbox->isChecked){
			UG_DrawLine(xs + 4, ys + 7, xs + 9, ys + 14, C_GREEN);
			UG_DrawLine(xs + 9, ys + 14, xs + 16, ys + 2, C_GREEN);
		}
		else
			UG_FillFrame(xs + 2, ys + 2, xs + 16, ys + 16, 0x001637);

		if(!obj->isTouchChanged)
			delay_ms(100);
	}
	obj->needUpdating = FALSE;
}

BOOL createCheckBox(DIALOG *dlg, DLG_CHECKBOX *checkBox, uint8_t id, uint16_t x, uint16_t y, char *text){
	DLG_OBJECT *obj;
	obj = _getFreeObject(dlg);
	if(obj == NULL)
		return FALSE;

	checkBox->isActive = TRUE;
	checkBox->isChecked = FALSE;
	checkBox->text = text;

	obj->update = _updateCheckBox;
	obj->type = OBJ_TYPE_SPINBOX;
	obj->event = OBJ_EVENT_NONE;
	obj->needUpdating = TRUE;
	obj->isTouchChanged = FALSE;
	obj->isPressed = FALSE;
	obj->isDrawn = FALSE; //Not required
	obj->data = checkBox;
	obj->id = id;
	obj->a_relative.xs = x;
	obj->a_relative.ys = y;
	obj->a_relative.xe = x + 21 + strlen(text) * 16;
	obj->a_relative.ye = y + 18;
	obj->a_absolute.xs = -1;
	obj->a_absolute.ys = -1;
	obj->a_absolute.xe = -1;
	obj->a_absolute.ye = -1;
	obj->isFree = FALSE;

	return TRUE;
}

void CheckBox_setCheckState(DLG_CHECKBOX *checkBox, BOOL isChecked){
	checkBox->isChecked = isChecked;
}

BOOL CheckBox_isChecked(DLG_CHECKBOX *checkBox){
	return checkBox->isChecked;
}


BOOL createRadioButtonList(DIALOG *dlg, DLG_RADIO_BUTTON_LIST *radioButtonList, uint8_t id, uint16_t x, uint16_t y, char **items, uint8_t itemsCount){
	DLG_OBJECT *obj;
	uint8_t maxTextLength = 0, currentItemIndex, currentItemTextLength;
	obj = _getFreeObject(dlg);
	if(obj == NULL || items == NULL){
		return FALSE;
	}

	for(currentItemIndex = 0; currentItemIndex < itemsCount; currentItemIndex++){
		for(currentItemTextLength = 0; items[currentItemIndex][currentItemTextLength]; currentItemTextLength++);
		if(currentItemTextLength > maxTextLength)
			maxTextLength = currentItemTextLength;
	}

	radioButtonList->currentItem = 0;
	radioButtonList->items = items;
	radioButtonList->itemsCount = itemsCount;

	obj->update = _updateRadioButtonList;
	obj->type = OBJ_TYPE_RADIO_BUTTON_LIST;
	obj->event = OBJ_EVENT_NONE;
	obj->needUpdating = TRUE;
	obj->isTouchChanged = FALSE;
	obj->isPressed = FALSE;
	obj->isDrawn = FALSE; //Not required
	obj->data = radioButtonList;
	obj->id = id;
	obj->a_relative.xs = x;
	obj->a_relative.ys = y;
	obj->a_relative.xe = x + 21 + maxTextLength * 16;
	obj->a_relative.ye = y + 23 * itemsCount;
	obj->a_absolute.xs = -1;
	obj->a_absolute.ys = -1;
	obj->a_absolute.xe = -1;
	obj->a_absolute.ye = -1;
	obj->isFree = FALSE;

	return TRUE;
}

int8_t RadioButtonList_getCurrentIndex(DLG_RADIO_BUTTON_LIST *radioButtonList){
	return radioButtonList->currentItem;
}

void RadioButtonList_getCurrentText(DLG_RADIO_BUTTON_LIST *radioButtonList, char *currentText){
	currentText = radioButtonList->items[radioButtonList->currentItem];
}

BOOL RadioButtonList_setIndex(DLG_RADIO_BUTTON_LIST *radioButtonList, uint8_t index){
	if(index < radioButtonList->itemsCount){
		radioButtonList->currentItem = index;
		return TRUE;
	}
	return FALSE;
}

static void _updateRadioButtonList(DIALOG *dlg, DLG_OBJECT *obj){
	DLG_RADIO_BUTTON_LIST *radioButtonList;
	DLG_AREA area;
	radioButtonList =  (DLG_RADIO_BUTTON_LIST *)(obj->data);
	//Touch
	//if(obj->isTouchChanged){
		if (obj->isPressed && obj->isTouchChanged){
			radioButtonList->currentItem = obj->touch_y / 23; //23 - height of single item
			obj->needUpdating = TRUE;
		}
		if (!obj->isPressed && obj->isTouchChanged){
			obj->needUpdating = TRUE;
			obj->event = RADIOBUTTONLIST_EVENT_STATUS_CHANGED;
		}
		obj->isTouchChanged = FALSE;
	//}
	//Update
	if (obj->needUpdating){
		int16_t xs, ys; //, xc, yc, xe, ye;
		uint8_t currentItemIndex;
		char valueStr[7];
		getDialogArea(dlg, &area);
		obj->a_absolute.xs = obj->a_relative.xs + area.xs;
		obj->a_absolute.ys = obj->a_relative.ys + area.ys;
		obj->a_absolute.xe = obj->a_relative.xe + area.xs;
		obj->a_absolute.ye = obj->a_relative.ye + area.ys;

//		xc = UG_GetXDim() / 2;
//		yc = UG_GetYDim() / 2;
//		xe = xc + dlg->width / 2;
//		ye = yc + (dlg->height + TITLE_HEIGHT + BUTTON_AREA_HEIGHT) / 2;
//		if (obj->a_absolute.ye >= ye) return;
//		if (obj->a_absolute.xe >= xe) return;

		xs = obj->a_absolute.xs;
		ys = obj->a_absolute.ys; // + TITLE_HEIGHT;

		if(!obj->isDrawn){
			UG_SetForecolor(C_WHITE);
			UG_SetBackcolor(0x001637);
			UG_FontSelect(&FONT_12X16);
			for(currentItemIndex = 0; currentItemIndex < radioButtonList->itemsCount; currentItemIndex++){
				UG_DrawCircle(xs + 9, ys + 23 * currentItemIndex + 5, 9, C_WHITE);
				UG_DrawCircle(xs + 9, ys + 23 * currentItemIndex + 5, 8, C_WHITE);
				UG_PutString(xs + 21, ys + 23 * currentItemIndex, radioButtonList->items[currentItemIndex]);
			}
			obj->isDrawn = TRUE;
		}

		for(currentItemIndex = 0; currentItemIndex < radioButtonList->itemsCount; currentItemIndex++){
			if(currentItemIndex == radioButtonList->currentItem){
				UG_FillCircle(xs + 9, ys + 23 * currentItemIndex + 5, 5, C_GREEN);
			}
			else{
				UG_FillCircle(xs + 9, ys + 23 * currentItemIndex + 5, 5, 0x001637);
			}
		}

		if(!obj->isTouchChanged)
			delay_ms(100);
	}
	obj->needUpdating = FALSE;
}

void setDialogState(DIALOG *dlg, DLG_STATE state){
	dlg->dialogState = state;
}

DLG_STATE getDialogState(DIALOG *dlg){
	return dlg->dialogState;
}

static void _cbMsgBox(DIALOG *dlg, DLG_MESSAGE *msg){
	if(msg->id == OBJ_TYPE_BUTTON){
		setDialogState(dlg, DLG_STATE_CLOSE);
	}
}

void msgBox(char *msg, char *title, DLG_STANDARD_ICON standardIcon, bool (*userFunction)()){
	DIALOG dlgMsgBox;
	DLG_OBJECT objects[3];
	DLG_BUTTON butt[1];
	DLG_LABEL label;
	uint8_t ids[1] = {0};
	char *captions[1] = {"OK"};
	createDialog(&dlgMsgBox, objects, (standardIcon == 0) ? 2 : 3, _cbMsgBox);
	setDialogSize(&dlgMsgBox, 330, 100);
	setDialogTitleText(&dlgMsgBox, title);
	if(!createButtonsInButtonBox(&dlgMsgBox, 1, butt, ids, captions))
		return;

	if(standardIcon == 0){
		createLabel(&dlgMsgBox, &label, 1, 5, TITLE_HEIGHT + 7, 320, 79);
		setLabelText(&label, msg, C_WHITE, &FONT_12X20);
	}
	else{
		createStandardIcon(&dlgMsgBox, &standardIcon, 1, 5, TITLE_HEIGHT + 5);
		createLabel(&dlgMsgBox, &label, 2, 70, TITLE_HEIGHT + 7, 255, 79);
		setLabelText(&label, msg, C_WHITE, &FONT_12X20);
	}

	do{
		updateDialog(&dlgMsgBox);
		if(userFunction != NULL){
			if(userFunction() == false){
				break;
			}
		}
	}
	while(getDialogState(&dlgMsgBox) == DLG_STATE_SHOWN);
}
